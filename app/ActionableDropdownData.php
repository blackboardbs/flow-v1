<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionableDropdownData extends Model
{
    protected $table = 'actionable_dropdown_data';

    public function item()
    {
        return $this->hasOne('App\ActionableDropdownItem', 'id', 'actionable_dropdown_item_id');
    }
}
