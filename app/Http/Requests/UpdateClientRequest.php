<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('maintain_client');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            /*'company' => 'required|string|max:255',*/
            'email' => 'required|email|max:255',
            'contact' => 'nullable|string|max:255',
            'referrer' => 'nullable|integer|exists:referrers,id',
            'process' => 'required|integer|exists:processes,id'
        ];
    }
}
