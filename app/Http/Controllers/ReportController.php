<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenerateReportRequest;
use App\ActionableMultipleAttachmentData;
use App\ActionableTemplateEmailData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableDocumentData;
use App\ActionableBooleanData;
use App\ActionableTextData;
use App\ActionableTextareaData;
use App\ActionableDateData;
use App\ActionableNotificationData;
use App\ActionsAssigned;
use App\Referrer;
use App\Template;
use App\Activity;
use App\ActivityLog;
use App\Client;
use App\ClientUser;
use App\Config;
use App\Process;
use App\Step;
use App\User;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Report;
use App\Http\Requests\UpdateReportRequest;
use App\Exports\DynamicReportExport;
use Maatwebsite\Excel\Excel;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\Presentation;
use Illuminate\Support\Arr;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use App\Log;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class ReportController extends Controller {
    public function index()
    {
        $parameters = [
            'reports' => Report::orderBy('name')->with('user', 'activity')->get()
        ];
        return view('reports.view')->with($parameters);
    }

    public function auditReport(Request $request){
        $logs = Log::with('user','client','activities.activity');

        if($request->input('f') && $request->input('f') != '') {
            $logs = $logs->where('created_at', '>=', $request->input('f'));
        }

        if($request->input('t') && $request->input('t') != '') {
            $logs = $logs->where('created_at', '<=', $request->input('t'));
        }

        $logs = $logs->orderBy('created_at','desc')->limit(1000)->get();

        $log_array = new Collection();

        foreach($logs as $log){

            foreach($log->activities as $activity){

                if($activity->old_value != null) {
                    $action = 'Activity Updated';
                } else {
                    $action = 'Activity Captured';
                }

                $new_value = $activity->new_value;

                if($activity->activity["actionable_type"] == 'App\ActionableBoolean'){
                    $new_value = ($activity->new_value != null && $activity->new_value == 1 ? 'Yes' : 'No');
                } elseif($activity->activity["actionable_type"] == 'App\ActionableDropdown'){
                    if($activity->new_value != null) {
                        $dd = ActionableDropdownItem::withTrashed()->where('id', $activity->new_value)->first();
                        $new_value = (isset($dd) && $dd != null ? $dd->name : '');
                    } else {
                        $new_value = '';
                    }
                    //dd($new_value);
                } elseif($activity->activity["actionable_type"] == 'App\ActionableNotification'){
                    $notifications = explode(',',$activity->new_value);
                    $data_value = '';
                    foreach ($notifications as $notification){
                        $user_name = User::withTrashed()->where('id',$notification)->first();
                        if($user_name["id"] != null) {
                            if ($data_value == '') {
                                $data_value = $user_name["first_name"] . ' ' . $user_name["last_name"];
                            } else {
                                $data_value .= "," . $user_name["first_name"] . ' ' . $user_name["last_name"];
                            }
                        }
                    }
                    $new_value = $data_value;
                }
                $log_array->push([
                    'client' => ($log->client["company"] != null && $log->client["company"] != '' ? $log->client["company"] : $log->client["first_name"].' '.$log->client["last_name"]),
                    'id' => $activity->id,
                    'client_id' => $log->client["id"],
                    'action' => $action,
                    'user_id' => $log->user["id"],
                    'user' => $log->user["first_name"].' '.$log->user["last_name"],
                    'activity_id' => $activity->activity_id,
                    'activity_name' => $activity->activity_name,
                    'old_value' => $activity->old_value,
                    'new_value' => $new_value,
                    'created_at' => Carbon::parse($activity->created_at)->format('Y-m-d')
                ]);
            }
        }

        if($request->has('activities_search') && $request->input('activities_search') !='' ){
            $activity_input = $request->input('activities_search');

            $log_array = $log_array->filter(function ($log) use ($activity_input) {
                return $log['activity_id'] == $activity_input;
            });
        }

        if($request->has('client_search') && $request->input('client_search') !='' ){
            $client_input = $request->input('client_search');

            $log_array = $log_array->filter(function ($log) use ($client_input) {
                return $log['client_id'] == $client_input;
            });
        }

        if($request->has('user') && $request->input('user') !='' ){
            $user = $request->input('user');

            $log_array = $log_array->filter(function ($log) use ($user) {
                return $log['user_id'] == $user;
            });
        }

        $client_array = [];

        $clients = Client::get();

        $client_array[0] = 'All';

        foreach ($clients as $ca){
            $client_array[$ca->id] = ($ca->company != null && $ca->company != '' ? $ca->company : $ca->first_name.' '.$ca->last_name);
        }

        $parameters = [
            'activities_dropdown' => Activity::orderBy('name')->pluck('name','id')->prepend('All', ''),
            'clients_dropdown' => $client_array,
            'users_dropdown' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', ''),
            'log_array' => $log_array
        ];

        return view('reports.auditreport')->with($parameters);

    }

    public function generateReport(Request $request){

        $clients = Client::with('referrer', 'process.steps.activities.actionable.data', 'introducer')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"))
            ->where('is_progressing','1');

        if ($request->has('s') && $request->input('s') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('s') . "%");
            });
        }

        $parameters = [ 'clients' => $clients->get(),
            'templates' => Template::orderBy('name')->pluck('name','id')->prepend('Select','0')
        ];

        return view('reports.generate_report')->with($parameters);

    }

    public function generateReportExport(GenerateReportRequest $request){
        //dd($request);
        $template = Template::where('id', $request->input('template'))->first();

        if($template->type() == 'docx'){
            return $this->wordExport($request->input('client_id'),$template->process_id,$template->id);
        }

        if($template->type() == 'pptx'){
            return $this->powerpointExport($request->input('client_id'),$template->process_id,$template->id);
        }
    }

    /**
     * Upload a PPTX file as a template
     *
     * @return void
     */
    /*public function generatePowerpoint() {

    }*/

    /**
     * Upload logic for PPTX
     *
     * @return void
     */
    /*public function storePowerpoint(Request $request) {
        $name = '';
        if ($request->hasFile('file')) {
            //$request->file('file')->store('templates');
            //ToDo: The above save every file as .bin, Please fix if you have a better way of uploading documents
            $file = $request->file('file');
            $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$file->getClientOriginalExtension();
            $stored = $file->storeAs('templates', $name);
        }

        $template = new Template;
        $template->name = $request->input('name');
        $template->file = $name;
        $template->user_id = auth()->id();
        $template->save();

        return redirect(route('templates.index'))->with('flash_success', 'Template uploaded successfully');
    }*/

    /**
     * Show a list of clients with a powerpoint report download link
     *
     * @param Request $request
     * @return void
     */
    /*public function powerpointShow(Request $request) {

        $clients = Client::with('referrer', 'process.steps.activities.actionable.data', 'introducer')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'))
            ->where('is_progressing','1');

        if ($request->has('s') && $request->input('s') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('s') . "%");
            });
        }

        $parameters = [ 'clients' => $clients->get(),
            'templates' => Template::orderBy('name')->pluck('name','id')->prepend('Select','0')
            ];

        return view('reports.powerpointshow')->with($parameters);
    }*/

    public function wordExport($client_id,$process_id,$template_id){
        $client = Client::where('id',$client_id)->first();

        $template_file = Template::withTrashed()->where('id',$template_id)->first();

        $steps = Step::with(['activities.actionable.data'=>function ($q) use ($client_id){
            $q->where('client_id',$client_id);
        }])->where('process_id',$process_id)->get();

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/' . $template_file->file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $var_array = array();
        $value_array = array();
        //dd($formfields);
        foreach(collect($client)->toArray() as $column_name => $value) {
            $exclude = ['referrer_id','introducer_id','deleted_at','created_at','updated_at','office_id','process_id','step_id','is_progressing','not_progressing_date','completed_at','needs_approval','id'];
            if(!in_array($column_name,$exclude)) {
                array_push($var_array, 'client.' . $column_name);
                array_push($value_array, $value);
            }
        }
//dd($var_array);
        foreach($steps as $step) {
            //dd($steps);
            foreach ($step["activities"] as $activity) {
                    $var = '';
                    switch ($activity['actionable_type']){
                        case 'App\ActionableDropdown':
                            $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                            array_push($var_array,$var);
                            $items = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->get();
                            /*foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }*/
                            break;

                        default:
                            $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                            array_push($var_array, $var);
                            break;
                    }

                    if (isset($activity["actionable"]->data) && count($activity["actionable"]->data) > 0) {
                        foreach ($activity["actionable"]->data as $value) {

                            switch ($activity['actionable_type']){
                                case 'App\ActionableDropdown':
                                    /*$items = ActionableDropdownItem::where('actionable_dropdown_id',$value->actionable_dropdown_id)->get();
                                    if($items){
                                        foreach ($items as $item) {*/

                                            $data = ActionableDropdownItem::where('id',$value->actionable_dropdown_item_id)->first();
                                            if($data){
                                                array_push($value_array, $data["name"]);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                    /*    }
                                    } else {
                                        array_push($value_array, '');
                                    }*/
                                    break;
                                case 'App\ActionableBoolean':
                                    $items = ActionableBooleanData::where('client_id',$client_id)->where('actionable_boolean_id',$value->actionable_boolean_id)->first();
                                    //dd($items);
                                    if($items){
                                        array_push($value_array, ($items->data == '0' ? 'No' : 'Yes'));
                                        //dd('');
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                default:

                                    array_push($value_array, $value->data);
                                    break;
                            }
                        }
                    } else {

                        //dd($section["input_type"]);
                        switch ($activity['actionable_type']){
                            case 'App\ActionableDropdown':
                                $items = ActionableDropdownItem::where('actionable_dropdown_id',$activity["actionable_id"])->get();
                                if($items){
                                    foreach ($items as $item) {

                                        array_push($value_array, '');

                                    }
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            default:

                                array_push($value_array, '');
                                break;
                        }
                    }
            }
        }
//dd($var_array);
        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedforms/'.($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name).'/';
        if (!File::exists(storage_path('app/forms/' . $processed_template_path))) {
            Storage::makeDirectory('forms/' . $processed_template_path);
        }
        $filename = explode('.',$template_file);

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',$template_file->name). "_" . ($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name) . ".docx";
        if(File::exists(storage_path('app/forms/' . $processed_template))){
            Storage::delete('forms/' . $processed_template);
        }

        /*$processed_template_pdf = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',$filename[0]). "_" . ($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name) . ".pdf";

        if(File::exists(storage_path('app/forms/' . $processed_template_pdf))){
            Storage::delete('forms/' . $processed_template_pdf);
        }*/
        $templateProcessor->saveAs(storage_path('app/forms/' . $processed_template));
        //dd(storage_path('app/forms/' .$processed_template));
        //shell_exec('libreoffice --headless --convert-to pdf '.storage_path('app/forms/' .$processed_template).' --outdir '.storage_path('app/forms/' . $processed_template_path));
        return Storage::download('forms/' . $processed_template);

    }
    /**
     * Export Activity data to powerpoint
     *
     * @param Int client_id
     * @param Request $request
     *
     * @return void
     */
    public function powerpointExport($client_id,$process_id,$template_id)
    {
        // Grab the client
        $client = Client::where('id',$client_id);

        // Client details in an array
        $client = $client->first()->toArray();

        // What will eventually be sent to the report
        $output = [];
        $processData = [];

        // We have a client
        if($client) {
            // loop over steps to get the activity names, storing them in an assoc. array
                $steps = Step::with(['process'=> function($q) use ($process_id) {
                    $q->where('id',$process_id);
                }])->orderBy('id')->get();

                    foreach($steps as $step) {
                        $activities = Activity::with(['actionable.data'=>function($query) use ($client){
                            $query->where('client_id',$client["id"])->orderBy('created_at','desc');
                        }])->get();

                    foreach($activities as $activity) {
                        if (strpos($activity['actionable_type'], 'Actionable') !== false) {
                            $completed_activity_clients_data = null;
                            switch ($activity['actionable_type']) {
                                case 'App\ActionableBoolean':
                                    $filter_data = ['' => 'All', 0 => 'No', 1 => 'Yes', 2 => 'Not Completed'];
                                    $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity['actionable_id'])
                                        ->select('client_id', 'data')
                                        ->distinct()
                                        ->pluck('data', 'client_id');
                                    break;
                                case 'App\ActionableDate':
                                    $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                    $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity['actionable_id'])
                                        ->select('client_id', 'data')
                                        ->distinct()
                                        ->pluck('data', 'client_id');
                                    break;
                                case 'App\ActionableText':
                                    $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                    $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity['actionable_id'])
                                        ->select('client_id', 'data')
                                        ->distinct()
                                        ->pluck('data', 'client_id');
                                    break;
                                case 'App\ActionableTextarea':
                                    $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                    $completed_activity_clients_data = ActionableTextareaData::where('actionable_textarea_id', $activity['actionable_id'])
                                        ->select('client_id', 'data')
                                        ->distinct()
                                        ->pluck('data', 'client_id');
                                    break;
                                case 'App\ActionableDropdown':
                                    $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity['actionable_id'])
                                        ->select('name', 'id')
                                        ->distinct()
                                        ->pluck('name', 'id')
                                        ->prepend('All', 0);

                                    /*if ($request->has('activity') && $request->input('activity') != '') {*/
                                        $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])

                                            ->select('client_id', 'actionable_dropdown_item_id')
                                            ->distinct()
                                            //->get()->toArray();
                                            ->pluck('actionable_dropdown_item_id', 'client_id');
                                    /*} else {
                                        $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])
                                            ->select('client_id', 'actionable_dropdown_item_id')
                                            ->distinct()
                                            //->get()->toArray();
                                            ->pluck('actionable_dropdown_item_id', 'client_id');
                                    }*/

                                    // $tmp_filter_data2 = $filter_data->toArray();
                                    // foreach($tmp_filter_data2 as $key=>$value):
                                    //     array_push($tmp_filter_data, $key);
                                    // endforeach;
                                    break;
                                case 'App\ActionableDocument':
                                    $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                    $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity['actionable_id'])
                                        ->select('client_id', 'actionable_document_id')
                                        ->distinct()
                                        ->pluck('actionable_document_id', 'client_id');
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $filter_data = Template::orderBy('name')
                                        ->select('name', 'id')
                                        ->distinct()
                                        ->pluck('name', 'id')
                                        ->prepend('All', 0);
                                    $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity['actionable_id'])
                                        ->select('client_id', 'template_id')
                                        ->distinct()
                                        ->pluck('template_id', 'client_id');
                                    break;
                                case 'App\ActionableNotification':
                                    $filter_data = ['' => 'All', 1 => 'Sent', 0 => 'Not Sent'];
                                    $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity['actionable_id'])
                                        ->select('client_id', 'actionable_notification_id')
                                        ->distinct()
                                        ->pluck('actionable_notification_id', 'client_id');
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $filter_data = Template::orderBy('name')
                                        ->select('name', 'id')
                                        ->distinct()
                                        ->pluck('name', 'id')
                                        ->prepend('All', 0);
                                    $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity['actionable_id'])
                                        ->select('client_id', 'template_id')
                                        ->distinct()
                                        ->pluck('template_id', 'client_id');
                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }

                            $data_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                            $completed_value = '';
                            $selected_drop_down_names = '';

                            $data = '';
                            $yn_value = '';
                            switch ($activity['actionable_type']) {
                                case 'App\ActionableBoolean':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) ? 'Yes' : 'No';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 2;
                                    if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '1') {
                                        $yn_value = "Yes";
                                    }
                                    if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '0') {
                                        $yn_value = "No";
                                    }
                                    break;
                                case 'App\ActionableDate':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                    break;
                                case 'App\ActionableText':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                    break;
                                case 'App\ActionableTextarea':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                    break;
                                case 'App\ActionableDropdown':
                                    $data_value = '';
                                    /*if ($request->has('s') && $request->input('s') != '') {
                                        $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity['actionable_id'])
                                            ->where('client_id', $client['id'])
                                            ->select('actionable_dropdown_item_id')
                                            ->distinct()
                                            ->get()->toArray();

                                        foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                                            //dd($selected_drop_down_item);
                                            if (in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                                if ($key == sizeof($selected_drop_down_items) - 1)
                                                    $data_value .= $selected_drop_down_item['item']['name'];
                                                else
                                                    $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                                            }
                                        endforeach;
                                    }*/
                                    $data = ActionableDropdownData::with('item')->where('client_id', $client['id'])->where('actionable_dropdown_id', $activity['actionable_id'])->first();
                                    //dd($data->item->name);
                                    $activity_data_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                    break;
                                case 'App\ActionableDocument':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';

                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                    break;
                                case 'App\ActionableTemplateEmail':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                    break;
                                case 'App\ActionableNotification':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                    break;
                                case 'App\ActionableMultipleAttachment':
                                    $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                    $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                    break;
                                default:
                                    //todo capture defaults
                                    break;

                            }


                            $processData[1] [] = ['name' => strtolower(str_replace(' ', '_', $activity['name'])),
                                'data' => $completed_value];


                        }
                    }
            }
        }

        // Get the pptx template
        $template = Template::where('id', $template_id)->first();

        $presentation = new Presentation($template->file, ['client' => $client, 'activities' => $processData[1]]);

        // do whatevs
        $presentation->run();
        $downloadFile = $presentation->getDownloadPath();

        $headers = array(
            'Content-Type: application/vnd.ms-powerpoint',
        );

        return \Response::download($downloadFile, 'report_'.date('Y_m_d_H_i_s').'.pptx', $headers);
    }

    public function assignedactivities(Request $request) {

        /* Get the raw data for assigned activities */
        $result = ActionsAssigned::with('client');

        if($request->input('assigned_user') && $request->input('assigned_user') != '') {
            $result = $result->whereHas('activity', function ($q) use ($request) {
                $q->where('user_id', $request->input('assigned_user'));
            });
        }

        if($request->input('activities_search') && $request->input('activities_search') != '') {
            $a = $request->input('activities_search');
            $result = $result->with(['activity' => function ($q) use ($a) {
                $q->where('activity_id', $a);
            }])->whereHas('activity', function ($q) use ($a) {
                $q->where('activity_id', $a);
            });
        }

        if($request->input('client_search') && $request->input('client_search') != 0) {
            $result = $result->whereHas('activity', function ($q) use ($request) {
                $q->where('client_id', $request->input('client_search'));
            });
        }

        if($request->input('f') && $request->input('f') != '') {
            $result = $result->where('due_date', '>=', $request->input('f'));
        }

        if($request->input('t') && $request->input('t') != '') {
            $result = $result->where('due_date', '<=', $request->input('t'));
        }

        $result = $result->whereHas('activity', function($q){
            $q->where('status','0');
        })->where('completed',0)->orderBy('due_date','desc')->get();

        $configs = Config::first();
//dd($result);
        /*  Separate out the collection into an array we can manipulate better in the template */
        $activities = [];
        $ud = [];

        foreach($result as $activity) {

            // User IDs are comma-separated in the database
            $split_users = explode(',', $activity->users);
            $auser_array = array();
            if ($activity->client) {
                // List of Activities
                foreach ($activity->activity as $activity_id) {
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        $ud[$user["first_name"] . ' ' . $user["last_name"]][] = ['due_date'=>$activity->due_date];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {
                        $clientid = $activity->client["id"];

                        if(isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"]) && $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"] > $activity->due_date){
                            $due_date = $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"];
                        } else {
                            $due_date = $activity->due_date;
                        }

                        $client_name = ($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"]);

                        //Get the current timestamp.
                        $now = strtotime(now());

                        //Calculate the difference.
                        $difference = $now - strtotime($due_date);

                        //Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days < -$configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(2);
                        } elseif ($days <= $configs->action_threshold) {
                            if (Carbon::parse(now()) > Carbon::parse($due_date)) {
                                $class = $activity->client->process->getStageHex(0);
                            } elseif (Carbon::parse(now()) >= Carbon::parse($due_date)->subDay($configs->action_threshold)) {
                                $class = $activity->client->process->getStageHex(1);
                            }
                        } elseif ($days > $configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(0);
                        } else {
                            $class = $activity->client->process->getStageHex(0);
                        }

                        if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                            $activities[$client_name] [$activity_id->activity_id] = [
                                'client_id' => $activity->client["id"],
                                'client_name' => $client_name,
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                'due_date' => $due_date,
                                'class' => $class];
                        } elseif (Auth::check() && Auth::user()->is("admin")) {
                            $activities[$client_name] [$activity_id->activity_id] = [
                                'client_id' => $activity->client["id"],
                                'client_name' => $client_name,
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                'due_date' => $due_date,
                                'created_date' => '',
                                'updated_date' => '',
                                'class' => $class];
                        }
                    }

                }

            }

        }

        ksort($activities);

        $client_array = [];

        $clients = Client::get();

        $client_array[0] = 'All';

        foreach ($clients as $ca){
            $client_array[$ca->id] = ($ca->company != null && $ca->company != '' ? $ca->company : $ca->first_name.' '.$ca->last_name);
        }

        //sort($client_array);

        $parameters = [
            'activities_dropdown' => Activity::orderBy('name')->pluck('name','id')->prepend('All', ''),
            'clients_dropdown' => $client_array,
            'assigned_users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All', ''),
            'activities' => $activities
        ];

        return view('reports.assignedactivities')->with($parameters);
    }

    public function show(Activity $activity, Request $request)
    {
        $request->session()->forget('path_route');

        $config = Config::select('client_referrer','client_director','client_onboardingm','client_onboardingl')->first();


        $client_activity = Activity::with('actionable.data')->get();
        //dd($client_activity);

        $clients = Client::with('referrer', 'process.steps.activities.actionable.data', 'introducer')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'))
        ->where('is_progressing','1');

        if ($request->has('s') && $request->input('s') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('s') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('s') . "%");
            });
        }
        //$adata = ActionableDropdownData::where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id);
        $clients = $clients->get();

        //Actitivy type report filter
        $completed_activity_clients_data = null;
        $filter_data = [];
        $tmp_filter_data = [];
        switch ($activity->actionable_type) {
            case 'App\ActionableBoolean':
                $filter_data = ['' => 'All', 0=>'No', 1=>'Yes', 2 => 'Not Completed'];
                $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDate':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableText':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDropdown':
                $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);

                if($request->has('activity') && $request->input('activity') !='' ) {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->where('actionable_dropdown_item_id', $request->input('activity'))
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }
                else
                {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }

                $tmp_filter_data2 = $filter_data->toArray();
                foreach($tmp_filter_data2 as $key=>$value):
                    array_push($tmp_filter_data, $key);
                endforeach;
                break;
            case 'App\ActionableDocument':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_document_id')
                    ->distinct()
                    ->pluck('actionable_document_id', 'client_id');
                break;
            case 'App\ActionableTemplateEmail':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            case 'App\ActionableNotification':
                $filter_data = ['' => 'All', 1 =>'Sent', 0 => 'Not Sent'];
                $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_notification_id')
                    ->distinct()
                    ->pluck('actionable_notification_id', 'client_id');
                break;
            case 'App\ActionableMultipleAttachment':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            default:
                //todo capture defaults
                break;
        }

        //loop clients
        $client_data = new Collection();
        $activity_value = '';
        $completed_values = [];
        foreach ($clients as $client) {

            $activity2 = Activity::select('id','actionable_id','actionable_type')->whereIn('id',$config)->get();

            $referrer = null;
            $director = null;
            $onboardingm = null;
            $onboardingl = null;



            /*foreach ($client_activity as $activity_value) {
                dd($client_activity);
                if ($activity_value['actionable_type'] == 'App\ActionableDropdown'){
                    //dd($activity_value);
                    if ($activity_value["id"] == $config->referrer) {
                        $arr = (array)$activity_value['dropdown_items'];
                        $arr2 = (array)$activity_value['dropdown_values'];

                        foreach ((array)$arr as $key => $value) {
                            if (in_array($key, $arr2)) {
                                if($referrer == null){
                                    $referrer = $value;
                                } else {
                                    $referrer = $referrer.', '.$value;
                                }
                            }

                        }
                    }

                    if ($activity_value["id"] == $config->client_director) {
                        $arr = (array)$activity_value['dropdown_items'];
                        $arr2 = (array)$activity_value['dropdown_values'];

                        foreach ((array)$arr as $key => $value) {
                            if (in_array($key, $arr2)) {
                                if($director == null){
                                    $director = $value;
                                } else {
                                    $director = $director.', '.$value;
                                }
                            }

                        }
                    }

                    if ($activity_value["id"] == $config->client_onboardingm) {
                        $arr = (array)$activity_value['dropdown_items'];
                        $arr2 = (array)$activity_value['dropdown_values'];

                        foreach ((array)$arr as $key => $value) {
                            if (in_array($key, $arr2)) {
                                if($onboardingm == null){
                                    $onboardingm = $value;
                                } else {
                                    $onboardingm = $onboardingm.', '.$value;
                                }
                            }

                        }
                    }

                    if ($activity_value["id"] == $config->client_onboardingl) {
                        $arr = (array)$activity_value['dropdown_items'];
                        $arr2 = (array)$activity_value['dropdown_values'];

                        foreach ((array)$arr as $key => $value) {
                            if (in_array($key, $arr2)) {
                                if($onboardingl == null){
                                    $onboardingl = $value;
                                } else {
                                    $onboardingl = $onboardingl.', '.$value;
                                }
                            }

                        }
                    }
                }
                if ($activity_value["id"] == $config->client_referrer && $referrer == null && isset($activity_value["value"])) {
                    $referrer = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_director && $director == null && isset($activity_value["value"])) {
                    $director = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_onboardingm && $onboardingm == null && isset($activity_value["value"])) {
                    $onboardingm = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_onboardingl && $onboardingl == null && isset($activity_value["value"])) {
                    $onboardingl = $activity_value["value"];
                }
                dd($activity_value["value"]);

            }*/

            foreach($activity2 as $resu){
                //dd($resu);
                if(isset($resu->actionable_type)) {
                    switch ($resu->actionable_type) {

                        case 'App\ActionableText':
                            $dir = DB::select("select a.id,b.data as name from actionable_texts a inner join actionable_text_data b on b.actionable_text_id = a.id where b.client_id = '" . $client->id . "' and a.id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        case 'App\ActionableDropdown':
                            $dir = DB::select("select a.actionable_dropdown_item_id,b.name as name from actionable_dropdown_data a inner join actionable_dropdown_items b on a.actionable_dropdown_item_id = b.id where a.client_id = '" . $client->id . "' and a.actionable_dropdown_id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }
                }

                $res["referrer"] = $referrer;
                $res["director"] = $director;
                $res["onboardingm"] = $onboardingm;
                $res["onboardingl"] = $onboardingl;
            }


            $data_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? $completed_activity_clients_data[$client->id] : '';
            $completed_value = '';
            $selected_drop_down_names = '';

            $data = '';
            $yn_value = '';
            switch ($activity->actionable_type) {
                case 'App\ActionableBoolean':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) ? 'Yes' : 'No';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 2;
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '1'){
                        $yn_value = "Yes";
                    }
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '0'){
                        $yn_value = "No";
                    }
                    break;
                case 'App\ActionableDate':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableText':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableDropdown':
                    $data_value = '';
                    if($request->has('s') && $request->input('s') !='' ) {
                        $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity->actionable_id)
                            ->where('client_id', $client->id)
                            ->select('actionable_dropdown_item_id')
                            ->distinct()
                            ->get()->toArray();

                        foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                            //dd($selected_drop_down_item);
                            if(in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                if ($key == sizeof($selected_drop_down_items) - 1)
                                    $data_value .= $selected_drop_down_item['item']['name'];
                                else
                                    $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                            }
                        endforeach;
                    }
                    $data = ActionableDropdownData::with('item')->where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id)->first();
                    $activity_data_value = isset($completed_activity_clients_data[$client->id]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableDocument':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';

                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableTemplateEmail':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableNotification':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableMultipleAttachment':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                default:
                    //todo capture defaults
                    break;
            }

            //dd($completed_value);
            //add to array
            $client_data->push([
                'company' => ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name ),
                'activity_data' => (isset($activity_data_value) && $activity_data_value != null ? $activity_data_value : ''),
                'id' => $client->id,
                'email' => $client->email,
                'contact' => $client->contact,
                'process' => $client->process->name,
                'created_at' => $client->created_at->toDateString(),
                'completed_at' => ($client->completed_at != null ? $client->completed_at->toDateString() : '' ) ,
                'completed_days' => $client->completed_days,
                'step_id' => $client->step_id,
                'step' => $client->getCurrentStep()->name,
                'is_progressing' => $client->is_progressing,
                'needs_approval' => $client->needs_approval,
                'activity_value' => $activity_value,
                'data_value' => $data_value,
                'completed_yn' => ($activity->actionable_type == "App\ActionableBoolean" ? $yn_value : $completed_value),
                'selected_drop_down_names' => $selected_drop_down_names,
                'introducer' => $client->introducer,
                'avatar' => $client->avatar,
                'referrer' => $referrer,
                'director' => $director,
                'onboardingl' => $onboardingl,
                'onboardingm' => $onboardingm
            ]);
        }

        $activity_selected_value = 'All';
        if($request->has('activity') && $request->input('activity') != ''){
            $activity_selected_value = $filter_data[$request->input('activity')];
        }

        if($request->has('activity') && $request->input('activity') !='' ){
            $activity_input = $request->input('activity');

            $client_data = $client_data->filter(function ($client) use ($activity_input) {
                return $client['activity_value'] == $activity_input;
            });
        }

        if($request->has('step') && $request->input('step') !='' ){
            $step_input = $request->input('step');

            $client_data = $client_data->filter(function ($client) use ($step_input) {
                return $client['step_id'] == $step_input;
            });
        }

        $parameters = [
            'clients' => $client_data,
            'filter_data' => $filter_data,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all'),
            'steps' => Step::orderBy('process_id')->orderBy('order')->pluck('name', 'id')->prepend('All steps', ''),
            'activity' => $activity,
            'activity_selected_value' => $activity_selected_value
        ];

        return view('reports.show')->with($parameters);
    }

    public function export(Activity $activity, Excel $excel, Request $request)
    {
        $config = Config::select('client_referrer','client_director','client_onboardingm','client_onboardingl')->first();

        $clients = Client::with('referrer', 'process.steps.activities.actionable.data', 'introducer')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'));

        if ($request->has('s_selected') && $request->input('s_selected') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('s_selected') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('s_selected') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('s_selected') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('s_selected') . "%");
            });
        }

        $clients = $clients->get();

        //Actitivy type report filter
        $completed_activity_clients_data = null;
        $filter_data = [];
        $tmp_filter_data = [];
        switch ($activity->actionable_type) {
            case 'App\ActionableBoolean':
                $filter_data = ['' => 'All', 0=>'No', 1=>'Yes', 2 => 'Not Completed'];
                $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDate':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableText':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDropdown':
                $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);

                if($request->has('activity_selected') && $request->input('activity_selected') !='' ) {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->where('actionable_dropdown_item_id', $request->input('activity_selected'))
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }
                else
                {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }

                $tmp_filter_data2 = $filter_data->toArray();
                foreach($tmp_filter_data2 as $key=>$value):
                    array_push($tmp_filter_data, $key);
                endforeach;
                break;
            case 'App\ActionableDocument':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_document_id')
                    ->distinct()
                    ->pluck('actionable_document_id', 'client_id');
                break;
            case 'App\ActionableTemplateEmail':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            case 'App\ActionableNotification':
                $filter_data = ['' => 'All', 1 =>'Sent', 0 => 'Not Sent'];
                $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_notification_id')
                    ->distinct()
                    ->pluck('actionable_notification_id', 'client_id');
                break;
            case 'App\ActionableMultipleAttachment':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            default:
                //todo capture defaults
                break;
        }

        //loop clients
        $client_data = new Collection();
        $activity_value = '';
        $completed_values = [];
        foreach ($clients as $client) {

            $activity2 = Activity::select('id','actionable_id','actionable_type')->whereIn('id',$config)->get();

            $referrer = null;
            $director = null;
            $onboardingm = null;
            $onboardingl = null;

            foreach($activity2 as $resu){
                //dd($resu);
                if(isset($resu->actionable_type)) {
                    switch ($resu->actionable_type) {

                        case 'App\ActionableText':
                            $dir = DB::select("select a.id,b.data as name from actionable_texts a inner join actionable_text_data b on b.actionable_text_id = a.id where b.client_id = '" . $client->id . "' and a.id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        case 'App\ActionableDropdown':
                            $dir = DB::select("select a.actionable_dropdown_item_id,b.name as name from actionable_dropdown_data a inner join actionable_dropdown_items b on a.actionable_dropdown_item_id = b.id where a.client_id = '" . $client->id . "' and a.actionable_dropdown_id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }
                }

                $res["referrer"] = $referrer;
                $res["director"] = $director;
                $res["onboardingm"] = $onboardingm;
                $res["onboardingl"] = $onboardingl;
            }


            $data_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? $completed_activity_clients_data[$client->id] : '';
            $completed_value = '';
            $selected_drop_down_names = '';
            $data = '';
            $yn_value = '';
            switch ($activity->actionable_type) {
                case 'App\ActionableBoolean':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) ? 'Yes' : 'No';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 2;
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '1'){
                        $yn_value = "Yes";
                    }
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '0'){
                        $yn_value = "No";
                    }
                    break;
                case 'App\ActionableDate':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableText':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableDropdown':
                    $data_value = '';
                    if($request->has('s_selected') && $request->input('s_selected') !='' ) {
                        $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity->actionable_id)
                            ->where('client_id', $client->id)
                            ->select('actionable_dropdown_item_id')
                            ->distinct()
                            ->get()->toArray();

                        foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                            //dd($selected_drop_down_item);
                            if(in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                if ($key == sizeof($selected_drop_down_items) - 1)
                                    $data_value .= $selected_drop_down_item['item']['name'];
                                else
                                    $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                            }
                        endforeach;
                    }
                    $data = ActionableDropdownData::with('item')->where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id)->first();
                    $activity_data_value = (isset($data->item->name) && $data->item->name != null ? $data->item->name : '');
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableDocument':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableTemplateEmail':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableNotification':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableMultipleAttachment':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                default:
                    //todo capture defaults
                    break;
            }

            $activity_selected_value = 'All';
            if($request->has('activity_selected') && $request->input('activity_selected') != ''){
                $activity_selected_value = $filter_data[$request->input('activity_selected')];
            }
            //dd($completed_value);
            //add to array
            $client_data->push([
                'id' => $client->id,
                'company' => ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name ),
                'referrer' => $referrer,
                'director' => $director,
                'onboardingl' => $onboardingl,
                'onboardingm' => $onboardingm,
                'email' => $client->email,
                'contact' => $client->contact,
                //'activity_value' => $activity_value,
                'activity_name' => $activity->name,
                //'activity_data' => $activity->actionable_type == "App\ActionableText" || $activity->actionable_type == "App\ActionableDate" || $activity->actionable_type == "App\ActionableDocument" || $activity->actionable_type == "App\ActionableBoolean" ? $completed_value : $data_value,
                'activity_data' => ($activity->actionable_type == "App\ActionableBoolean" ? $yn_value : (isset($activity_data_value) && $activity_data_value != null ? $activity_data_value : '')),
                //'selected_drop_down_names' => $selected_drop_down_names,
                'process' => $client->process->name,
                'created_at' => $client->created_at->toDateString(),
                'completed_at' => $client->completed_at ,
                'completed_days' => $client->completed_days,
                /*'step_id' => $client->step_id,*/
                //'step_id' => $client->step_id,
                'step' => $client->getCurrentStep()->name,
                /*'is_progressing' => $client->is_progressing == 1? 'Yes' : 'No',
                'needs_approval' => $client->needs_approval == 1? 'Yes' : 'No',*/
                /*'introducer' => $client->introducer,
                'avatar' => $client->avatar,*/

                //'completed_yn' => $completed_value
            ]);
        }



        if($request->has('activity_selected_name') && $request->input('activity_selected_name') !='' ){
            $activity_input = $request->input('activity_selected_name');

            $client_data = $client_data->filter(function ($client) use ($activity_input) {
                return $client['activity_data'] == $activity_input;
            });
        }

        if($request->has('process_step_selected_name') && $request->input('process_step_selected_name') !='' ){
            $step_input = $request->input('process_step_selected_name');

            $client_data = $client_data->filter(function ($client) use ($step_input) {
                return $client['step'] == $step_input;
            });
        }

        return $excel->download(new DynamicReportExport($client_data), 'clients_'.date('Y_m_d_H_i_s').'.xlsx');
    }

    public function pdfexport(Activity $activity, Request $request)
    {
        $config = Config::select('client_referrer','client_director','client_onboardingm','client_onboardingl')->first();

        $clients = Client::with('referrer', 'process.steps.activities.actionable.data', 'introducer')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'));

        if ($request->has('pdf_s_selected') && $request->input('pdf_s_selected') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('pdf_s_selected') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('pdf_s_selected') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('pdf_s_selected') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('pdf_s_selected') . "%");
            });
        }

        $clients = $clients->get();

        //Actitivy type report filter
        $completed_activity_clients_data = null;
        $filter_data = [];
        $tmp_filter_data = [];
        switch ($activity->actionable_type) {
            case 'App\ActionableBoolean':
                $filter_data = ['' => 'All', 0=>'No', 1=>'Yes', 2 => 'Not Completed'];
                $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDate':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableText':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity->actionable_id)
                    ->select('client_id', 'data')
                    ->distinct()
                    ->pluck('data', 'client_id');
                break;
            case 'App\ActionableDropdown':
                $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);

                if($request->has('pdf_activity_selected') && $request->input('pdf_activity_selected') !='' ) {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->where('actionable_dropdown_item_id', $request->input('pdf_activity_selected'))
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }
                else
                {
                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)
                        ->select('client_id', 'actionable_dropdown_item_id')
                        ->distinct()
                        //->get()->toArray();
                        ->pluck('actionable_dropdown_item_id', 'client_id');
                }

                $tmp_filter_data2 = $filter_data->toArray();
                foreach($tmp_filter_data2 as $key=>$value):
                    array_push($tmp_filter_data, $key);
                endforeach;
                break;
            case 'App\ActionableDocument':
                $filter_data = ['' => 'All', 1 =>'Completed', 0 => 'Not Completed'];
                $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_document_id')
                    ->distinct()
                    ->pluck('actionable_document_id', 'client_id');
                break;
            case 'App\ActionableTemplateEmail':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            case 'App\ActionableNotification':
                $filter_data = ['' => 'All', 1 =>'Sent', 0 => 'Not Sent'];
                $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity->actionable_id)
                    ->select('client_id', 'actionable_notification_id')
                    ->distinct()
                    ->pluck('actionable_notification_id', 'client_id');
                break;
            case 'App\ActionableMultipleAttachment':
                $filter_data = Template::orderBy('name')
                    ->select('name', 'id')
                    ->distinct()
                    ->pluck('name', 'id')
                    ->prepend('All', 0);
                $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity->actionable_id)
                    ->select('client_id', 'template_id')
                    ->distinct()
                    ->pluck('template_id', 'client_id');
                break;
            default:
                //todo capture defaults
                break;
        }

        //loop clients
        $client_data = new Collection();

        $activity_value = '';
        $completed_values = [];
        foreach ($clients as $client) {

            $activity2 = Activity::select('id','actionable_id','actionable_type')->whereIn('id',$config)->get();

            $referrer = null;
            $director = null;
            $onboardingm = null;
            $onboardingl = null;

            foreach($activity2 as $resu){
                //dd($resu);
                if(isset($resu->actionable_type)) {
                    switch ($resu->actionable_type) {

                        case 'App\ActionableText':
                            $dir = DB::select("select a.id,b.data as name from actionable_texts a inner join actionable_text_data b on b.actionable_text_id = a.id where b.client_id = '" . $client->id . "' and a.id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        case 'App\ActionableDropdown':
                            $dir = DB::select("select a.actionable_dropdown_item_id,b.name as name from actionable_dropdown_data a inner join actionable_dropdown_items b on a.actionable_dropdown_item_id = b.id where a.client_id = '" . $client->id . "' and a.actionable_dropdown_id = '" . $resu->actionable_id . "' limit 1");

                            foreach ($dir as $t) {

                                if ($resu->id == $config->client_director) {
                                    $director = $t->name;
                                }
                                if ($resu->id == $config->client_referrer) {
                                    $referrer = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingm) {
                                    $onboardingm = $t->name;
                                }
                                if ($resu->id == $config->client_onboardingl) {
                                    $onboardingl = $t->name;
                                }
                            }
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }
                }

                $res["referrer"] = $referrer;
                $res["director"] = $director;
                $res["onboardingm"] = $onboardingm;
                $res["onboardingl"] = $onboardingl;
            }


            $data_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? $completed_activity_clients_data[$client->id] : '';
            $completed_value = '';
            $data = '';
            $yn_value = '';
            $selected_drop_down_names = '';
            switch ($activity->actionable_type) {
                case 'App\ActionableBoolean':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) ? 'Yes' : 'No';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 2;
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '1'){
                        $yn_value = "Yes";
                    }
                    if(isset($completed_activity_clients_data[$client->id]) && $completed_activity_clients_data[$client->id] == '0'){
                        $yn_value = "No";
                    }
                    break;
                case 'App\ActionableDate':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableText':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableDropdown':
                    $data_value = '';
                    if($request->has('pdf_s_selected') && $request->input('pdf_s_selected') !='' ) {
                        $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity->actionable_id)
                            ->where('client_id', $client->id)
                            ->select('actionable_dropdown_item_id')
                            ->distinct()
                            ->get()->toArray();

                        foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                            //dd($selected_drop_down_item);
                            if(in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                if ($key == sizeof($selected_drop_down_items) - 1)
                                    $data_value .= $selected_drop_down_item['item']['name'];
                                else
                                    $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                            }
                        endforeach;


                    }
                    $data = ActionableDropdownData::with('item')->where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id)->first();
                    $activity_data_value = (isset($data->item->name) && $data->item->name != null ? $data->item->name : '');
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableDocument':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableTemplateEmail':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                case 'App\ActionableNotification':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? 1 : 0;
                    break;
                case 'App\ActionableMultipleAttachment':
                    $completed_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != ''? 'Completed' : 'Not Completed';
                    $activity_value = isset($completed_activity_clients_data[$client->id]) ? $completed_activity_clients_data[$client->id] : 0;
                    break;
                default:
                    //todo capture defaults
                    break;
            }

            //dd($completed_value);
            //add to array
            $client_data->push([
                'company' => ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name ),
                'activity_data' => (isset($activity_data_value) && $activity_data_value != null ? $activity_data_value : ''),
                'id' => $client->id,
                'email' => $client->email,
                'contact' => $client->contact,
                'process' => $client->process->name,
                'created_at' => $client->created_at->toDateString(),
                'completed_at' => ($client->completed_at != null ? $client->completed_at->toDateString() : '' ) ,
                'completed_days' => $client->completed_days,
                'step_id' => $client->step_id,
                'step' => $client->getCurrentStep()->name,
                'is_progressing' => $client->is_progressing,
                'needs_approval' => $client->needs_approval,
                'activity_value' => $activity_value,
                'data_value' => $data_value,
                'completed_yn' => ($activity->actionable_type == "App\ActionableBoolean" ? $yn_value : $completed_value),
                'selected_drop_down_names' => $selected_drop_down_names,
                'introducer' => $client->introducer,
                'avatar' => $client->avatar,
                'referrer' => $referrer,
                'director' => $director,
                'onboardingl' => $onboardingl,
                'onboardingm' => $onboardingm
            ]);
        }

        $activity_selected_value = 'All';
        if($request->has('pdf_activity_selected') && $request->input('pdf_activity_selected') != ''){
            $activity_selected_value = $filter_data[$request->input('pdf_activity_selected')];
        }

        if($request->has('pdf_activity_selected') && $request->input('pdf_activity_selected') !='' ){
            $activity_input = $request->input('pdf_activity_selected');

            $client_data = $client_data->filter(function ($client) use ($activity_input) {
                return $client['activity_value'] == $activity_input;
            });
        }

        if($request->has('pdf_process_step_selected') && $request->input('pdf_process_step_selected') !='' ){
            $step_input = $request->input('pdf_process_step_selected');

            $client_data = $client_data->filter(function ($client) use ($step_input) {
                return $client['step_id'] == $step_input;
            });
        }

        $parameters = [
            'clients' => $client_data,
            //'filter_data' => $filter_data,
            //'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all'),
            //'steps' => Step::orderBy('process_id')->orderBy('order')->pluck('name', 'id')->prepend('All steps', ''),
            'activity' => $activity,
            'activity_selected_value' => $activity_selected_value
        ];

        $pdf = PDF::loadView('pdf.customreport', $parameters)->setPaper('a4')->setOrientation('landscape');
        return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');
    }

    public function converted(Request $request)
    {
        $end_date = Carbon::now();

        if ($request->has('t')) {
            $end_date = Carbon::parse($request->input('t'));
        }

        $start_year = $end_date->copy()->subYear();
        $start_quarter = $end_date->copy()->subMonths(3);
        $start_month = $end_date->copy()->subMonth();

        $config = Config::first();

        $last_year = Client::where('completed_at', '>=', $start_year)->where('completed_at', '<=', $end_date)->count();
        $last_quarter = Client::where('completed_at', '>=', $start_quarter)->where('completed_at', '<=', $end_date)->count();
        $last_month = Client::where('completed_at', '>=', $start_month)->where('completed_at', '<=', $end_date)->count();

        $parameters = [
            'end_date' => $end_date->toDateString(),
            'clients' => [
                'last_year' => [
                    'actual' => $last_year,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_year),
                    'difference' => $last_year - ($config->onboards_per_day * $end_date->diffInDays($start_year)),
                ],
                'last_quarter' => [
                    'actual' => $last_quarter,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_quarter),
                    'difference' => $last_quarter - ($config->onboards_per_day * $end_date->diffInDays($start_quarter)),
                ],
                'last_month' => [
                    'actual' => $last_month,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_month),
                    'difference' => $last_month - ($config->onboards_per_day * $end_date->diffInDays($start_month)),
                ]
            ],
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.converted')->with($parameters);
    }

    public function fees()
    {
        $parameters = [
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.fees')->with($parameters);
    }

    public function referrer(Request $request)
    {
        $referrers = Referrer::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('All Referrers', 'all');
        $clients = Client::with('referrer')->orderBy('company')->get();

        if ($request->has('referrer') && $request->input('referrer') != 'all') {
            $clients = Client::with('referrer')->where('referrer_id', $request->input('referrer'))->orderBy('company')->get();
        }

        $parameters = [
            'referrer_options' => $referrers,
            'clients' => $clients
        ];
        return view('reports.referrer')->with($parameters);
    }

    public function conversion(Request $request)
    {
        $types = [
            1 => 'Lead to Prospective',
            2 => 'Prospective to Service Agreed',
            3 => 'Service Agreed to Converted',
            4 => 'First Contact to Converted',
        ];

        switch ($request->input('type')) {
            case 1:
                $target = Activity::whereIn('step_id', [1, 2, 3])->sum('threshold');
                break;
            case 2:
                $target = Activity::whereIn('step_id', [3, 4])->sum('threshold');
                break;
            case 3:
                $target = Activity::whereIn('step_id', [4, 5])->sum('threshold');
                break;
            case 4:
            default:
                $target = Activity::sum('threshold');
                break;
        }

        $end_date = Carbon::now();

        if ($request->has('t')) {
            $end_date = Carbon::parse($request->input('t'));
        }

        $start_year = $end_date->copy()->subYear();
        $start_quarter = $end_date->copy()->subMonths(3);
        $start_month = $end_date->copy()->subMonth();

        $config = Config::first();

        $last_year = Client::where('completed_at', '>=', $start_year)->where('completed_at', '<=', $end_date)->count();
        $last_quarter = Client::where('completed_at', '>=', $start_quarter)->where('completed_at', '<=', $end_date)->count();
        $last_month = Client::where('completed_at', '>=', $start_month)->where('completed_at', '<=', $end_date)->count();

        $parameters = [
            'end_date' => $end_date->toDateString(),
            'types' => $types,
            'clients' => [
                'last_year' => [
                    'actual' => $last_year,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_year),
                    'difference' => $last_year - ($config->onboards_per_day * $end_date->diffInDays($start_year)),
                ],
                'last_quarter' => [
                    'actual' => $last_quarter,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_quarter),
                    'difference' => $last_quarter - ($config->onboards_per_day * $end_date->diffInDays($start_quarter)),
                ],
                'last_month' => [
                    'actual' => $last_month,
                    'target' => $config->onboards_per_day * $end_date->diffInDays($start_month),
                    'difference' => $last_month - ($config->onboards_per_day * $end_date->diffInDays($start_month)),
                ]
            ],
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.conversion')->with($parameters);
    }

    public function feeProposalSent(Request $request)
    {
        $actionable_id = Activity::where('name', 'Fee proposal sent?')->first()->actionable_id;

        $actionable_template_email_data = ActionableTemplateEmailData::with('client.process')->where('actionable_template_email_id', $actionable_id)->select('client_id', DB::raw('MIN(created_at) as min_created_at, MAX(created_at) as max_created_at'))->groupBy('client_id')->orderBy('created_at')->get();

        $parameters = [
            'actionable_template_email_data' => $actionable_template_email_data,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.feeproposalsent')->with($parameters);
    }

    public function loa()
    {
        $loa_actionable_id = Activity::where('name', 'LOA (Letter of Authority)')->first()->actionable_id;

        $actionable_template_email_data = ActionableTemplateEmailData::with('client.process')
            ->where('actionable_template_email_id', $loa_actionable_id)
            ->select('client_id')
            ->distinct()
            ->pluck('client_id');

        $clients = Client::with('process')
            ->whereIn('id', $actionable_template_email_data)
            ->get();

        $parameters = [
            'clients' => $clients,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.loa')->with($parameters);

    }

    public function loe()
    {
        $loe_actionable_id = Activity::where('name', 'LOE (Letter of Engagement)')->first()->actionable_id;

        $actionable_template_email_data = ActionableTemplateEmailData::with('client.process')
            ->where('actionable_template_email_id', $loe_actionable_id)
            ->select('client_id')
            ->distinct()
            ->pluck('client_id');

        $clients = Client::with('process')
            ->whereIn('id', $actionable_template_email_data)
            ->get();

        $parameters = [
            'clients' => $clients,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.loe')->with($parameters);
    }

    public function aml()
    {
        $aml_actionable_id = Activity::where('name', 'AML forms upload')->first()->actionable_id;

        $actionable_document_data = ActionableDocumentData::with('client.process')
            ->where('actionable_document_id', $aml_actionable_id)
            ->distinct()
            ->pluck('client_id');

        $clients = Client::with('process')
            ->whereIn('id', $actionable_document_data)
            ->get();

        $parameters = [
            'clients' => $clients,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.aml')->with($parameters);
    }

    public function crf()
    {
        $crf_actionable_id = Activity::where('name', 'Client Registration Form completed (CRF)')->first()->actionable_id;

        //dd($crf_actionable_id);
        $actionable_dropdown_data = ActionableDropdownData::with('client.process')
            ->where('actionable_dropdown_id', $crf_actionable_id)
            ->select('client_id')
            ->distinct()
            ->pluck('client_id');

        $clients = Client::with('process')
            ->whereIn('id', $actionable_dropdown_data)
            ->get();

        $parameters = [
            'clients' => $clients,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all')
        ];

        return view('reports.crf')->with($parameters);
    }

    public function clientReports(Request $request)
    {
        /*
         * Stages for actions are
         * 1. No
         * 2. In progress
         * 3. Yes
         * 4. N/A
         */

        $actionables = [
            'sloa' => Activity::where('name', 'LOA (Letter of Authority)')->first(),
            'loe' => Activity::where('name', 'LOE (Letter of Engagement)')->first(),
            'crf' => Activity::where('name', 'CRF (Client Registration Form) completed?')->first(),
            'aml' => Activity::where('name', 'AML Forms received?')->first()
        ];

        $dependants = [
            'sloa' => [],
            'loe' => [],
            'crf' => [],
            'aml' => []
        ];

        //find the actionable data for each activity dependency
        foreach ($actionables as $actionable_key => $actionable) {
            if (!is_null($actionable->dependant_activity_id)) {
                array_push($dependants[$actionable_key], $actionable->dependant->actionable->data);
            }

            //todo optimise so that if they all have the same dependendant it should reuse the dependency chain
            $current_dependant = ($actionable->dependant_activity_id) ? $actionable->dependant : null;
            while (!is_null($current_dependant->dependant_activity_id)) {
                array_push($dependants[$actionable_key], $current_dependant->dependant->actionable->data);
                $current_dependant = $current_dependant->dependant;
            }
        }

        //get all data for completed rows of the fields
        $completed = [
            'sloa' => ActionableDocumentData::where('actionable_document_id', $actionables['sloa']->actionable_id)
                ->select('client_id')
                ->distinct()
                ->pluck('client_id', 'client_id'),
            'loe' => ActionableTemplateEmailData::where('actionable_template_email_id', $actionables['loe']->actionable_id)
                ->select('client_id')
                ->distinct()
                ->pluck('client_id', 'client_id'),
            'crf' => ActionableBooleanData::where('actionable_boolean_id', $actionables['crf']->actionable_id)
                ->select('client_id', 'data')
                ->distinct()
                ->pluck('data', 'client_id'),
            'aml' => ActionableTemplateEmailData::where('actionable_template_email_id', $actionables['aml']->actionable_id)
                ->select('client_id')
                ->distinct()
                ->pluck('client_id', 'client_id'),
        ];


        $client_data = new Collection();

        $clients = Client::select('company', 'id','process_id', 'is_progressing')->with('process.steps.activities.actionable.data');

        //if company name query filter on that
        if($request->has('q') && $request->input('q') != ''){
            $clients->where('company','LIKE','%'.$request->input('q').'%');
        }

        $clients = $clients->get();

        //loop clients
        foreach ($clients as $client) {

            //if there is a completed data row set it to complete else set to not complete
            $sloa = (isset($completed['sloa'][$client->id])) ? 1 : 2;
            $loe = (isset($completed['loe'][$client->id])) ? 1 : 2;
            //additionally test if data is true
            $crf = (isset($completed['crf'][$client->id]) && $completed['crf'][$client->id] == true) ? 1 : 2;
            $aml = (isset($completed['aml'][$client->id])) ? 1 : 2;

            //if incomplete test if in progress
            if ($sloa == 2) {
                foreach ($dependants['sloa'] as $dependant) {
                    if (count($dependant->where('client_id', $client->id))) {
                        $sloa = 3;
                        break;
                    }
                }
            }

            if ($loe == 2) {
                foreach ($dependants['loe'] as $dependant) {
                    if (count($dependant->where('client_id', $client->id))) {
                        $loe = 3;
                        break;
                    }
                }
            }

            if ($crf == 2) {
                foreach ($dependants['crf'] as $dependant) {
                    if (count($dependant->where('client_id', $client->id))) {
                        $crf = 3;
                        break;
                    }
                }
            }

            if ($aml == 2) {
                foreach ($dependants['aml'] as $dependant) {
                    if (count($dependant->where('client_id', $client->id))) {
                        $aml = 3;
                        break;
                    }
                }
            }


            $current_step = $client->getCurrentStep();

            //add to array
            $client_data->push([
                'name' => $client->company,
                'id' => $client->id,
                'is_progressing' => $client->is_progressing,
                'step' => $current_step['name'],
                'step_id' => $current_step['id'],
                'sloa' => $sloa,
                'loe' => $loe,
                'crf' => $crf,
                'aml' => $aml,
                'reports' => $request->has('reports')?$request->input('reports'):1
            ]);
        }

        //if step filter is provided, filter on the provided step
        if ($request->has('s') && $request->input('s') != 'all' && $request->input('s') != '') {
            $selected_step = $request->input('s');
            $client_data = $client_data->filter(function ($client) use ($selected_step) {
                return $client['step_id'] == $selected_step;
            });
        }

        //if filter is provided, filter on the provided stage
        if($request->has('sloa') && $request->input('sloa') !=0 ){
            $sloa_input = $request->input('sloa');

            $client_data = $client_data->filter(function ($client) use ($sloa_input) {
                return $client['sloa'] == $sloa_input;
            });
        }

        if($request->has('loe') && $request->input('loe') !=0 ){
            $loe_input = $request->input('loe');

            $client_data = $client_data->filter(function ($client) use ($loe_input) {
                return $client['loe'] == $loe_input;
            });
        }

        if($request->has('crf') && $request->input('crf') !=0 ){
            $crf_input = $request->input('crf');

            $client_data = $client_data->filter(function ($client) use ($crf_input) {
                return $client['crf'] == $crf_input;
            });
        }

        if($request->has('aml') && $request->input('aml') !=0 ){
            $aml_input = $request->input('aml');

            $client_data = $client_data->filter(function ($client) use ($aml_input) {
                return $client['aml'] == $aml_input;
            });
        }

        if($request->has('reports') && $request->input('reports') !=-1 ){
            $reports_input = $request->input('reports');

            $client_data = $client_data->filter(function ($client) use ($reports_input) {
                return $client['is_progressing'] == $reports_input;
            });
        }
        else
        {
            $reports_input = 1;
            $client_data = $client_data->filter(function ($client) use ($reports_input) {
                return $client['is_progressing'] == $reports_input;
            });
        }

        $parameters = [
            'clients' => $client_data,
            'options' => [0 => 'Select', 1 => 'Yes', 2 => 'No', 3 => 'In Progress'],
            'steps' => Step::orderBy('process_id')->orderBy('order')->pluck('name', 'id')->prepend('All steps', 'all')
        ];

        return view('reports.clientreports')->with($parameters);
    }

    public function create()
    {
        $parameters = [
            'activities' => Activity::orderBy('name')->pluck('name', 'id')
        ];
        return view('reports.create')->with($parameters);
    }

    public function store(Request $request)
    {
        $report = new Report;
        $report->name = $request->input('name');
        $report->activity_id = $request->input('activity');
        $report->user_id = auth()->id();
        $report->save();

        return redirect(route('reports.index'))->with('flash_success', 'Report created successfully');
    }

    public function edit($reportid)
    {
        $parameters = [
            'reports' => Report::where('id','=',$reportid)->get(),
            'activities' => Activity::orderBy('name')->pluck('name', 'id')
        ];

        return view('reports.edit')->with($parameters);
    }

    public function update(UpdateReportRequest $request, $reportid){

        $report = Report::find($reportid);
        $report->name = $request->input('name');
        $report->activity_id = $request->input('activity');
        $report->user_id = auth()->id();
        $report->save();

        return redirect(route('reports.index'))->with('flash_success', 'Report saved successfully');
    }

    public function destroy($id){
        Report::destroy($id);

        return redirect(route('reports.index'))->with('flash_success', 'Report deleted successfully');
    }

}