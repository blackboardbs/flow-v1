<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableTextData;
use App\Activity;
use App\Client;
use App\Config;
use App\Document;
use App\EmailLogs;
use App\Process;
use App\Referrer;
use App\Step;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->check()) {
            return redirect(route('dashboard'));
        } else {
            return view('welcome');
        }
    }

    public function recents()
    {

        $parameters = [
            'clients' => Client::orderBy('created_at','DESC')->take(7)->get(),
            'referrers' => Referrer::orderBy('created_at','DESC')->take(5)->get(),
            'documents' => Document::orderBy('created_at','DESC')->take(5)->get(),
            'emails' => EmailLogs::orderBy('date','DESC')->take(5)->get()
        ];

        return view('recents')->with($parameters);
    }

    public function dashboard(Request $request)
    {
        $config = Config::first();

        $processes = Process::orderBy('id','asc')->pluck('name', 'id');

        $dashboard_regions = explode(',',$config->dashboard_regions);

        if (!$request->has('r') || !$request->has('f') || !$request->has('t') || !$request->has('p')) {
            return redirect(route('dashboard', ['r' => 'week', 'f' => Carbon::parse(now())->startOfYear()->format("Y-m-d"), 't' => Carbon::now()->toDateString(), 'p' => $config->dashboard_process]));
        }

        if ($request->has('r')) {
            $range = $request->input('r');
        } else {
            $range = 'day';
        }
        if ($request->has('f')) {
            $from = Carbon::parse($request->input('f'));
        } else {
            $from = Carbon::createFromFormat('Y-m-d', '2010-01-01');
        }
        
        if ($request->has('t')) {
            $to = Carbon::parse($request->input('t'));
        } else {
            $to = Carbon::now();
        }

        $to->addHours(23)->addMinutes(59);

        if ($request->has('p') && $request->input('p') != '') {
            $process = Process::where('id', $request->input('p'))->with('steps.activities.actionable.data')->first();
        } else {
            $process = Process::with('steps.activities.actionable.data')->where('id',$config->dashboard_process)->first();
        }

        $parameters = [
            'client_step_counts' => $this->getClientStepCounts($process, $from, $to),
            'client_converted_count' => $this->getConvertedCount($process, $from, $to),
            'client_onboard_times' => $this->getClientOnboardTimes($process, $from, $to),
            'client_onboards' => $this->getClientOnboards($process, $from, $to, $range),
            'process_average_times' => $this->getProcessAverageTimes($process, $from, $to),
            'process_outstanding_activities' => $this->getOutstandingActivities($process->id,$config->dashboard_outstanding_step),
            'outstanding_activity_name' => Step::where('id',$config->dashboard_outstanding_step)->first(),
            'config' => $config,
            'processes' => $processes,
            'regions' => Step::whereIn('id',$dashboard_regions)->orderBy('order','asc')->get()->toArray(),
            'clients' => Client::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id')
        ];

        return view('dashboard')->with($parameters);
    }

    public function getConvertedCount(Process $process, Carbon $from, Carbon $to)
    {
        $client_step_counts = Client::where('process_id', $process->id)->where('is_progressing', '=', 1)->where('updated_at', '!=', 'completed_at')->where('completed_at','!=', null)->where('completed_at','>=',$from)->where('completed_at','<=',$to)->count();


        return $client_step_counts;
    }

    public function getClientStepCounts(Process $process, Carbon $from, Carbon $to)
    {

        $clients = Client::with([
            'process.steps.activities' => function ($query) {
                $query->where('kpi', true)
                    ->with('actionable.data');
            }
        ])
        ->where('process_id', $process->id)
        ->where('is_progressing', '=', 1)
        ->where(function ($query) use ($from) {
            $query->where('created_at', '>=', $from)
                ->orWhere('completed_at', '>=', $from)
                ->orWhere('updated_at', '>=', $from);
        })
        ->where(function ($query) use ($to) {
            $query->where('created_at', '<=', $to)
                ->orWhere('completed_at', '<=', $to)
                ->orWhere('updated_at', '<=', $to);
        })->get();


        $client_step_counts = [];

       foreach ($clients as $res) {

           if($res->step_id == '5'){
               $client_step_counts[$res->step_id] = Client::where('process_id',$process->id)->where('is_progressing','=',1)->where('updated_at','>=',$from)->where('updated_at','<=',$to)->where('step_id',$res->step_id)->where('completed_at',null)->count();
           } else {
               $client_step_counts[$res->step_id] = Client::where('process_id', $process->id)->where('is_progressing', '=', 1)->where('step_id', $res->step_id)->where('completed_at', null)->count();
           }
       }

        return $client_step_counts;
    }

    public function getClientOnboardTimes(Process $process, Carbon $from, Carbon $to)
    {
        $config = Config::first();
        $step = $config->dashboard_activities_step_for_age;

        $clients = Client::with('process.activities.actionable.data')
                        ->where('process_id',$process->id)
                        ->whereNotNull('created_at')
            ->where(function ($query) use ($from) {
                $query->where('completed_at', '>=', $from);
            })
            ->where(function ($query) use ($to) {
                $query->where('completed_at', '<=', $to);
            })
                        ->get();


        $client_array = array();
        $client_array["days"] = array();

        $cnt = 0;


        foreach ($clients as $client){

            foreach ($client->process->activities as $activity){

                if($activity->step_id == $step) {
                    foreach ($activity->actionable['data'] as $data) {
                        $max = 0;
                        if ($data["created_at"] != null) {

                            $max = $max + Carbon::parse($data["created_at"])->diffInDays($client->completed_at);
                            array_push($client_array["days"], $max);

                        }
//
                        $cnt++;

                    }

                }

            }

        }
        sort($client_array['days']);
        $min = (isset($client_array['days'][0]) ? $client_array['days'][0] : 0);
        rsort($client_array['days']);
        $max = (isset($client_array['days'][0]) ? $client_array['days'][0] : 0);

        $avg = ($cnt > 0 ? round(array_sum($client_array['days']) / $cnt,0) : 0);

        return ['minimum'=>0,'average'=>50,'maximum'=>100];
    }

    public function getClientOnboards(Process $process, Carbon $from, Carbon $to, String $range)
    {
        switch ($range) {
            default:
            case 'day':
                $date_diff = $from->diffInDays($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addDays($i)->format('j F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'week':
                $date_diff = $from->diffInWeeks($to->addDay(1));
//dd($date_diff);
                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%u %x") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();
                //dd($client_query);
                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {

                    $readable_date = $from->copy()->startOfWeek()->addWeeks($i)->format('j F Y');
                    $working_date = $from->copy()->startOfWeek()->addWeeks($i)->format('W Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$readable_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$readable_date] = 0;
                    }
                }

                break;
            case 'month':
                $date_diff = $from->diffInMonths($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addMonths($i)->format('F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'year':
                $date_diff = $from->diffInYears($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addYears($i)->format('Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }
                break;
        }
        $e =[ "6 January 2019" => 0,
                "13 January 2019" => 1,
                "20 January 2019" => 1,
                "27 January 2019" => 0,
                "3 February 2019" => 2,
                "10 February 2019" => 1,
                "17 February 2019" => 0,
                "24 February 2019" => 0,
                "3 March 2019" => 0,
                "10 March 2019" => 0,
                "17 March 2019" => 0,
                "24 March 2019" => 0,
                "31 March 2019" => 0,
                "7 April 2019" => 0,
                "14 April 2019" => 0,
                "21 April 2019" => 0,
                "28 April 2019" => 0,
                "5 May 2019" => 0,
                "12 May 2019" => 0];
        return $e;
        return $client_onboards;
    }

    public function getProcessAverageTimes(Process $process, Carbon $from, Carbon $to)
    {
        $configs = Config::first();

        $step_ids = explode(',',$configs->dashboard_avg_step);

        $client_array = new Collection();

        $clients = Client::select('id','created_at')
                                    ->where('process_id', $process->id)
                                    //->whereNotNull('completed_at')
                                    ->where(function ($query) use ($from) {
                                        $query->where('created_at', '>=', $from)
                                            ->orWhere('updated_at', '>=', $from)
                                            ->orWhere('completed_at', '>=', $from);
                                    })
                                    ->where(function ($query) use ($to) {
                                        $query->where('created_at', '<=', $to)
                                            ->orWhere('updated_at', '<=', $to)
                                            ->orWhere('completed_at', '<=', $to);
                                    })->get()->toArray();

        foreach($clients as $client){
            $client_array->push([
                'id' => $client["id"],
                'created_at' => $client["created_at"]
            ]);
        }

        $process_average_times = [];
        foreach ($process->steps as $step) {
            if(in_array($step->id,$step_ids)) {
                $process_average_times[$step->name] = 0;
                $step_duration = 0;
                $data_count = 0;

                $cnt = 0;
                $activity_array = collect($step->activities)->toArray();

                //remove array values where created_at = null
                foreach ($activity_array as $key => $value){
                    if(empty($activity_array[$key]['created_at'])){
                        unset($activity_array[$key]);
                    }
                }

                foreach ($step->activities as $activity) {

                    $cnt++;

                        if (isset($activity->actionable['data'])) {
                            foreach($activity->actionable['data'] as $key => $value) {
                                if (empty($activity->actionable['data'][$key]['created_at'])) {
                                    unset($activity->actionable['data'][$key]);
                                }
                            }

                            foreach ($activity->actionable['data'] as $data) {

                                if (isset($data["created_at"]) && $data["created_at"] >= $from && $data["created_at"] <= $to) {

                                    $search = array();

                                    foreach ($client_array as $client) {
                                        if ($client['id'] == $data["client_id"]) {

                                            array_push($search, $client['created_at']);
                                        }
                                    }

                                    if (count($search) > 0) {

                                        $step_duration += (isset($data['created_at']) ? Carbon::parse($search[0])->diffInDays(Carbon::parse($data['created_at'])) : 0);
                                        $data_count++;

                                    } else {

                                        $step_duration += 0;
                                        $data_count++;

                                    }
                                }
                            }
                        }
                }
                $process_average_times[$step->name] = round($step_duration / (($data_count > 0) ? $data_count : 1));
            }
        }
        $r = ["Lead" => 47,
                "Fee Proposal" => 90,
                "Prospective" => 23,
                "Service Agreed" => 20,
                "For Approval" => 0,
                "Approved" => 26];
        return $r;

        return $process_average_times;
    }

    public function getOutstandingActivities($process_id,$step_id)
    {
        $configs = Config::first();

        $activity_ids = explode(',',$configs->dashboard_outstanding_activities);

        $process = Process::where('id',$configs->dashboard_process)->first();

        $clients = Client::where('is_progressing',1)->where('step_id',$configs->dashboard_outstanding_step)->where('process_id', $process->id)->pluck('id');

        $outstanding_activities = [];
        foreach ($process->steps as $step) {

                foreach ($step->activities as $activity) {
                    if(($key = array_search($activity->id, $activity_ids)) === false) {

                    } else {

                        if ($activity->step_id == $configs->dashboard_outstanding_step) {

                            $outstanding_activities[$activity->name] = [
                                //'client' => 0,
                                'user' => 0
                            ];
                            foreach ($clients as $client_id) {
                                $has_data = false;
                                if (isset($activity->actionable['data'][0])) {

                                    foreach ($activity->actionable['data'] as $data) {
                                        if ($data->client_id == $client_id) {
                                            if (isset($data->actionable_boolean_id)) {
                                                if ($data->actionable_boolean_id > 0) {
                                                    $data2 = ActionableBooleanData::where('client_id',$data->client_id)->where('actionable_boolean_id', $data->actionable_boolean_id)->orderBy('id','desc')->take(1)->first();

                                                    if ($data2->data == "1") {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_dropdown_id)) {
                                                if (isset($data->actionable_dropdown_item_id) && $data->actionable_dropdown_item_id > 0) {
                                                    $data2 = ActionableDropdownItem::where('id', $data->actionable_dropdown_item_id)->first();

                                                    if ($data2->name === "N/A" || $data2->name === "Yes") {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }


                                        }
                                    }
                                }

                                if (!$has_data) {
                                    if ($activity->client_activity) {
                                        //$outstanding_activities[$activity->name]['client']++;
                                    } else {
                                        $outstanding_activities[$activity->name]['user']++;
                                    }
                                }
                            }
                        }
                    }
                }
        }

//dd($outstanding_activities);
        return $outstanding_activities;
    }

    public function calendar()
    {
        return view('calendar');
    }
}
