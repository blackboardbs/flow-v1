<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachment;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableDocumentData;
use App\ActionableTextData;
use App\ActionableTextareaData;
use App\ActionableTemplateEmail;
use App\ActionableTemplateEmailData;
use App\ActionableDocumentEmailData;
use App\ActionActivities;
use App\Actions;
use App\ActionsAssigned;
use App\Activity;
use App\ActivityComment;
use App\ActivityLog;
use App\Client;
use App\ClientActivity;
use App\ClientComment;
use App\ClientCRFForm;
use App\ClientUser;
use App\Config;
use App\Document;
use App\EmailSignature;
use App\EmailTemplate;
use App\Events\NotificationEvent;
use App\Http\Requests\StoreClientFormRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreFollowRequest;
use App\Http\Requests\UpdateClientFormRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Log;
use App\Notification;
use App\Process;
use App\Referrer;
use App\Step;
use App\Template;
use App\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Action;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\TemplateMail;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use App\ClientForm;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\Mail\ClientNotifyMail;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['sendtemplate', 'sendnotification']);
        $this->middleware('auth:api')->only(['sendtemplate', 'sendnotification']);
        $this->middleware('permission:maintain_client')->except(['create', 'store']);

        $this->progress_colours = [
            'not_started' => 'background-color: rgba(64,159,255, 0.15)',
            'started' => 'background-color: rgba(255,255,70, 0.15)',
            'progress_completed' => 'background-color: rgba(60,255,70, 0.15)',
        ];
    }

    public function index(Request $request)
    {
        $config = Config::first();


        $client_arr = array();
        $np = 0;
        $clients = Client::with(['referrer', 'process.steps.activities.actionable.data', 'introducer'])
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'));

        if ($request->has('s') && $request->input('s') == 'deleted') {
            $clients = Client::withTrashed()->with('referrer', 'process.steps.activities.actionable.data', 'introducer')
                ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at),0) as completed_days'))->whereNotNull('deleted_at');
        }
        if($request->has('oa')){
            $activity = Activity::where('step_id',$config->dashboard_outstanding_step)->where('name',$request->input('oa'))->first();
//dd($activity);
            switch($activity->actionable_type){
                case 'App\ActionableDropdown':
                    $data = DB::select("select distinct(a.client_id) from actionable_dropdown_data a left join actionable_dropdown_items b on b.id = a.actionable_dropdown_item_id where a.actionable_dropdown_id = '".$activity->actionable_id."' and (b.name = 'N/A' or b.name = 'Yes')");
//dd($data);
                    $c = array();

                    foreach ($data as $data2){
                        array_push($c,$data2->client_id);
                    }
                    //dd($c);
                    $clients = Client::select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at),0) as completed_days'))
                        ->whereNotIn('id',$c);
                    //dd($data);
                    break;
                case 'App\ActionableBoolean':
                    $data = ActionableBooleanData::where('actionable_boolean_id',$activity->actionable_id)->where('data',1)->pluck('client_id');
//dd($data);
                    $clients = Client::select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at),0) as completed_days'))
                        ->whereNotIn('id',$data);
                    break;
            }
            //dd($activity);
        }
//dd($clients);
        if ($request->has('f') && $request->input('f') != '') {
            $from = Carbon::parse($request->input('f'));
            $clients->where(function ($query) use ($from) {
                $query->where('created_at', '>=', $from)
                    ->orWhere('completed_at', '>=', $from);
            });
        }

        if ($request->has('t') && $request->input('t') != '') {
            $to = Carbon::parse($request->input('t'))->addHours(23)->addMinutes(59);
            $clients->where(function ($query) use ($to) {
                $query->where('created_at', '<=', $to)
                    ->orWhere('completed_at', '<=', $to);
            });
        }

        if ($request->has('p') && $request->input('p') != 'all') {
            $clients->where('process_id', $request->input('p'));
        }

        if ($request->has('c')) {
            if ($request->input('c') == 'no') {
                $clients->whereNull('completed_at');
            }

            if ($request->input('c') == 'yes') {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')) {
                    $clients->whereDate('completed_at','>=',$request->input('f'))->whereDate('completed_at','<=',$request->input('t'));
                } else {
                    $clients->whereNotNull('completed_at');
                }

            }

            if ($request->input('c') == 'progress') {
                $clients->where('is_progressing', 1);
            }

            if ($request->input('c') == 'noprogress') {
                $clients->where('is_progressing', 0);
            }

            if ($request->input('c') == 'approval') {
                $clients->where('needs_approval', true);
            }
        }

        if ($request->has('np')) {
            if ($request->input('np') == 'yes') {
                $clients->whereNull('completed_at')
                    ->where('is_progressing', false)
                    ->orWhere('is_progressing','0');
                $np = 1;
            }
        } else {
            //check if not progressing was selected as a filter variable
            if ($request->input('c') == 'noprogress') {
                $clients->where('is_progressing', false)
                    ->orWhere('is_progressing', '0');

                $np = 1;

                //filter non progressing if dates selected
                if ($request->has('f') && $request->input('f') != '') {
                    $from = Carbon::parse($request->input('f'));
                    $clients->where(function ($query) use ($from) {
                        $query->where('not_progressing_date', '>=', $from);
                    });
                }

                if ($request->has('t') && $request->input('t') != '') {
                    $to = Carbon::parse($request->input('t'))->addHours(23)->addMinutes(59);
                    $clients->where(function ($query) use ($to) {
                        $query->where('not_progressing_date', '<=', $to);
                    });
                }
            } else {
                $clients->where('is_progressing', '1');
            }
        }



        //todo implement concat on company name
        if ($request->has('q') && $request->input('q') != '') {
            $clients->where(function ($query) use ($request) {
                $query->where('company', 'like', "%" . $request->input('q') . "%")
                    ->orWhere('first_name', 'like', "%" . $request->input('q') . "%")
                    ->orWhere('last_name', 'like', "%" . $request->input('q') . "%")
                    ->orWhere('id_number', 'like', "%" . $request->input('q') . "%")
                    ->orWhere('email', 'like', "%" . $request->input('q') . "%");
            });
        }


        if ($request->has('si') && $request->input('si') != '') {
            $direction = 'desc';
            if ($request->has('so') && $request->input('so') == 'a') {
                $direction = 'asc';
            }

            switch ($request->input('si')) {
                case 'name':
                    $clients->orderBy('company', $direction);
                    break;
                case 'email':
                    $clients->orderBy('email', $direction);
                    break;
                case 'contact':
                    $clients->orderBy('contact', $direction);
                    break;
                case 'process':
                    $clients->orderBy('process_id', $direction);
                    break;
                case 'created':
                    $clients->orderBy('created_at', $direction);
                    break;
                case 'completed':
                    $clients->orderBy('completed_at', $direction);
                    break;
                case 'completed_days':
                    $clients->orderBy('completed_days', $direction);
                    break;
                case 'referrer':
                    $clients->orderBy('referrer_id', $direction);
                    break;
                default:
                    $clients->orderBy('company', $direction);
                    break;
            }

        } else {

        }

        $clients = $clients->get();
        //dd($clients);
        //

        $config = Config::select('client_referrer','client_director','client_onboardingm','client_onboardingl')->first();

        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            $selected_step = $request->input('step');
            /*if (in_array($selected_step,$max_step)) {
                return redirect('clients?c=yes');
            }*/
            if ($selected_step == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($selected_step == 1001) {
                return redirect('clients?np=yes');
            }


            if($request->has('c') && $request->input('c') == 'yes') {
                $clients = $clients->filter(function ($client) use ($selected_step) {
                    return $client->step_id == $selected_step;
                });
            } elseif($request->has('c') && $request->input('c') == 'no') {
                $clients = $clients->filter(function ($client) use ($selected_step) {
                    return $client->getCurrentStep()->id == $selected_step;
                });
            } elseif($request->has('c') && $request->input('c') == 'all') {
                $clients = $clients->filter(function ($client) use ($selected_step) {
                    return $client->step_id == $selected_step;
                });
            } else {
                $clients = $clients->filter(function ($client) use ($selected_step) {
                    return $client->step_id == $selected_step;
                });
            }
        }


        foreach ($clients as $result){
            //dd($result);

            $activity = Activity::select('id','actionable_id','actionable_type')->whereIn('id',$config)->get();
            $process_name = null;

            $process = Process::select("name")->where('id',$result->process_id)->get();

            foreach($process as $proc){
                $process_name = $proc->name;
            }

            $introducer = User::select("avatar")->where('id',$result->introducer_id)->get();

            foreach($introducer as $intro){
                $avatar = $intro->avatar;
            }

            $res["id"] = $result->id;
            $res["company"] = ($result->company != null ? $result->company : $result->first_name . ' ' . $result->last_name);
            $res["email"] = $result->email;
            $res["contact"] = $result->contact;
            $res["id_number"] = $result->id_number;
            $res["cif_code"] = $result->cif_code;
            $res["company_registration_number"] = $result->company_registration_number;
            $res["completed_days"] = $result->completed_days;
            $res["is_progressing"] = $result->is_progressing;
            $res["needs_approval"] = $result->needs_approval;
            $res["avatar"] = $avatar;
            $res["introducer"] = $result->introducer_id;
            $res["completed_at"] = $result->completed_at;
            $res["created_at"] = $result->created_at->toDateString();
            $res["non_progressing"] = ($result->not_progressing_date != null ? Carbon::parse($result->not_progressing_date)->toDateString() : '');
            $res["process"] = $process_name;
            $res["step"] = $result->getCurrentStep()->name;


            array_push($client_arr,$res);
        }

        if($request->has('sort') && $request->input('sort') == 'company'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['company'] <=> $item2['company'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['company'] <=> $item1['company'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'step'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['step'] <=> $item2['step'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['step'] <=> $item1['step'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'email'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['email'] <=> $item2['email'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['email'] <=> $item1['email'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'created'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['created_at'] <=> $item2['created_at'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['created_at'] <=> $item1['created_at'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'non_progressing'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['non_progressing'] <=> $item2['non_progressing'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['non_progressing'] <=> $item1['non_progressing'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'completed'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['completed_at'] <=> $item2['completed_at'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['completed_at'] <=> $item1['completed_at'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'duration'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['completed_days'] <=> $item2['completed_days'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['completed_days'] <=> $item1['completed_days'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'introducer'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['introducer'] <=> $item2['introducer'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['introducer'] <=> $item1['introducer'];
                });
            }
        }

        if($request->has('sort') && $request->input('sort') == 'process'){
            if($request->has('direction') && $request->input('direction') == 'asc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['process'] <=> $item2['process'];
                });
            }

            if($request->has('direction') && $request->input('direction') == 'desc') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['process'] <=> $item1['process'];
                });
            }
        }

        if(!$request->has('sort')){
            usort($client_arr, function ($item1, $item2) {
                return $item1['company'] <=> $item2['company'];
            });
        }

        if($request->has('si') && $request->input('si') == 'completed_days') {
            if ($request->has('so') && $request->input('so') == 'a') {
                usort($client_arr, function ($item1, $item2) {
                    return $item1['completed_days'] <=> $item2['completed_days'];
                });
            }

            if ($request->has('so') && $request->input('so') == 'd') {
                usort($client_arr, function ($item1, $item2) {
                    return $item2['completed_days'] <=> $item1['completed_days'];
                });
            }
        }



        $parameters = [
            'np' => $np,
            'clients' => $client_arr,
            'processes' => Process::orderBy('name')->pluck('name', 'id')->prepend('All processes', 'all'),
            'steps' => Step::orderBy('process_id')->orderBy('order')->withTrashed()->pluck('name', 'id')->prepend('All steps', '')
        ];

        return view('clients.index')->with($parameters);
    }

    public function create()
    {
        //TODO show process steps on process selection

        $last_process = Client::where('referrer_id', auth()->id())->orderBy('created_at', 'desc')->first();

        if ($last_process) {
            $last_process = $last_process->process_id;
        } else {
            $last_process = '';
        }

        $processes = Process::where('process_type_id','1')->orderBy('name')->pluck('name', 'id')->prepend('Please Select','0');
        $referrers = Referrer::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        $parameters = [
            'processes' => $processes,
            'last_process' => $last_process,
            'referrers' => $referrers
        ];

        return view('clients.create')->with($parameters);
    }

    public function store(StoreClientRequest $request)
    {
        $client = new Client;
        $client->first_name = $request->input('first_name');
        $client->last_name = $request->input('last_name');
        $client->id_number = $request->input('id_number');
        $client->company = $request->input('company');
        $client->company_registration_number = $request->input('company_registration_number');
        $client->cif_code = $request->input('cif_code');
        $client->email = $request->input('email');
        $client->contact = $request->input('contact');
        $client->referrer_id = $request->input('referrer');
        $client->introducer_id = auth()->id();
        $client->office_id = auth()->user()->office()->id ?? 1;
        $client->process_id = $request->input('process');
        $client->step_id = Step::where('process_id',$request->input('process'))->orderBy('order','asc')->first()->id;
        $client->needs_approval = !auth()->user()->can('maintain_client');
        $client->save();


        $adminuser = User::whereHas('roles', function($q){$q->where('name', 'admin');})->get()->toArray();

        //Mail::to($adminuser)->send(new ClientNotifyMail($client));

        if (auth()->user()->can('maintain_client')) {
            if ($request->session()->pull('platform') === 'desktop')
                return redirect(route('clients.show', $client->id))->with('flash_success', 'Client captured successfully');
            else
                return redirect(route('clients.create'))->with('flash_success', 'Client captured successfully');
        } else {
            if ($request->session()->pull('platform') === 'desktop')
                return redirect()->back()->with('flash_success', 'Client captured successfully');
            else
                return redirect(route('clients.create'))->with('flash_success', 'Client captured successfully');
        }
    }

    public function show(Request $request,$clientid)
    {
        //dd($request->headers->get('referer'));
        if((strpos($request->headers->get('referer'),'reports') !== false) || (strpos($request->headers->get('referer'),'custom_report') !== false)) {
            $request->session()->put('path_route',$request->headers->get('referer'));
            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $client = Client::withTrashed()->find($clientid);
        //$process_progress = $client->getProcessProgress();

        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $client_actual_times = [];
        foreach ($client->process->steps as $step) {
            $client_actual_times[$step->name] = 0;

            foreach ($step->activities as $activity) {
                if (isset($activity->actionable->data[0])) {
                    $client_actual_times[$step->name] += 1;
                }
            }
        }

        $steps = Step::where('process_id', $client->process_id)->orderBy('order', 'asc')->get();
        $c_step_order = Step::where('id', $client->step_id)->withTrashed()->first();
//dd($steps);
        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $client->process->getStageHex(0);
                $step_stage = 0;

                if ($c_step_order->order == $a_step->order) {
                    $progress_color = $client->process->getStageHex(1);
                    $step_stage = 1;
                }

                if ($c_step_order->order > $a_step->order) {
                    $progress_color = $client->process->getStageHex(2);
                    $step_stage = 2;
                }


                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'stage' => $step_stage
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;


        $config = Config::all()->first();

        $referrer = null;
        $director = null;
        $onboardingm = null;
        $onboardingl = null;
        foreach ($process_progress as $key => $step){
            foreach ($step['activities'] as $activity_value) {
                    if ($activity_value['type'] == 'dropdown'){
                        //dd($director);
                        if ($activity_value["id"] == $config->referrer) {
                            $arr = (array)$activity_value['dropdown_items'];
                            $arr2 = (array)$activity_value['dropdown_values'];

                            foreach ((array)$arr as $key => $value) {
                                if (in_array($key, $arr2)) {
                                    if($referrer == null){
                                        $referrer = $value;
                                    } else {
                                        $referrer = $referrer.', '.$value;
                                    }
                                }

                            }
                        }

                        if ($activity_value["id"] == $config->client_director) {
                            $arr = (array)$activity_value['dropdown_items'];
                            $arr2 = (array)$activity_value['dropdown_values'];

                            foreach ((array)$arr as $key => $value) {
                                if (in_array($key, $arr2)) {
                                    if($director == null){
                                        $director = $value;
                                    } else {
                                        $director = $director.', '.$value;
                                    }
                                }

                            }
                        }

                        if ($activity_value["id"] == $config->client_onboardingm) {
                            $arr = (array)$activity_value['dropdown_items'];
                            $arr2 = (array)$activity_value['dropdown_values'];

                            foreach ((array)$arr as $key => $value) {
                                if (in_array($key, $arr2)) {
                                    if($onboardingm == null){
                                        $onboardingm = $value;
                                    } else {
                                        $onboardingm = $onboardingm.', '.$value;
                                    }
                                }

                            }
                        }

                        if ($activity_value["id"] == $config->client_onboardingl) {
                            $arr = (array)$activity_value['dropdown_items'];
                            $arr2 = (array)$activity_value['dropdown_values'];

                            foreach ((array)$arr as $key => $value) {
                                if (in_array($key, $arr2)) {
                                    if($onboardingl == null){
                                        $onboardingl = $value;
                                    } else {
                                        $onboardingl = $onboardingl.', '.$value;
                                    }
                                }

                            }
                        }
                    }
                if ($activity_value["id"] == $config->client_referrer && $referrer == null && isset($activity_value["value"])) {
                    $referrer = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_director && $director == null && isset($activity_value["value"])) {
                    $director = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_onboardingm && $onboardingm == null && isset($activity_value["value"])) {
                    $onboardingm = $activity_value["value"];
                }
                if ($activity_value["id"] == $config->client_onboardingl && $onboardingl == null && isset($activity_value["value"])) {
                    $onboardingl = $activity_value["value"];
                }

            }
        }


        $parameters = [
            'client' => $client,
            //'view_process_dropdown' => Process::with('steps')->pluck('name','id'),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'process_progress' => $process_progress,
            'steps' => $step_data,
            'referrer' => $referrer,
            'director' => $director,
            'onboardingm' => $onboardingm,
            'onboardingl' => $onboardingl,
            //'steps' => $client->process->steps()->get(),
            'client_actual_times' => $client_actual_times,
            'path' => $path,
            'path_route' => $path_route
        ];

        return view('clients.details')->with($parameters);
    }

    public function progress(Client $client)
    {
        $client->with('process.office.area.region.division');

        $parameters = [
            'client' => $client,
            'process_progress' => $client->getProcessProgress(),
            'steps' => Step::all(),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id')
        ];

        return view('clients.progress')->with($parameters);
    }

    public function stepProgress(Request $request,$clientid, Process $process, Step $step)
    {
        if($request->session()->get('path_route') != null) {

            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $process->getStageHex(0);


        if($client->step_id == $step->id)
            $client_progress = $process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $process->getStageHex(2);

        $steps = Step::where('process_id', $process->id)->orderBy('order','asc')->get();

        $step_data = [];

        if($process->id == $client->process_id) {
            $c_step_order = Step::where('id', $client->step_id)->where('process_id', $process->id)->withTrashed()->first();

            foreach ($steps as $a_step) {


                $progress_color = $client->process->getStageHex(0);
                $step_stage = 0;

                if ($c_step_order["order"] == $a_step->order) {
                    $progress_color = $client->process->getStageHex(1);
                    $step_stage = 1;
                }

                if ($c_step_order["order"] > $a_step->order) {
                    $progress_color = $client->process->getStageHex(2);
                    $step_stage = 2;
                }

                /*if ($c_step_order["order"] == $a_step->order && $client->completed_at != null && $a_step->process_id == $client->process_id) {
                    $progress_color = $client->process->getStageHex(2);
                    $step_stage = 2;
                }*/

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'stage' => $step_stage
                ];

                array_push($step_data, $tmp_step);
            }
        } else {


            //dd($highest_process_step);
            foreach ($steps as $a_step) {
                $c_step_order = $client->getClientHighestStepOrder($process);
                //dd($c_step_order);
                $progress_color = $client->process->getStageHex(0);
                $step_stage = 0;

                if (($c_step_order+1) == $a_step->order) {
                    $progress_color = $client->process->getStageHex(1);
                    $step_stage = 1;
                }

                if (($c_step_order+1) > $a_step->order) {
                    $progress_color = $client->process->getStageHex(2);
                    $step_stage = 2;
                }

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'stage' => $step_stage
                ];

                array_push($step_data, $tmp_step);

            }
        }





        $max_step = Step::orderBy('order','desc')->where('process_id', $process->id)->first();

        //get next step id where order == current order+1
        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $process->id)->where('order','>',$step->order)->whereNull('deleted_at')->first();
        //dd($step->id);
        $next_step = $step->id;
        //dd($step->id);
        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);
        //$next_step = $step->id + 1;

        //dd($step);
        //dd(User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'));
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');


        $activities_comments = DB::select("SELECT activity_id,COUNT(activity_id) as activity_count,`private` as pr,user_id FROM client_activities_comments where client_id = '".$client->id."' and deleted_at is null group by activity_id,private,user_id");

        //dd($activities_comments);
        $activity_comment = array();


        foreach ($activities_comments as $value) {

            //$activity_comment[$value->activity_id] = $value->activity_count;
            //if(isset($activity_comment[$value->activity_id]) && $activity_comment[$value->activity_id] > 0) {
            if ((isset($value->pr) && $value->pr > 0) && (isset($value->user_id) && Auth::id() == $value->user_id)) {
                $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                //dd($activity_comment[$value->activity_id]);
            } else {
                //} else {
                if (isset($value->pr) && $value->pr == 0){
                    $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                }
                //dd($activity_comment[$value->activity_id]);
            }

        }

        $actions = ActionsAssigned::with('client')->whereHas('activity', function($q){
            $q->where('status',0);
        })->where('completed',0)->where('clients',$client->id)->orderBy('id','desc')->get();

        $configs = Config::first();

        $activities = [];
        $auser_array = [];
        $aduedate_array = array();

        foreach ($actions as $activity) {
            $split_users = explode(',', $activity->users);

            if ($activity->client) {

                foreach ($activity->activity as $activity_id) {
                    $auser_array = array();
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {

                        //Get the current timestamp.
                        $now = strtotime(now());

                        //Calculate the difference.
                        $difference = $now - strtotime($activity->due_date);

                        //Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days < -$configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(2);
                        } elseif ($days <= $configs->action_threshold) {
                            if (Carbon::parse(now()) >= Carbon::parse($activity->due_date)->subDay($configs->action_threshold)) {
                                $class = $activity->client->process->getStageHex(2);
                            } else {
                                $class = $activity->client->process->getStageHex(1);
                            }
                        } elseif ($days > $configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(0);
                        } else {
                            $class = $activity->client->process->getStageHex(0);
                        }

                        if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        } elseif (Auth::check() && Auth::user()->is("admin")) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        }
                    }
                }
            }
        }

        $parameters = [
            'step_dropdown' => Step::where('process_id',$client->process_id)->pluck('name','id'),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'process' => Process::whereHas('steps')->where('process_type_id','1')->orderBy('name','asc')->pluck('name','id')->prepend('Please Select','0'),
            'activities' => ($activities != null ? $activities : []),
            'action_due_date' => ($aduedate_array != null ? $aduedate_array : []),
            'client' => $client,
            'activity_comment' => $activity_comment,
            'step' => $step,
            'active' => $step,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'process_progress' => $client->getProcessStepProgress($step),
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
            'path' => $path,
            'path_route' => $path_route
        ];
//dd($activity_comment);
        return view('clients.stepprogress')->with($parameters);
    }

    public function stepProgressAction(Request $request,$clientid,Step $step,$action_id )
    {
        if($request->session()->get('path_route') != null) {

            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        //dd($step["activities"]);
        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();
//dd($step["activities"]);
        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($c_step_order->order == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }


            if($c_step_order->order > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);

        endforeach;


        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

        //get next step id where order == current order+1
        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();
//dd($step->id);
        $next_step = $step->id;
        //dd($step->id);
        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);
        //$next_step = $step->id + 1;

        //dd($step);
        //dd(User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'));
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');


        $activities_comments = DB::select("SELECT activity_id,COUNT(activity_id) as activity_count,`private` as pr,user_id FROM client_activities_comments where client_id = '".$client->id."' and deleted_at is null group by activity_id,private,user_id");

//dd($activities_comments);
        $activity_comment = array();


        foreach ($activities_comments as $value) {

            //$activity_comment[$value->activity_id] = $value->activity_count;
            //if(isset($activity_comment[$value->activity_id]) && $activity_comment[$value->activity_id] > 0) {
            if ((isset($value->pr) && $value->pr > 0) && (isset($value->user_id) && Auth::id() == $value->user_id)) {
                $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                //dd($activity_comment[$value->activity_id]);
            } else {
                //} else {
                if (isset($value->pr) && $value->pr == 0){
                    $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                }
                //dd($activity_comment[$value->activity_id]);
            }



//            $activity_comment[$value->activity_id] = $activity_count;
            //array_push($activity_comment,$activity_comment[$value->activity_id]);
        }

        $actions = ActionsAssigned::with('client')->whereHas('activity', function($q){
            $q->where('status',0);
        })->where('completed',0)->where('clients',$clientid)->get();

        $configs = Config::first();

        $activities = [];

        $aduedate_array = array();

        foreach ($actions as $activity) {
            $split_users = explode(',', $activity->users);

            if ($activity->client) {

                foreach ($activity->activity as $activity_id) {
                    $auser_array = array();
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {

                        //Get the current timestamp.
                        $now = strtotime(now());

                        //Calculate the difference.
                        $difference = $now - strtotime($activity->due_date);

                        //Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days < -$configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(2);
                        } elseif ($days <= $configs->action_threshold) {
                            if (Carbon::parse(now()) >= Carbon::parse($activity->due_date)->subDay($configs->action_threshold)) {
                                $class = $activity->client->process->getStageHex(2);
                            } else {
                                $class = $activity->client->process->getStageHex(1);
                            }
                        } elseif ($days > $configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(0);
                        } else {
                            $class = $activity->client->process->getStageHex(0);
                        }

                        if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        } elseif (Auth::check() && Auth::user()->is("admin")) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        }
                    }
                }
            }
        }

        $parameters = [
            'activities' => $activities,
            'client' => $client,
            'activity_comment' => $activity_comment,
            'step' => $step,
            'active' => $step,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'process_progress' => $client->getProcessStepProgress($step),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
            'path' => $path,
            'path_route' => $path_route
        ];
//dd($activity_comment);
        return view('clients.stepprogressactions')->with($parameters);
    }

    public function activityProgress($clientid, Step $step)
    {
        //dd($step);
        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
//dd($steps);
        $process_progress = $client->getProcessStepProgress($step);
        //dd($process_progress);
        $completed = "";
        $not_completed = "";
        $stepw = $process_progress[0];
        //dd($step['order']);
        foreach ($stepw['activities'] as $activity):
            if (isset($activity['value'])) {
                $completed .= '<li>' . $activity['name'] . '</li>';
            } else {
                $not_completed .= '<li>' . $activity['name'] . '</li>';
            }
        endforeach;

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($client->step_id == $a_step->id) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if($client->step_id > $a_step->id) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        /*if (count($step_data) > 1) {
            foreach ($step_data as $key => $part) {
                $sort[$key] = strtotime($part['order']);
            }

            array_multisort($sort, SORT_ASC, $step_data);
        }*/

        $max_step = Step::where('process_id', $client->process_id)->max('id');

        $next_step = $step->id;
        if($next_step == $max_step)
            $next_step = $max_step;
        else
            $next_step = $step->id + 1;

        //dd();
        //dd(User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'));
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');
        $parameters = [
            'client' => $client,
            'step' => $step,
            'next_step' => $next_step,
            'process_progress' => $process_progress,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'completed' => $completed,
            'not_completed' => $not_completed,
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options
        ];

        return view('clients.activityprogress')->with($parameters);
    }

    public function actions(Request $request,$clientid, Step $step){

        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();
//dd($step["activities"]);
        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($c_step_order->order == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if($c_step_order->order > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);

        endforeach;
        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

        //$n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();
        $next_step = $step->id;
        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');


        $activities_comments = DB::select("SELECT activity_id,COUNT(activity_id) as activity_count,`private` as pr,user_id FROM client_activities_comments where client_id = '".$client->id."' and deleted_at is null group by activity_id,private,user_id");
        $activity_comment = array();
        foreach ($activities_comments as $value) {
            if ((isset($value->pr) && $value->pr > 0) && (isset($value->user_id) && Auth::id() == $value->user_id)) {
                $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
            } else {
                if (isset($value->pr) && $value->pr == 0){
                    $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                }
            }
        }

        /* Get the raw data for assigned activities */
        $result = ActionsAssigned::with('client')->whereHas('activity', function($q) use ($clientid){
            $q->where('status','0')
                ->where('client_id',$clientid);
        })->where('completed',0)->get();
        $configs = Config::first();

        /*  Separate out the collection into an array we can manipulate better in the template */
        $activities = [];

        $due_date = '';

        foreach($result as $activity) {

            // User IDs are comma-separated in the database
            $split_users = explode(',', $activity->users);
            $auser_array = array();
            if ($activity->client) {
                // List of Activities
                foreach ($activity->activity as $activity_id) {
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {
                        $clientid = $activity->client["id"];
                        $parent_activity = Activity::withTrashed()->with(['actionable.data' => function ($query) use ($clientid) {
                            $query->where('client_id', $clientid);
                        }])->where('id', $activity_id->activity_id)->first();

                        if(isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"]) && $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"] > $activity->due_date){
                            $due_date = $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"];
                        } else {
                            $due_date = $activity->due_date;
                        }

                        if (isset($parent_activity->actionable['data'][0])) {
                            //foreach ($parent_activity->actionable['data'] as $data) {

                            //Get the current timestamp.
                            $now = strtotime(now());

                            //Calculate the difference.
                            $difference = $now - strtotime($due_date);

                            //Convert seconds into days.
                            $days = floor($difference / (60 * 60 * 24));

                            if ($days < -$configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(2);
                            } elseif ($days <= $configs->action_threshold) {
                                if (Carbon::parse(now()) > Carbon::parse($due_date)) {
                                    $class = $activity->client->process->getStageHex(0);
                                } elseif (Carbon::parse(now()) >= Carbon::parse($due_date)->subDay($configs->action_threshold)) {
                                    $class = $activity->client->process->getStageHex(1);
                                }
                            } elseif ($days > $configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(0);
                            } else {
                                $class = $activity->client->process->getStageHex(0);
                            }

                            if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'class' => $class];
                            } elseif (Auth::check() && Auth::user()->is("admin")) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            }
                        } else {

                            $now2 = strtotime(now());

                            //Calculate the difference.
                            $difference2 = $now2 - strtotime($due_date);

                            //Convert seconds into days.
                            $days2 = floor($difference2 / (60 * 60 * 24));

                            if ($days2 < -$configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(2);
                            } elseif ($days2 <= $configs->action_threshold) {
                                if (Carbon::parse(now()) > Carbon::parse($due_date)) {
                                    $class = $activity->client->process->getStageHex(0);
                                } elseif (Carbon::parse(now()) >= Carbon::parse($due_date)->subDay($configs->action_threshold)) {
                                    $class = $activity->client->process->getStageHex(1);
                                }
                            } elseif ($days2 > $configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(0);
                            } else {
                                $class = $activity->client->process->getStageHex(0);
                            }

                            if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            } elseif (Auth::check() && Auth::user()->is("admin")) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            }
                        }
                    }
                }
            }
        }
        //dd($actions_data);
        $parameters = [

            'actions_data' => $activities,
            'actions' => Actions::where('status',1)->pluck('name','id')->prepend('Please select' , 0),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'client' => $client,
            'activity_comment' => $activity_comment,
            'step' => $step,
            'active' => $step,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'process_progress' => $client->getProcessStepProgress($step),
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users_drop' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
        ];

        return view('clients.actions.index')->with($parameters);
    }

    public function documents(Request $request,Client $client)
    {
        if((strpos($request->headers->get('referer'),'reports') !== false) || (strpos($request->headers->get('referer'),'custom_report') !== false)) {
            $request->session()->put('path_route',$request->headers->get('referer'));
            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($client->step_id == $a_step->id) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if($client->step_id > $a_step->id) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        return view('clients.documents')->with([
            'client' => $client->load('documents.user'),
            'process_progress' => $process_progress,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'path' => $path,
            'path_route' => $path_route,

        ]);
    }

    public function edit(Client $client)
    {
        $last_process = Client::where('referrer_id', auth()->id())->orderBy('created_at', 'desc')->first();

        if ($last_process) {
            $last_process = $last_process->process_id;
        } else {
            $last_process = '';
        }

        $processes = Process::orderBy('name')->pluck('name', 'id');
        $referrers = Referrer::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');

        $parameters = [
            'client' => $client,
            'processes' => $processes,
            'last_process' => $last_process,
            'referrers' => $referrers
        ];

        return view('clients.edit')->with($parameters);
    }

    public function update(UpdateClientRequest $request, Client $client)
    {
        //check if client was in the selected process before
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($client) {
            $query->where('client_id', $client);
        }])->where('process_id',$request->input('process'))->get();

        $process_activities=array();
        foreach ($highest_step_id as $highest_step) {
            if($client->isStepActivitiesCompleted(Step::find($highest_step->id)) && $client->completed_at == null) {
                foreach ($highest_step->process->activities as $activity){
                //get the step ids of the process if the client was previously in the selected process

                    foreach ($activity->actionable['data'] as $data) {
                        //push the step id into the array
                        array_push($process_activities, ["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                    }
                }
            }
        }
        //sort the array in descending order
        rsort($process_activities);
        //dd($process_activities);
        $client->first_name = $request->input('first_name');
        $client->last_name = $request->input('last_name');
        $client->id_number = $request->input('id_number');
        $client->company = $request->input('company');
        $client->company_registration_number = $request->input('company_registration_number');
        $client->cif_code = $request->input('cif_code');
        $client->email = $request->input('email');
        $client->contact = $request->input('contact');
        $client->referrer_id = $request->input('referrer');
        //$client->introducer_id = auth()->id();
        //$client->office_id = auth()->user()->office()->id;
        $client->process_id = $request->input('process');
        $client->step_id = (isset($process_activities[0]) && $process_activities[0] != null ? $process_activities[0]["step_id"]+1 : Step::withTrashed()->where('process_id',$request->input('process'))->orderBy('order','asc')->first()->id);
        $client->save();

        return redirect()->back()->with(['flash_success' => "Client updated successfully."]);
    }

    public function destroy($id)
    {
        Client::destroy($id);
        return redirect()->route("clients.index")->with('flash_success','Client deleted successfully');

    }

    public function restore($id)
    {
        Client::onlyTrashed()->where('id',$id)->restore();

        return redirect('clients')->with('flash_success','Client successfully restored');

    }

    public function storeProgress(Client $client, Request $request)
    {
        //$client->load('process.steps.activities.actionable.data');
        //dd($request->input('step_id'));
        //foreach ($client->process->steps as $step) {

        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;
            $step = Step::find($request->input('step_id'));
            $step->load(['activities.actionable.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);
//dd($request);
            $all_activities_completed = false;
            foreach ($step->activities as $activity) {
                if(is_null($request->input($activity->id))){
                    if($request->input('old_'.$activity->id) != $request->input($activity->id)){

                        if(is_array($request->input($activity->id))){

                            $old = explode(',',$request->input('old_'.$activity->id));
                            $diff = array_diff($old,$request->input($activity->id));
                            //dd($diff);

                            foreach($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_'.$activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_'.$activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\ActionableBoolean':
                                ActionableBooleanData::where('actionable_boolean_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableDate':
                                ActionableDateData::where('actionable_date_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableText':
                                dd($activity->actionable_type);
                                ActionableTextData::where('actionable_text_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableTextarea':
                                dd($activity->actionable_type);
                                ActionableTextareaData::where('actionable_textarea_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableDropdown':
                                ActionableDropdownData::where('actionable_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if($request->input('old_'.$activity->id) == $request->input($activity->id)){
                        continue;
                    }
                    if(is_array($request->input($activity->id))){

                        $old = explode(',',$request->input('old_'.$activity->id));
                        $diff = array_diff($old,$request->input($activity->id));
                        //dd($diff);

                        foreach($request->input($activity->id) as $key => $value) {
                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_'.$activity->id);

                        $activity_log = new ActivityLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->activity_id = $activity->id;
                        $activity_log->activity_name = $activity->name;
                        $activity_log->old_value = $request->input('old_'.$activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    //dd($request);
                    switch ($activity->actionable_type) {
                        case 'App\ActionableBoolean':
                            ActionableBooleanData::where('client_id',$client->id)->where('actionable_boolean_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_boolean_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDate':
                            ActionableDateData::where('client_id',$client->id)->where('actionable_date_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableDateData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_date_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableText':
                            ActionableTextData::where('client_id',$client->id)->where('actionable_text_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableTextData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_text_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableTextarea':
                            ActionableTextareaData::where('client_id',$client->id)->where('actionable_textarea_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableTextareaData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_textarea_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDropdown':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    ActionableDropdownData::insert([
                                        'actionable_dropdown_id' => $activity->actionable_id,
                                        'actionable_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    ActionableDropdownData::where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id)->whereIn('actionable_dropdown_item_id',$diff)->delete();
                                }



                            }
                            break;
                        /*case 'App\ActionableMultipleAttachment':
                            ActionableMultipleAttachmentData::insert([
                                'email' => $request->input($activity->id),
                                'template_id' => $request->input('template_email_'.$activity->id),
                                'actionable_ma_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120
                            ]);
                            break;*/
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }

            //Move process step to the next step if all activities completed
            $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

            $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

            //dd($n_step);
            /*if($client->isStepActivitiesCompleted($step) && $client->step_id == $request->input('step_id') && $client->step_id < $max_step->id){
                $client = Client::find($client->id);
                $client->step_id = $n_step->id;
                $client->save();
            }*/
            if($client->step_id == $request->input('step_id')){
                $client = Client::find($client->id);
                $client->step_id = $step->id;
                $client->save();
            }

            //Handle files
            foreach($request->files as $key => $file):
                $file_activity = Activity::find($key);
                switch($file_activity->actionable_type){
                    case 'App\ActionableDocument':
                        $afile = $request->file($key);
                        $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$afile->getClientOriginalExtension();
                        $stored = $afile->storeAs('documents', $name);

                        $document = new Document;
                        $document->name = $file_activity->name;
                        $document->file = $name;
                        $document->user_id = auth()->id();
                        $document->client_id = $client->id;
                        $document->save();

                        ActionableDocumentData::insert([
                            'actionable_document_id' => $file_activity->actionable_id,
                            'document_id' => $document->id,
                            'client_id' => $client->id,
                            'user_id' => auth()->id(),
                            'duration' => 120
                        ]);
                        break;
                    default:
                        //todo capture detaults
                        break;
                }

            endforeach;

            //$notification = $this->activityNotification($client, $log->id);

        }

        //Move process step to the next step if all activities completed

        $load_next_step = false;

        if($load_next_step == true) {
            return redirect()->route('clients.stepprogress', ['client' => $client, 'step' => $n_step]);
        }

        return redirect()->back()->with(['flash_success' => "Client details captured."]);
    }

    public function storeActions(Client $client, Request $request)
    {

        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;

                $step = Step::find($request->input('step_id'));
                $step->load(['activities.actionable.data' => function ($query) use ($id) {
                    $query->where('client_id', $id);
                }]);

                $all_activities_completed = false;
                foreach ($step->activities as $activity) {
                    if (is_null($request->input($activity->id))) {
                        if ($request->input('old_' . $activity->id) != $request->input($activity->id)) {

                            if (is_array($request->input($activity->id))) {

                                $old = explode(',', $request->input('old_' . $activity->id));
                                $diff = array_diff($old, $request->input($activity->id));
                                //dd($diff);

                                foreach ($request->input($activity->id) as $key => $value) {
                                    $activity_log = new ActivityLog;
                                    $activity_log->log_id = $log->id;
                                    $activity_log->activity_id = $activity->id;
                                    $activity_log->activity_name = $activity->name;
                                    $activity_log->old_value = $request->input('old_' . $activity->id);
                                    $activity_log->new_value = $value;
                                    $activity_log->save();
                                }
                            } else {
                                $old = $request->input('old_' . $activity->id);

                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $request->input($activity->id);
                                $activity_log->save();
                            }

                            switch ($activity->actionable_type) {
                                case 'App\ActionableBoolean':
                                    ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableDate':
                                    ActionableDateData::where('actionable_date_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableText':
                                    ActionableTextData::where('actionable_text_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableDropdown':
                                    ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }
                        }
                    }

                    if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                        //If value did not change, do not save it again or add it to log
                        if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                            continue;
                        }
                        if (is_array($request->input($activity->id))) {

                            $old = explode(',', $request->input('old_' . $activity->id));
                            $diff = array_diff($old, $request->input($activity->id));
                            //dd($diff);

                            foreach ($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_' . $activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        //activity type hook
                        //dd($request);
                        switch ($activity->actionable_type) {
                            case 'App\ActionableBoolean':
                                ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->where('data', $old)->delete();

                                ActionableBooleanData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_boolean_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableDate':
                                ActionableDateData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_date_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableText':

                                ActionableTextData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_text_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableDropdown':
                                foreach ($request->input($activity->id) as $key => $value) {
                                    if (in_array($value, $old, true)) {

                                    } else {
                                        ActionableDropdownData::insert([
                                            'actionable_dropdown_id' => $activity->actionable_id,
                                            'actionable_dropdown_item_id' => $value,
                                            'client_id' => $client->id,
                                            'user_id' => auth()->id(),
                                            'duration' => 120,
                                            'created_at' => now()
                                        ]);
                                    }

                                    if (!empty($diff)) {
                                        ActionableDropdownData::where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->whereIn('actionable_dropdown_item_id', $diff)->delete();
                                    }


                                }
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                    }
                }

                //Handle files
                foreach ($request->files as $key => $file):
                    $file_activity = Activity::find($key);
                    switch ($file_activity->actionable_type) {
                        case 'App\ActionableDocument':
                            $afile = $request->file($key);
                            $name = Carbon::now()->format('Y-m-d') . "-" . strtotime(Carbon::now()) . "." . $afile->getClientOriginalExtension();
                            $stored = $afile->storeAs('documents', $name);

                            $document = new Document;
                            $document->name = $file_activity->name;
                            $document->file = $name;
                            $document->user_id = auth()->id();
                            $document->client_id = $client->id;
                            $document->save();

                            ActionableDocumentData::insert([
                                'actionable_document_id' => $file_activity->actionable_id,
                                'document_id' => $document->id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120
                            ]);
                            break;
                        default:
                            //todo capture detaults
                            break;
                    }

                endforeach;

                //$notification = $this->activityNotification($client, $log->id);

        }

        return redirect()->back()->with(['flash_success' => "Client details captured."]);
    }

    public function autocompleteClientProcess(Request $request, Client $clientid, Process $processid,$newprocess)
    {
        //autocomplete all entries for current process

            $steps = Step::with('activities.actionable.data')->where('process_id',$processid->id)->get();
            /*return $step;
            exit;*/
            $activities_auto_completed = [];
            //return null;
            //todo just change switch logic to assign fake data for all activities
        foreach($steps as $step) {
            foreach ($step->activities as $activity) {

                //Check if activity is not already set/completed
                if (!$clientid->isActivitieCompleted($activity)) {
                    //if(!isset($activity->actionable['data'][0])){
                    $found = false;
                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->client_id == $clientid->id) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $activities_auto_completed[] = $activity->id;
                        $actionable_text_data = $activity->actionable_type . 'Data';
                        $actionable_data = new $actionable_text_data;
                        $actionable_data->client_id = $clientid->id;
                        $actionable_data->user_id = auth()->id();
                        $actionable_data->duration = 120;
                        switch ($activity->getTypeName()) {
                            case 'text':
                                $actionable_data->data = null;
                                $actionable_data->actionable_text_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'template_email':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_template_email_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'document_email':
                                //return 'document_email';
                                break;
                            case 'document':
                                $actionable_data->actionable_document_id = $activity->actionable_id;
                                $actionable_data->document_id = 1;
                                $actionable_data->save();
                                break;
                            case 'dropdown':
                                $item = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->take(1)->first();

                                $actionable_data->actionable_dropdown_id = $activity->actionable_id;
                                $actionable_data->actionable_dropdown_item_id = 0;
                                $actionable_data->save();
                                break;
                            case 'date':
                                $actionable_data->data = null;
                                $actionable_data->actionable_date_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'boolean':
                                $actionable_data->data = null;
                                $actionable_data->actionable_boolean_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'notification':
                                $notification = new Notification;
                                $notification->name = $clientid->company . ' has been updated: ' . $activity->name;
                                $notification->link = route('clients.progress', $clientid);
                                $notification->save();

                                $actionable_data->actionable_notification_id = $activity->actionable_id;
                                $actionable_data->notification_id = $notification->id;
                                $actionable_data->save();
                                break;
                            case 'multiple_attachment':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_ma_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            default:
                                return 'error';
                                break;
                        }
                    }

                }
            }
        }

        $new_process_id = $newprocess;

        //check if selected process has any activities for this client
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($clientid) {
            $query->where('client_id', $clientid->id);
        }])->where('process_id',$new_process_id)->get();

        $process_activities=array();
        if(count($highest_step_id) > 0) {

            foreach ($highest_step_id as $highest_step) {
                if($clientid->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                    foreach ($highest_step->process->activities as $activity){
                        //get the step ids of the process if the client was previously in the selected process
                        foreach ($activity->actionable['data'] as $data) {
                            //push the step id into the array
                            array_push($process_activities,["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                        }
                    }
                }
            }


            //sort the array in descending order
            usort($process_activities, function ($item1, $item2) {
                return $item2['order'] <=> $item1['order'];
            });

        }

        $process_first_step = Step::where('process_id',$new_process_id)->orderBy('order','asc')->first();

        $new_step_id = ( isset($process_activities[0]) ? $process_activities[0]["step_id"] : $process_first_step->id );

        //update client with new process id and step id
        //$client = Client::find($clientid);
        $clientid->process_id = $new_process_id;
        $clientid->step_id = $new_step_id;
        $clientid->save();

        return response()->json();
    }

    public function keepClientProcess(Request $request, Client $clientid, Process $processid,$newprocess){
        $new_process_id = $newprocess;

        //check if selected process has any activities for this client
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($clientid) {
            $query->where('client_id', $clientid->id);
        }])->where('process_id',$new_process_id)->get();

        $process_activities=array();
        if(count($highest_step_id) > 0) {

               foreach ($highest_step_id as $highest_step) {
                   if($clientid->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                       foreach ($highest_step->process->activities as $activity){
                           //get the step ids of the process if the client was previously in the selected process
                           foreach ($activity->actionable['data'] as $data) {
                               //push the step id into the array
                               array_push($process_activities,["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                           }
                       }
                   }
                }


            //sort the array in descending order
            usort($process_activities, function ($item1, $item2) {
                return $item2['order'] <=> $item1['order'];
            });

        }

        $process_first_step = Step::where('process_id',$new_process_id)->orderBy('order','asc')->first();

        $new_step_id = ( isset($process_activities[0]) ? $process_activities[0]["step_id"] : $process_first_step->id );

        //update client with new process id and step id
        //$client = Client::find($clientid);
        $clientid->process_id = $new_process_id;
        $clientid->step_id = $new_step_id;
        $clientid->save();

        return response()->json();
    }

    public function follow(Client $client, StoreFollowRequest $request)
    {
        if ($request->input('follow')) {
            ClientUser::insert([
                'client_id' => $client->id,
                'user_id' => auth()->id()
            ]);
        } else {
            ClientUser::where('client_id', $client->id)->where('user_id', auth()->id())->delete();
        }

        return redirect()->back()->with('flash_success', 'Follow status updated successfully.');
    }

    public function complete($client_id,$step_id,$newdate)
    {
        $client = Client::find($client_id);
        $client->completed_at = $newdate;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function uncomplete($client_id,$step_id)
    {
        $client = Client::find($client_id);
        $client->completed_at = null;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function changecomplete($client_id,$step_id,$newdate)
    {
        $client = Client::find($client_id);
        $client->completed_at = $newdate;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function viewTemplate(Client $client, Template $template)
    {

        $processed_template = $this->processTemplate($client, $template->file);

        return response()->download(storage_path('app/templates/' . $processed_template));

    }

    public function viewDocument(Client $client, Document $document)
    {

        $processed_document = $this->processTemplate($client, $document->file);

        return response()->download(storage_path('app/documents/' . $processed_document));

    }

    public function sendTemplate(Client $client, Activity $activity, Request $request)
    {
        $template = Template::find($request->input('template_file'));

        $processed_templates = array();
        $processed_templates[0]['file'] = $this->processTemplate($client, $template->file);
        $processed_templates[0]['type'] = 'template';

        $actionable_template_email = $activity->actionable;

        ActionableTemplateEmailData::insert([
            'template_id' => $template->id,
            'email' => $client->email,
            'actionable_template_email_id' => $actionable_template_email->id,
            'client_id' => $client->id,
            'user_id' => auth()->id(),
            'duration' => 120,
            //'file' => $processed_template, To Do Add file name
        ]);

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');
            $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
        }

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_subject, $email_content));
        endforeach;

        return response()->json(['success' => 'Template sent successfully.']);

    }

    public function sendDocument(Client $client, Activity $activity, Request $request)
    {
        $documents= Document::find($request->input('document_file'));

        $processed_documents= array();
        $processed_documents[0]['file'] = $this->processTemplate($client, $documents->file);
        $processed_documents[0]['type'] = 'document';

        $actionable_documents_email = $activity->actionable;

        ActionableDocumentEmailData::insert([
            'document_id' => $documents->id,
            'email' => $client->email,
            'actionable_document_email_id' => $actionable_documents_email->id,
            'client_id' => $client->id,
            'user_id' => auth()->id(),
            'duration' => 120,
            //'file' => $processed_template, To Do Add file name
        ]);

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');
            $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
        }

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            Mail::to(trim($email))->send(new TemplateMail($client, $processed_documents, $email_subject, $email_content));
        endforeach;

        return response()->json(['success' => 'Template sent successfully.']);
    }

    public function sendDocuments(Client $client, Activity $activity, Request $request, Step $step)
    {
        //Todo
        //Send to all templates
        $processed_templates = Array();
        $counter = 0;
        if($request->input('templates')) {
            foreach ($request->input('templates') as $template_id):
                $template = Template::find($template_id);

                $processed_templates[$counter]['file'] = $this->processTemplate($client, $template->file);
                $processed_templates[$counter]['type'] = 'template';

                $actionable_template_email = $activity->actionable;

                ActionableMultipleAttachmentData::insert([
                    'template_id' => $template->id,
                    'email' => $client->email,
                    'actionable_ma_id' => $actionable_template_email->id,
                    'client_id' => $client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120,
                    //'file' => $processed_template, To Do Add file name
                ]);
                $counter++;
            endforeach;
        }

        if($request->input('documents')) {
            foreach ($request->input('documents') as $document_id):
                $document = Document::find($document_id);
                $processed_templates[$counter]['file'] = $document->file;
                $processed_templates[$counter]['type'] = 'document';
                $counter++;
            endforeach;
        }

        //$email_signature = EmailSignature::where('user_id','=',auth()->id())->get();

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');
            $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
        }


            $a = new ActionableMultipleAttachmentData();
            $a->template_id = $request->input('template_email');
            $a->email = $request->input('email');
            $a->actionable_ma_id = $activity->actionable_id;
            $a->client_id = $client->id;
            $a->user_id = auth()->id();
            $a->duration = '120';
            $a->save();
            //'file' => $processed_template, To Do Add file name

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            //Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_content,$email_signature));
            //Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_subject, $email_content));
        endforeach;

        $request->session()->forget('email_template');

        return response()->json(['success' => 'Documents sent successfully.', 'docs' => $processed_templates]);
    }

    /*
    * @param Client
    * @param template file
    * @return the new generated processed template
    * Use to process a docx document, if document type is not .docx, return as it is
    */
    public function processTemplate(Client $client, $template_file)
    {

        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext != "docx") {
            return $template_file;
        }

        $client->load('referrer', 'introducer');

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $templateProcessor->setValue(
            ['client.first_name', 'client.last_name', 'client.email', 'client.contact'],
            [$client->first_name, $client->last_name, $client->email, $client->contact]
        );

        $templateProcessor->setValue(
            ['referrer.first_name', 'referrer.last_name', 'referrer.email', 'referrer.avatar'],
            isset($client->referrer) ?
                [
                    $client->referrer->first_name,
                    $client->referrer->last_name,
                    $client->referrer->email,
                    $client->referrer->contact
                ] :
                ['', '', '', '']
        );

        $templateProcessor->setValue(
            ['introducer.first_name', 'introducer.last_name', 'introducer.email', 'introducer.contact'],
            isset($client->introducer) ?
                [
                    $client->introducer->first_name,
                    $client->introducer->last_name,
                    $client->introducer->email,
                    $client->introducer->contact
                ] :
                ['', '', '', '']
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedtemplates/' . date("Y") . DIRECTORY_SEPARATOR . date("m");
        if (!File::exists(storage_path('app/templates/' . $processed_template_path))) {
            Storage::makeDirectory('templates/' . $processed_template_path);
        }

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . "letter_" . $client->id . "_" . date("Y_m_d_H_i_s") . "_" . uniqid() . ".docx";

        $templateProcessor->saveAs(storage_path('app/templates/' . $processed_template));

        return $processed_template;

    }

    public function sendNotification(Client $client, Activity $activity, Request $request)
    {
        //Notification update
        $log = new Log;
        $log->client_id = $client->id;
        $log->user_id = auth()->id();
        $log->save();

        $client->load('users');

        $activity_log = new ActivityLog;
        $activity_log->log_id = $log->id;
        $activity_log->activity_id = $activity->id;
        $activity_log->activity_name = $activity->name;
        $activity_log->old_value = $request->input('old_'.$activity->id);
        $activity_log->new_value = implode(',',$request->input('notification_user'));
        $activity_log->save();


        $notification = new Notification;
        $notification->name = $client->company . ' has been updated: ' . $activity->name;
        $notification->link = route('activitieslog', $log->id);
        //$notification->link = route('clients.progress', $client).'/1';
        $notification->save();

        $actionable_notification_data = new ActionableNotificationData;
        $actionable_notification_data->actionable_notification_id = $activity->actionable_id;
        $actionable_notification_data->notification_id = $notification->id;
        $actionable_notification_data->client_id = $client->id;
        $actionable_notification_data->user_id = auth()->id();
        $actionable_notification_data->duration = 120;
        $actionable_notification_data->save();

        $user_notifications = [];

        if (!is_null($client->introducer_id)) {
            array_push($user_notifications, [
                'user_id' => $client->introducer_id,
                'notification_id' => $notification->id
            ]);
        }

        if (!is_null($client->user_id)) {
            array_push($user_notifications, [
                'user_id' => $activity->user_id,
                'notification_id' => $notification->id
            ]);
        }

        if ( $request->has('notification_user') && (!empty($request->input('notification_user')))){
            $notification_users = $request->input('notification_user');
            foreach($notification_users as $notification_user):
                array_push($user_notifications, [
                    'user_id' => (int)$notification_user,
                    'notification_id' => $notification->id
                ]);
            endforeach;
        }

        NotificationEvent::dispatch($client->introducer_id, $notification);

        foreach ($client->users as $user) {
            array_push($user_notifications, [
                'user_id' => $user->id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($user->id, $notification);
        }

        UserNotification::insert($user_notifications);

        return response()->json(['success' => 'Template sent successfully.']);
    }

    public function activityNotification(Client $client, $log_id)
    {
        $client->load('users');

        $notification = new Notification;
        $notification->name = $client->company . ' Notification for : ' . $client->company;
        $notification->link = route('activitieslog', $log_id);
        $notification->save();

        $user_notifications = [];
        array_push($user_notifications, [
            'user_id' => $client->introducer_id,
            'notification_id' => $notification->id
        ]);

        NotificationEvent::dispatch($client->introducer_id, $notification);

        foreach ($client->users as $user) {
            array_push($user_notifications, [
                'user_id' => $user->id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($user->id, $notification);
        }

        UserNotification::insert($user_notifications);

        return true;
    }

    public function storeComment(Client $client, Request $request)
    {
        $comment = new ClientComment;
        $comment->client_id = $client->id;
        $comment->user_id = auth()->id();
        $comment->comment = $request->input('comment');
        $comment->save();

        return redirect()->back()->with('flash_success', 'Comment added successfully');
    }

    public function createClientActivity($token, Request $request)
    {
        $client_activity = ClientActivity::where('token', $token)->firstOrFail();

        $client_activity->load('activity.actionable.data', 'client');

        $activity = [
            'name' => $client_activity->activity->name,
            'type' => $client_activity->activity->getTypeName(),
            'id' => $client_activity->activity_id,
            'value' => (isset($client_activity->activity->actionable['data'][0])) ? $client_activity->activity->actionable['data'][0] : ''
        ];

        $parameters = [
            'client' => $client_activity->client->load('introducer'),
            'activity' => $activity,
            'client_activity' => $client_activity
        ];

        return view('clients.clientactivity')->with($parameters);
    }

    public function storeClientActivity($token, Request $request)
    {
        $client_activity = ClientActivity::where('token', $token)->firstOrFail();

        $client_activity->load('client','activity');

        //activity type hook
        switch ($client_activity->activity->actionable_type) {
            case 'App\ActionableBoolean':
                ActionableBooleanData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_boolean_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableDate':
                ActionableDateData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_date_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableText':
                ActionableTextData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_text_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableDocument':
                ActionableDocumentData::insert([
                    'actionable_document_id' => $client_activity->activity->actionable_id,
                    'document_id' => $request->input($client_activity->activity->id),
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id()
                ]);
                break;
            case 'App\ActionableDropdown':
                ActionableDropdownData::insert([
                    'actionable_dropdown_id' => $client_activity->activity->actionable_id,
                    'actionable_dropdown_item_id' => $request->input($client_activity->activity->id),
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            default:
                //todo capture defaults
                break;
        }

        return redirect()->back()->with(['flash_success' => "Information updated successfully."]);
    }

    public function storeProgressing(Client $client){
        $client->is_progressing = !$client->is_progressing;
        if($client->is_progressing == 0) {
            $client->not_progressing_date = now();
        } else {
            $client->not_progressing_date = null;
        }

        $client->save();

        return redirect()->back()->with(['flash_info'=>'Client status updated successfully.']);
    }

    public function approval(Client $client, Request $request){
        $client->needs_approval = false;
        $client->save();

        if(!$request->input('status')){
            $client->delete();

            return redirect(route('clients.index'))->with(['flash_info'=>'Client declined successfully.']);
        }

        return redirect()->back()->with(['flash_info'=>'Client approved successfully.']);
    }

    public function completeStep(Client $client, Process $process, Step $step){
        $step->load('activities.actionable.data');
        //dd($step);
        $activities_auto_completed = [];
        //return null;
        //todo just change switch logic to assign fake data for all activities
        foreach($step->activities as $activity){

            //Check if activity is not already set/completed
            if(!$client->isActivitieCompleted($activity)){
                //if(!isset($activity->actionable['data'][0])){
                $found  = false;
                foreach ($activity->actionable['data'] as $datum){
                    if($datum->client_id == $client->id){
                        $found = true;
                        break;
                    }
                }

                if(!$found){
                    $activities_auto_completed[] = $activity->id;
                    $actionable_text_data = $activity->actionable_type.'Data';
                    $actionable_data = new $actionable_text_data;
                    $actionable_data->client_id = $client->id;
                    $actionable_data->user_id = auth()->id();
                    $actionable_data->duration = 120;
                    switch($activity->getTypeName()){
                        case 'text':
                            $actionable_data->data = null;
                            $actionable_data->actionable_text_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'template_email':
                            $actionable_data->template_id = 1;
                            $actionable_data->email = null;
                            $actionable_data->actionable_template_email_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'document_email':
                            //return 'document_email';
                            break;
                        case 'document':
                            $actionable_data->actionable_document_id = $activity->actionable_id;
                            $actionable_data->document_id = 1;
                            $actionable_data->save();
                            break;
                        case 'dropdown':
                            $item = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->take(1)->first();

                            $actionable_data->actionable_dropdown_id = $activity->actionable_id;
                            $actionable_data->actionable_dropdown_item_id = 0;
                            $actionable_data->save();
                            break;
                        case 'date':
                            $actionable_data->data = null;
                            $actionable_data->actionable_date_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'boolean':
                            $actionable_data->data = null;
                            $actionable_data->actionable_boolean_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'notification':
                            $notification = new Notification;
                            $notification->name = $client->company . ' has been updated: ' . $activity->name;
                            $notification->link = route('clients.progress', $client);
                            $notification->save();

                            $actionable_data->actionable_notification_id = $activity->actionable_id;
                            $actionable_data->notification_id = $notification->id;
                            $actionable_data->save();
                            break;
                        case 'multiple_attachment':
                            $actionable_data->template_id = 1;
                            $actionable_data->email = null;
                            $actionable_data->actionable_ma_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        default:
                            //return 'error';
                            break;
                    }
                }

            }
        }

        //Move process step to the next step if all activities completed
        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

        if($client->isStepActivitiesCompleted($step) && $step->order < $max_step->order){
            $client = Client::find($client->id);
            //$client->step_id = $client->step_id + 1;
            $client->step_id = $n_step->id;
            $client->save();
        }

        if(!$n_step) {
            if ($client->isStepActivitiesCompleted($step)) {
                $client = Client::find($client->id);
                //$client->step_id = $client->step_id + 1;
                $client->process_id = $process->id;
                $client->step_id = $max_step->id;
                $client->save();
            }
        }

        return response()->json(['success' => 'Template sent successfully.', 'activities_auto_completed' => $activities_auto_completed]);
    }

    public function completeStep2(Client $client, Process $process, Step $step){
        $prev_steps = Step::where('process_id',$step->process_id)->where('order','<',$step->order)->orderBy('order','desc')->get();

        foreach ($prev_steps as $prev_step) {
            $pstep = Step::find($prev_step->id);
            $pstep->load('activities.actionable.data');
            //dd($step);
            $activities_auto_completed = [];
            //return null;
            //todo just change switch logic to assign fake data for all activities
            foreach ($pstep->activities as $activity) {

                //Check if activity is not already set/completed
                if (!$client->isActivitieCompleted($activity)) {
                    //if(!isset($activity->actionable['data'][0])){
                    $found = false;
                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->client_id == $client->id) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $activities_auto_completed[] = $activity->id;
                        $actionable_text_data = $activity->actionable_type . 'Data';
                        $actionable_data = new $actionable_text_data;
                        $actionable_data->client_id = $client->id;
                        $actionable_data->user_id = auth()->id();
                        $actionable_data->duration = 120;
                        switch ($activity->getTypeName()) {
                            case 'text':
                                $actionable_data->data = null;
                                $actionable_data->actionable_text_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'template_email':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_template_email_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'document_email':
                                //return 'document_email';
                                break;
                            case 'document':
                                $actionable_data->actionable_document_id = $activity->actionable_id;
                                $actionable_data->document_id = 1;
                                $actionable_data->save();
                                break;
                            case 'dropdown':
                                $item = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->take(1)->first();

                                $actionable_data->actionable_dropdown_id = $activity->actionable_id;
                                $actionable_data->actionable_dropdown_item_id = 0;
                                $actionable_data->save();
                                break;
                            case 'date':
                                $actionable_data->data = null;
                                $actionable_data->actionable_date_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'boolean':
                                $actionable_data->data = null;
                                $actionable_data->actionable_boolean_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'notification':
                                $notification = new Notification;
                                $notification->name = $client->company . ' has been updated: ' . $activity->name;
                                $notification->link = route('clients.progress', $client);
                                $notification->save();

                                $actionable_data->actionable_notification_id = $activity->actionable_id;
                                $actionable_data->notification_id = $notification->id;
                                $actionable_data->save();
                                break;
                            case 'multiple_attachment':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_ma_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            default:
                                //return 'error';
                                break;
                        }
                    }

                }
            }
        }

        //Move process step to the next step if all activities completed
        $max_step = Step::where('process_id', $client->process_id)->max('id');

        $client = Client::find($client->id);
        //$client->step_id = $client->step_id + 1;
        $client->process_id = $process->id;
        $client->step_id = $step->id;
        $client->save();


        return response()->json(['success' => 'Client moved successfully.']);
    }

    public function forms(Request $request,Client $client)
    {
        if((strpos($request->headers->get('referer'),'reports') !== false) || (strpos($request->headers->get('referer'),'custom_report') !== false)) {
            $request->session()->put('path_route',$request->headers->get('referer'));
            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order')->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::orderBy('id','desc')->get();
        $clientforms2 = ClientForm::orderBy('id','desc')->where('client_id',$client->id)->get();

        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();

        $r = $client->load('forms.user',"crfforms");
        //dd($clientforms2);
        return view('clients.forms.index')->with([
            'client' => $client->load('forms.user',"crfforms"),
            'process_progress' => $process_progress,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray()),
            'list' => $clientforms2,
            'path' => $path,
            'path_route' => $path_route
        ]);
    }

    public function createCrfForm(Client $client)
    {
        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientCRFForm::where('client_id',$client->id)->get();

        return view('clients.forms.create')->with([
            'client' => $client->load('forms.user'),
            'process_progress' => $process_progress,
            'view_process_dropdown' => Process::orderBy('name','asc')->get(),
            'steps' => $step_data,

        ]);

    }

    public function saveCrfForm($clientid, Request $request){
        $form = new ClientCRFForm();
        $form->client_id = $clientid;
        $form->form_date = $request->input('sec1_date');$form->client_code = $request->input('client_code');
        $form->client_name = $request->input('client_name');
        $form->director = $request->input('director');
        $form->manager = $request->input('manager');
        $form->form_completed_by = $request->input('form_completed_by');
        $form->office = $request->input('office');
        $form->contact_type = $request->input('contact_type');
        $form->business_type =  $request->input('business_type');
        $form->client_type = $request->input('client_type');
        if($request->input('sec3_country') == 'Other'){
            $form->country = $request->input('sec3_country2');
        } else {
            $form->country = $request->input('sec3_country');
        }
        if($request->input('sec3_industry') == "Other") {
            $form->industry = $request->input('sec3_industry2');
        } else {
            $form->industry = $request->input('sec3_industry');
        }
        $form->year_end = $request->input('year_end');


        $form->services = '';
        if($request->has('provided_services')) {
            foreach ($request->input('provided_services') as $services):
                if ($form->services == '')
                    $form->services = $services;
                else
                    $form->services .= "," . $services;
            endforeach;
        }
        $form->company_secretarial_department = $request->input('company_secretarial_department');
        $form->company_registration_number = $request->input('company_reg_no');
        $form->audit_status = $request->input('audit_status');
        $form->cosec_filing = $request->input('cosec_filing');
        $form->ct_number = $request->input('ct_number');
        $form->vat_number = $request->input('vat_number');
        $form->tax_filing = $request->input('corp_tax_filing');
        $form->ct_filing_date = $request->input('ct_filing_date');
        $form->billing_address1 = $request->input('billing_address1');
        $form->billing_address2 = $request->input('billing_address2');
        $form->billing_address3 = $request->input('billing_address3');
        $form->billing_town = $request->input('billing_town');
        $form->billing_county = $request->input('billing_county');
        $form->billing_pcode = $request->input('billing_pcode');
        $form->billing_country = $request->input('billing_country');
        $form->business_address1 = $request->input('business_address1');
        $form->business_address2 = $request->input('business_address2');
        $form->business_address3 = $request->input('business_address3');
        $form->business_town = $request->input('business_town');
        $form->business_county = $request->input('business_county');
        $form->business_pcode = $request->input('business_pcode');
        $form->business_country = $request->input('business_country');
        $form->registered_address1 = $request->input('registered_address1');
        $form->registered_address2 = $request->input('registered_address2');
        $form->registered_address3 = $request->input('registered_address3');
        $form->registered_town = $request->input('registered_town');
        $form->registered_county = $request->input('registered_county');
        $form->registered_pcode = $request->input('registered_pcode');
        $form->registered_country = $request->input('registered_country');
        if($request->has('company_telephone_numbers_na')){
            $form->company_tel_number1 = 'N/A';
            $form->company_tel_number2 = 'N/A';
        } else {
            $form->company_tel_number1 = $request->input('company_telephone_numbers');
            $form->company_tel_number2 = $request->input('company_telephone_numbers2');
        }
        if($request->has('company_fax_numbers_na')){
            $form->company_fax_number = 'N/A';
        } else {
            $form->company_fax_number = $request->input('company_fax_numbers');
        }
        if($request->has('company_email_address_na')){
            $form->company_email_address = 'N/A';
        } else {
            $form->company_email_address = $request->input('company_email_address');
        }
        if($request->has('company_website_na')){
            $form->company_website = 'N/A';
        } else {
            $form->company_website = $request->input('company_website');
        }
        $form->i1_name = $request->input('i1_name');
        $form->i1_position = $request->input('i1_position');
        if($request->has('i1_office_number_na')){
            $form->i1_office_number = 'N/A';
        } else {
            $form->i1_office_number = $request->input('i1_office_number');
        }
        if($request->has('i1_home_number_na')){
            $form->i1_home_number = 'N/A';
        } else {
            $form->i1_home_number = $request->input('i1_home_number');
        }
        if($request->has('i1_mobile_number_na')){
            $form->i1_mobile_number = 'N/A';
        } else {
            $form->i1_mobile_number = $request->input('i1_mobile_number');
        }
        if($request->has('i1_email_address_na')){
            $form->i1_email_address = 'N/A';
        } else {
            $form->i1_email_address = $request->input('i1_email_address');
        }
        $form->i1_address1 = $request->input('i1_address1');
        $form->i1_address2 = $request->input('i1_address2');
        $form->i1_address3 = $request->input('i1_address3');
        $form->i1_town = $request->input('i1_town');
        $form->i1_county = $request->input('i1_county');
        $form->i1_pcode = $request->input('i1_pcode');
        $form->i1_country = $request->input('i1_country');
        if($request->has('i2_na')){
            $form->i2_na = '1';
            $form->i2_name = '';
            $form->i2_position = '';
            $form->i2_office_number = '';
            $form->i2_home_number = '';
            $form->i2_mobile_number = '';
            $form->i2_email_address = '';
            $form->i2_address1 = '';
            $form->i2_address2 = '';
            $form->i2_address3 = '';
            $form->i2_town = '';
            $form->i2_county = '';
            $form->i2_pcode = '';
            $form->i2_country = '';
        } else {
            $form->i2_na = '0';
            $form->i2_name = $request->input('i2_name');
            $form->i2_position = $request->input('i2_position');
            if($request->input('i2_office_number_na')){
                $form->i2_office_number = 'N/A';
            } else {
                $form->i2_office_number = $request->input('i2_office_number');
            }
            if($request->input('i2_home_number_na')){
                $form->i2_home_number = 'N/A';
            } else {
                $form->i2_home_number = $request->input('i2_home_number');
            }
            if($request->input('i2_mobile_number_na')){
                $form->i2_mobile_number = 'N/A';
            } else {
                $form->i2_mobile_number = $request->input('i2_mobile_number');
            }
            if($request->input('i2_email_address_na')){
                $form->i2_email_address = 'N/A';
            } else {
                $form->i2_email_address = $request->input('i2_email_address');
            }
            $form->i2_address1 = $request->input('i2_address1');
            $form->i2_address2 = $request->input('i2_address2');
            $form->i2_address3 = $request->input('i2_address3');
            $form->i2_town = $request->input('i2_town');
            $form->i2_county = $request->input('i2_county');
            $form->i2_pcode = $request->input('i2_pcode');
            $form->i2_country = $request->input('i2_country');
        }
        if($request->has('st_uk_tax_number_na')){
            $form->st_uk_tax_number = 'N/A';
        } else {
            $form->st_uk_tax_number = $request->input('st_uk_tax_number');
        }
        if($request->has('st_ni_number_na')){
            $form->st_ni_number = 'N/A';
        } else {
            $form->st_ni_number = $request->input('st_ni_number');
        }
        if($request->has('st_vat_number_na')){
            $form->st_vat_number = 'N/A';
        } else {
            $form->st_vat_number = $request->input('st_vat_number');
        }
        $form->st_personal_tax = $request->input('st_personal_tax');
        $form->st_personal_address1 = $request->input('st_personal_address1');
        $form->st_personal_address2 = $request->input('st_personal_address2');
        $form->st_personal_address3 = $request->input('st_personal_address3');
        $form->st_personal_town = $request->input('st_personal_town');
        $form->st_personal_county = $request->input('st_personal_county');
        $form->st_personal_pcode = $request->input('st_personal_pcode');
        $form->st_personal_country = $request->input('st_personal_country');
        $form->st_billing_address1 = $request->input('st_billing_address1');
        $form->st_billing_address2 = $request->input('st_billing_address2');
        $form->st_billing_address3 = $request->input('st_billing_address3');
        $form->st_billing_town = $request->input('st_billing_town');
        $form->st_billing_county = $request->input('st_billing_county');
        $form->st_billing_pcode = $request->input('st_billing_pcode');
        $form->st_billing_country = $request->input('st_billing_country');
        $form->st_business_address1 = $request->input('st_business_address1');
        $form->st_business_address2 = $request->input('st_business_address2');
        $form->st_business_address3 = $request->input('st_business_address3');
        $form->st_business_town = $request->input('st_business_town');
        $form->st_business_county = $request->input('st_business_county');
        $form->st_business_pcode = $request->input('st_business_pcode');
        $form->st_business_country = $request->input('st_business_country');
        $form->st_contact_name = $request->input('st_contact_name');
        if($request->has('st_office_number_na')){
            $form->st_office_number = 'N/A';
        } else {
            $form->st_office_number = $request->input('st_office_number');
        }
        if($request->has('st_home_number_na')){
            $form->st_home_number = 'N/A';
        } else {
            $form->st_home_number = $request->input('st_home_number');
        }
        if($request->has('st_mobile_number_na')){
            $form->st_mobile_number = 'N/A';
        } else {
            $form->st_mobile_number = $request->input('st_mobile_number');
        }
        if($request->has('st_fax_number_na')){
            $form->st_fax_number = 'N/A';
        } else {
            $form->st_fax_number = $request->input('st_fax_number');
        }
        if($request->has('st_email_address1_na')){
            $form->st_email_address1 = 'N/A';
        } else {
            $form->st_email_address1 = $request->input('st_email_address1');
        }
        if($request->has('st_email_address2_na')){
            $form->st_email_address2 = 'N/A';
        } else {
            $form->st_email_address2 = $request->input('st_email_address2');
        }
        if($request->has('st_website_na')){
            $form->st_website = 'N/A';
        } else {
            $form->st_website = $request->input('st_website');
        }
        if($request->has('p_partnership_ref_no_na')){
            $form->p_partnership_ref_no = 'N/A';
        } else {
            $form->p_partnership_ref_no = $request->input('p_partnership_ref_no');
        }
        if($request->has('p_uk_tax_number_na')){
            $form->p_uk_tax_number = 'N/A';
        } else {
            $form->p_uk_tax_number = $request->input('p_uk_tax_number');
        }
        if($request->has('p_ni_number_na')){
            $form->p_ni_number = 'N/A';
        } else {
            $form->p_ni_number = $request->input('p_ni_number');
        }
        if($request->has('p_vat_number_na')){
            $form->p_vat_number = 'N/A';
        } else {
            $form->p_vat_number = $request->input('p_vat_number');
        }
        $form->p_personal_tax = $request->input('p_personal_tax');
        $form->p_billing_address1 = $request->input('p_billing_address1');
        $form->p_billing_address2 = $request->input('p_billing_address2');
        $form->p_billing_address3 = $request->input('p_billing_address3');
        $form->p_billing_town = $request->input('p_billing_town');
        $form->p_billing_county = $request->input('p_billing_county');
        $form->p_billing_pcode = $request->input('p_billing_pcode');
        $form->p_billing_country = $request->input('p_billing_country');
        $form->p_business_address1 = $request->input('p_business_address1');
        $form->p_business_address2 = $request->input('p_business_address2');
        $form->p_business_address3 = $request->input('p_business_address3');
        $form->p_business_town = $request->input('p_business_town');
        $form->p_business_county = $request->input('p_business_county');
        $form->p_business_pcode = $request->input('p_business_pcode');
        $form->p_business_country = $request->input('p_business_country');
        if($request->has('p_office_number_na')){
            $form->p_office_number = 'N/A';
        } else {
            $form->p_office_number = $request->input('p_office_number');
        }
        if($request->has('p_fax_number_na')){
            $form->p_fax_number = 'N/A';
        } else {
            $form->p_fax_number = $request->input('p_fax_number');
        }
        if($request->has('p_email_address1_na')){
            $form->p_email_address1 = 'N/A';
        } else {
            $form->p_email_address1 = $request->input('p_email_address1');
        }
        if($request->has('p_email_address2_na')){
            $form->p_email_address2 = 'N/A';
        } else {
            $form->p_email_address2 = $request->input('p_email_address2');
        }
        if($request->has('p_website_na')){
            $form->p_website = 'N/A';
        } else {
            $form->p_website = $request->input('p_website');
        }
        $form->m1_address1 = $request->input('m1_address1');
        $form->m1_address2 = $request->input('m1_address2');
        $form->m1_address3 = $request->input('m1_address3');
        $form->m1_town = $request->input('m1_town');
        $form->m1_county = $request->input('m1_county');
        $form->m1_pcode = $request->input('m1_pcode');
        $form->m1_country = $request->input('m1_country');
        $form->m1_contact_name = $request->input('m1_contact_name');
        if($request->has('m1_home_number_na')){
            $form->m1_home_number = 'N/A';
        } else {
            $form->m1_home_number = $request->input('m1_home_number');
        }
        if($request->has('m1_mobile_number_na')){
            $form->m1_mobile_number = 'N/A';
        } else {
            $form->m1_mobile_number = $request->input('m1_mobile_number');
        }
        if($request->has('m1_email_address1_na')){
            $form->m1_email_address1 = 'N/A';
        } else {
            $form->m1_email_address1 = $request->input('m1_email_address1');
        }
        if($request->has('m1_email_address2_na')){
            $form->m1_email_address2 = 'N/A';
        } else {
            $form->m1_email_address2 = $request->input('m1_email_address2');
        }
        if($request->has('m2_na')) {
            $form->m2_address1 = '';
            $form->m2_address2 = '';
            $form->m2_address3 = '';
            $form->m2_town = '';
            $form->m2_county = '';
            $form->m2_pcode = '';
            $form->m2_country = '';
            $form->m2_contact_name = '';
            $form->m2_home_number = '';
            $form->m2_mobile_number = '';
            $form->m2_email_address1 = '';
            $form->m2_email_address2 = '';
            $form->m2_email_address2 = '';
        } else {
            $form->m2_address1 = $request->input('m2_address1');
            $form->m2_address2 = $request->input('m2_address2');
            $form->m2_address3 = $request->input('m2_address3');
            $form->m2_town = $request->input('m2_town');
            $form->m2_county = $request->input('m2_county');
            $form->m2_pcode = $request->input('m2_pcode');
            $form->m2_country = $request->input('m2_country');
            $form->m2_contact_name = $request->input('m2_contact_name');
            if ($request->input('m2_home_number_na')) {
                $form->m2_home_number = 'N/A';
            } else {
                $form->m2_home_number = $request->input('m2_home_number');
            }
            if ($request->input('m2_mobile_number_na')) {
                $form->m2_mobile_number = 'N/A';
            } else {
                $form->m2_mobile_number = $request->input('m2_mobile_number');
            }
            if ($request->input('m2_email_address1_na')) {
                $form->m2_email_address1 = 'N/A';
            } else {
                $form->m2_email_address1 = $request->input('m2_email_address1');
            }
            if ($request->input('m2_email_address2_na')) {
                $form->m2_email_address2 = 'N/A';
            } else {
                $form->m2_email_address2 = $request->input('m2_email_address2');
            }
        }
        if ($request->has('pen_ref_no_na')) {
            $form->pen_ref_no = 'N/A';
        } else {
            $form->pen_ref_no = $request->input('pen_ref_no');
        }
        if ($request->has('pen_tax_no')) {
            $form->pen_tax_no = 'N/A';
        } else {
            $form->pen_tax_no = $request->input('pen_tax_no');
        }
        $form->pen_tax_filing = $request->input('pen_tax_filing');
        $form->pen_tax_filing_date = $request->input('pen_tax_filing_date');
        $form->pen_billing_address1 = $request->input('pen_billing_address1');
        $form->pen_billing_address2 = $request->input('pen_billing_address2');
        $form->pen_billing_address3 = $request->input('pen_billing_address3');
        $form->pen_billing_town = $request->input('pen_billing_town');
        $form->pen_billing_county = $request->input('pen_billing_county');
        $form->pen_billing_pcode = $request->input('pen_billing_pcode');
        $form->pen_billing_country = $request->input('pen_billing_country');
        $form->pen_billing_contact_name = $request->input('pen_billing_contact_name');
        $form->pen_billing_position = $request->input('pen_billing_position');
        if ($request->has('pen_billing_office_number_na')) {
            $form->pen_billing_office_number = 'N/A';
        } else {
            $form->pen_billing_office_number = $request->input('pen_billing_office_number');
        }
        if ($request->has('pen_billing_home_number_na')) {
            $form->pen_billing_home_number = 'N/A';
        } else {
            $form->pen_billing_home_number = $request->input('pen_billing_home_number');
        }
        if ($request->has('pen_billing_mobile_number_na')) {
            $form->pen_billing_mobile_number = 'N/A';
        } else {
            $form->pen_billing_mobile_number = $request->input('pen_billing_mobile_number');
        }
        if ($request->has('pen_billing_fax_number_na')) {
            $form->pen_billing_fax_number = 'N/A';
        } else {
            $form->pen_billing_fax_number = $request->input('pen_billing_fax_number');
        }
        if ($request->has('pen_billing_email_address_na')) {
            $form->pen_billing_email_address = 'N/A';
        } else {
            $form->pen_billing_email_address = $request->input('pen_billing_email_address');
        }
        $form->pen_res_address1 = $request->input('pen_res_address1');
        $form->pen_res_address2 = $request->input('pen_res_address2');
        $form->pen_res_address3 = $request->input('pen_res_address3');
        $form->pen_res_town = $request->input('pen_res_town');
        $form->pen_res_county = $request->input('pen_res_county');
        $form->pen_res_pcode = $request->input('pen_res_pcode');
        $form->pen_res_country = $request->input('pen_res_country');
        $form->liq_company_registration_number = $request->input('liq_company_registration_number');
        $form->liq_tax_ref = $request->input('liq_tax_ref');
        $form->liq_d1_name = $request->input('liq_d1_name');
        $form->liq_d1_position = $request->input('liq_d1_position');
        if($request->has('liq_d1_office_number_na')){
            $form->liq_d1_office_number = 'N/A';
        } else {
            $form->liq_d1_office_number = $request->input('liq_d1_office_number');
        }
        if($request->has('liq_d1_home_number_na')){
            $form->liq_d1_home_number = 'N/A';
        } else {
            $form->liq_d1_home_number = $request->input('liq_d1_home_number');
        }
        if($request->has('liq_d1_mobile_number_na')){
            $form->liq_d1_mobile_number = 'N/A';
        } else {
            $form->liq_d1_mobile_number = $request->input('liq_d1_mobile_number');
        }
        if($request->has('liq_d1_email_address_na')){
            $form->liq_d1_email_address = 'N/A';
        } else {
            $form->liq_d1_email_address = $request->input('liq_d1_email_address');
        }
        $form->liq_d1_address1 = $request->input('liq_d1_address1');
        $form->liq_d1_address2 = $request->input('liq_d1_address2');
        $form->liq_d1_address3 = $request->input('liq_d1_address3');
        $form->liq_d1_town = $request->input('liq_d1_town');
        $form->liq_d1_county = $request->input('liq_d1_county');
        $form->liq_d1_pcode = $request->input('liq_d1_pcode');
        $form->liq_d1_country = $request->input('liq_d1_country');

        $form->liq_d2_name = $request->input('liq_d2_name');
        $form->liq_d2_position = $request->input('liq_d2_position');
        if($request->has('liq_d2_office_number_na')){
            $form->liq_d2_office_number = 'N/A';
        } else {
            $form->liq_d2_office_number = $request->input('liq_d2_office_number');
        }
        if($request->has('liq_d2_home_number_na')){
            $form->liq_d2_home_number = 'N/A';
        } else {
            $form->liq_d2_home_number = $request->input('liq_d2_home_number');
        }
        if($request->has('liq_d2_mobile_number_na')){
            $form->liq_d2_mobile_number = 'N/A';
        } else {
            $form->liq_d2_mobile_number = $request->input('liq_d2_mobile_number');
        }
        if($request->has('liq_d2_email_address_na')){
            $form->liq_d2_email_address = 'N/A';
        } else {
            $form->liq_d2_email_address = $request->input('liq_d2_email_address');
        }
        $form->liq_d2_address1 = $request->input('liq_d2_address1');
        $form->liq_d2_address2 = $request->input('liq_d2_address2');
        $form->liq_d2_address3 = $request->input('liq_d2_address3');
        $form->liq_d2_town = $request->input('liq_d2_town');
        $form->liq_d2_county = $request->input('liq_d2_county');
        $form->liq_d2_pcode = $request->input('liq_d2_pcode');
        $form->liq_d2_country = $request->input('liq_d2_country');
        if($request->has('x_tailored_emails')) {
            $form->x_tailored_emails = $request->input('x_tailored_emails');
        } else {
            $form->x_tailored_emails = 'No';
        }
        if($request->has('x_uk_mailings')) {
            $form->x_uk_mailings = $request->input('x_uk_mailings');
        } else {
            $form->x_uk_mailings = 'No';
        }
        if($request->has('x_how_did_you_hear') && $request->input('x_how_did_you_hear') == 'Other') {
            $form->x_how_did_you_hear = $request->input('x_how_did_you_hear2');
        } else {
            $form->x_how_did_you_hear = $request->input('x_how_did_you_hear');
        }
        $form->x_referrer_name = $request->input('x_referrer_name');
        $form->x_existing_account_name = $request->input('x_existing_account_name');
        $form->x_staff_member_name = $request->input('x_staff_member_name');
        $form->x_relationship_type1 = $request->input('x_relationship_type1');
        $form->x_client_name1 = $request->input('x_client_name1');
        $form->x_client_code1 = $request->input('x_client_code1');
        $form->x_relationship_type2 = $request->input('x_relationship_type2');
        $form->x_client_name2 = $request->input('x_client_name2');
        $form->x_client_code2 = $request->input('x_client_code2');
        $form->x_notes = $request->input('x_notes');
        $form->p_credit_limit = $request->input('p_credit_limit');
        $form->p_tax_type = $request->input('p_tax_type');
        $form->p_currency = $request->input('p_currency');
        if($request->has('p_invoice_email_only')) {
            $form->p_invoice_email_only = 'Yes';
        } else {
            $form->p_invoice_email_only = 'No';
        }
        if($request->has('p_email')) {
            $form->p_email = $request->input('p_email');
        }
        if($request->has('p_fee_quote')) {
            $form->p_fee_quote = $request->input('p_fee_quote');
        }
        $form->p_client_value = $request->input('p_client_value');
        $form->p_client_rating= $request->input('p_client_rating');
        $form->cdd_source = $request->input('cdd_source');
        $form->cdd_sort_of_business = $request->input('cdd_sort_of_business');
        if($request->has('cdd_date_and_place_na')){
            $form->cdd_date_and_place = 'N/A';
        } else {
            $form->cdd_date_and_place = $request->input('cdd_date_and_place');
        }
        $form->cr_company_search = $request->input('cr_company_search');
        $form->cr_copy_of_certificate = $request->input('cr_copy_of_certificate');
        $form->cr_list_of_directors = $request->input('cr_list_of_directors');
        $form->cr_list_of_names = $request->input('cr_list_of_names');
        $form->cr_photo_id = $request->input('cr_photo_id');
        $form->cr_due_diligence = $request->input('cr_due_diligence');
        $form->ir_satisfied = $request->input('ir_satisfied');
        if($request->input('ir_satisfied') == "Yes") {
            $form->ir_y_what_basis = $request->input('ir_y_what_basis');
        } else {
            $form->ir_y_what_basis = '';
        }
        $form->ir_photo_id_saved = $request->input('ir_photo_id');
        $form->ir_due_diligence = $request->input('ir_due_diligence');
        $form->pep_considered = $request->input('pep_considered');
        $form->pep_due_diligence = $request->input('pep_due_diligence');
        $form->pep_approved = $request->input('pep_approved');
        $form->ccr_business_understanding = $request->input('ccr_business_understanding');
        $form->ccr_services_understanding = $request->input('ccr_services_understanding');
        $form->ccr_concerns_ownership = $request->input('ccr_concerns_ownership');
        if($request->input('ccr_concerns_ownership') == "Yes") {
            $form->ccr_identify_concerns = $request->input('ccr_identify_concerns');
        } else {
            $form->ccr_identify_concerns = '';
        }
        $form->ccr_confirm_integrity = $request->input('ccr_confirm_integrity');
        $form->ccr_management_concerns = $request->input('ccr_management_concerns');
        if($request->input('ccr_management_concerns') == "Yes") {
            $form->ccr_identify_management_concerns = $request->input('ccr_identify_management_concerns');
        } else {
            $form->ccr_identify_management_concerns = $request->input('ccr_identify_management_concerns');
        }
        $form->ccr_financial_concerns = $request->input('ccr_financial_concerns');
        if($request->input('ccr_financial_concerns') == "Yes") {
            $form->ccr_identify_financial_concerns = $request->input('ccr_identify_financial_concerns');
        } else {
            $form->ccr_identify_financial_concerns = '';
        }
        $form->ccr_legal_environment = $request->input('ccr_legal_environment');
        if($request->input('ccr_legal_environment') == "Yes") {
            $form->ccr_confirm_legal_environment = $request->input('ccr_confirm_legal_environment');
        } else {
            $form->ccr_confirm_legal_environment = '';
        }
        $form->ccr_accountant = $request->input('ccr_accountant');
        if($request->input('ccr_accountant') == "Yes") {
            $form->ccr_accountant_concerns = $request->input('ccr_accountant_concerns');
        } else {
            $form->ccr_accountant_concerns = $request->input('ccr_accountant_concerns');
        }
        $form->ccr_accountant_frequency = $request->input('ccr_accountant_frequency');
        if($request->input('ccr_accountant_frequency') == "Yes") {
            $form->ccr_accountant_frequency_concerns = $request->input('ccr_accountant_frequency_concerns');
        } else {
            $form->ccr_accountant_frequency_concerns = '';
        }
        $form->ccr_firms = $request->input('ccr_firms');
        if($request->input('ccr_firms') == "Yes") {
            $form->ccr_firms_concerns = $request->input('ccr_firms_concerns');
        } else {
            $form->ccr_firms_concerns = '';
        }
        $form->ccr_risk = $request->input('ccr_risk');
        if($request->input('ccr_risk') == "Yes") {
            $form->ccr_risk_concerns = $request->input('ccr_risk_concerns');
        } else {
            $form->ccr_risk_concerns = '';
        }
        $form->ccr_confirm_engagement_director = $request->input('ccr_confirm_engagement_director');
        $form->ccr_confirm_engagement_manager = $request->input('ccr_confirm_engagement_manager');
        $form->ccr_exposure_regulated_business = $request->input('ccr_exposure_regulated_business');
        if($request->input('ccr_exposure_regulated_business') == "Yes") {
            $form->ccr_exposure_regulated_business_confirm = $request->input('ccr_exposure_regulated_business_confirm');
        } else {
            $form->ccr_exposure_regulated_business_confirm = '';
        }
        $form->ccr_availability_concerns = $request->input('ccr_availability_concerns');
        $form->ccr_partner_concerns = $request->input('ccr_partner_concerns');
        $form->ccr_staffing_engagement = $request->input('ccr_staffing_engagement');
        $form->ccr_client_connection = $request->input('ccr_client_connection');
        if($request->input('ccr_client_connection') == "Yes") {
            $form->ccr_client_connection_details = $request->input('ccr_client_connection_details');
        } else {
            $form->ccr_client_connection_details = '';
        }
        $form->ccr_already_service = $request->input('ccr_already_service');
        if($request->input('ccr_already_service') == "Yes") {
            $form->ccr_already_service_detail = $request->input('ccr_already_service_details');
        } else {
            $form->ccr_already_service_detail = '';
        }
        $form->ccr_staff_connection = $request->input('ccr_staff_connection');
        if($request->input('ccr_staff_connection') == "Yes") {
            $form->ccr_staff_connection_detail = $request->input('ccr_staff_connection_details');
        } else {
            $form->ccr_staff_connection_detail = '';
        }
        $form->ccr_previously_acted = $request->input('ccr_previously_acted');
        if($request->input('ccr_previously_acted') == "Yes") {
            $form->ccr_previously_acted_detail = $request->input('ccr_previously_acted_details');
        } else {
            $form->ccr_previously_acted_detail = '';
        }
        $form->ccr_potential_risk = $request->input('ccr_potential_risk');
        $form->client_authorization = $request->input('client_authorization');
        $form->client_process_completed = $request->input('client_process_completed');



        $form->save();
//dd($form);

        $client = Client::where('id',$clientid)->first();

        $parameters = [
            'crf' => ClientCRFForm::where('id',$form->id)->get()
        ];

        $pdf = PDF::loadView('clients.forms.pdf', $parameters)->setPaper('a4');
        $pdf->save(storage_path('app/crf/'.preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf')),$overwrite=true);



        $cform = new ClientForm();
        $cform->name = 'CRF Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name);
        $cform->crf_form_id = $form->id;
        $cform->form_type = "CRF Form";
        $cform->file = preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf');
        $cform->user_id = Auth::id();
        $cform->client_id = $clientid;
        $cform->save();

        //$cform->file =
        //return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');

        return redirect()->route('clients.forms',['client'=>$clientid])->with(['flash_success'=>'Client CRF Form Captured successfully.']);
    }

    public function editCrfForm($clientid,$formid){

        $client = Client::where('id',$clientid)->first();

        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $form = ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->first();
        //dd($form);

        $parameters = [
            'process_progress' => $process_progress,
            'view_process_dropdown' => Process::orderBy('name','asc')->get(),
            'steps' => $step_data,
            'client' => Client::where('id',$clientid)->first(),
            'crf' => ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->get()
        ];

        return view('clients.forms.edit')->with($parameters);
    }

    public function generateCrfForm($clientid,$formid){

        $clientform = ClientForm::where('crf_form_id',$formid)->first();
//dd($form);
        $client = Client::where('id',$clientid)->first();

        $auth = User::where('id',$clientform->signed_by)->first();

        if(isset($clientform->signed) && $clientform->signed > 0) {
            $parameters = [
                'auth' => $auth,
                'date_signed' => Carbon::parse($clientform->signed_date)->format('Y-m-d'),
                'crf' => ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->get()
            ];
        } else {
            $parameters = [
                'auth' => '',
                'date_signed' => '',
                'crf' => ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->get()
            ];
        }

        $pdf = PDF::loadView('clients.forms.pdf', $parameters)->setPaper('a4');
        return $pdf->stream('CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf');

    }

    public function updateCrfForm(Request $request,$clientid,$formid){
        $form = ClientCRFForm::find($formid);
        $form->form_date = $request->input('sec1_date');
        $form->client_code = $request->input('client_code');
        $form->client_name = $request->input('client_name');
        $form->director = $request->input('director');
        $form->manager = $request->input('manager');
        $form->form_completed_by = $request->input('form_completed_by');
        $form->office = $request->input('office');
        $form->contact_type = $request->input('contact_type');
        $form->business_type =  $request->input('business_type');
        $form->client_type = $request->input('client_type');
        $form->country = $request->input('sec3_country');
        if($request->input('sec3_country') == 'Other'){
            $form->country2 = $request->input('sec3_country2');
        } else {
            $form->country2 = '';
        }
        $form->industry = $request->input('sec3_industry');
        if($request->input('sec3_industry') == "Other") {
            $form->industry2 = $request->input('sec3_industry2');
        } else {
            $form->industry2 = '';
        }
        $form->year_end = $request->input('year_end');


        $form->services = '';
        if($request->has('provided_services')) {
            foreach ($request->input('provided_services') as $services):
                if ($form->services == '')
                    $form->services = $services;
                else
                    $form->services .= "," . $services;
            endforeach;
        }
        $form->company_secretarial_department = $request->input('company_secretarial_department');
        $form->company_registration_number = $request->input('company_reg_no');
        $form->audit_status = $request->input('audit_status');
        $form->cosec_filing = $request->input('cosec_filing');
        $form->ct_number = $request->input('ct_number');
        $form->vat_number = $request->input('vat_number');
        $form->tax_filing = $request->input('corp_tax_filing');
        $form->ct_filing_date = $request->input('ct_filing_date');
        $form->billing_address1 = $request->input('billing_address1');
        $form->billing_address2 = $request->input('billing_address2');
        $form->billing_address3 = $request->input('billing_address3');
        $form->billing_town = $request->input('billing_town');
        $form->billing_county = $request->input('billing_county');
        $form->billing_pcode = $request->input('billing_pcode');
        $form->billing_country = $request->input('billing_country');
        $form->business_address1 = $request->input('business_address1');
        $form->business_address2 = $request->input('business_address2');
        $form->business_address3 = $request->input('business_address3');
        $form->business_town = $request->input('business_town');
        $form->business_county = $request->input('business_county');
        $form->business_pcode = $request->input('business_pcode');
        $form->business_country = $request->input('business_country');
        $form->registered_address1 = $request->input('registered_address1');
        $form->registered_address2 = $request->input('registered_address2');
        $form->registered_address3 = $request->input('registered_address3');
        $form->registered_town = $request->input('registered_town');
        $form->registered_county = $request->input('registered_county');
        $form->registered_pcode = $request->input('registered_pcode');
        $form->registered_country = $request->input('registered_country');
        if($request->has('company_telephone_numbers_na')){
            $form->company_tel_number1 = 'N/A';
            $form->company_tel_number2 = 'N/A';
        } else {
            $form->company_tel_number1 = $request->input('company_telephone_numbers');
            $form->company_tel_number2 = $request->input('company_telephone_numbers2');
        }
        if($request->has('company_fax_numbers_na')){
            $form->company_fax_number = 'N/A';
        } else {
            $form->company_fax_number = $request->input('company_fax_numbers');
        }
        if($request->has('company_email_address_na')){
            $form->company_email_address = 'N/A';
        } else {
            $form->company_email_address = $request->input('company_email_address');
        }
        if($request->has('company_website_na')){
            $form->company_website = 'N/A';
        } else {
            $form->company_website = $request->input('company_website');
        }
        $form->i1_name = $request->input('i1_name');
        $form->i1_position = $request->input('i1_position');
        if($request->has('i1_office_number_na')){
            $form->i1_office_number = 'N/A';
        } else {
            $form->i1_office_number = $request->input('i1_office_number');
        }
        if($request->has('i1_home_number_na')){
            $form->i1_home_number = 'N/A';
        } else {
            $form->i1_home_number = $request->input('i1_home_number');
        }
        if($request->has('i1_mobile_number_na')){
            $form->i1_mobile_number = 'N/A';
        } else {
            $form->i1_mobile_number = $request->input('i1_mobile_number');
        }
        if($request->has('i1_email_address_na')){
            $form->i1_email_address = 'N/A';
        } else {
            $form->i1_email_address = $request->input('i1_email_address');
        }
        $form->i1_address1 = $request->input('i1_address1');
        $form->i1_address2 = $request->input('i1_address2');
        $form->i1_address3 = $request->input('i1_address3');
        $form->i1_town = $request->input('i1_town');
        $form->i1_county = $request->input('i1_county');
        $form->i1_pcode = $request->input('i1_pcode');
        $form->i1_country = $request->input('i1_country');
        if($request->has('i2_na')){
            $form->i2_na = '1';
            $form->i2_name = '';
            $form->i2_position = '';
            $form->i2_office_number = '';
            $form->i2_home_number = '';
            $form->i2_mobile_number = '';
            $form->i2_email_address = '';
            $form->i2_address1 = '';
            $form->i2_address2 = '';
            $form->i2_address3 = '';
            $form->i2_town = '';
            $form->i2_county = '';
            $form->i2_pcode = '';
            $form->i2_country = '';
        } else {
            $form->i2_na = '0';
            $form->i2_name = $request->input('i2_name');
            $form->i2_position = $request->input('i2_position');
            if($request->input('i2_office_number_na')){
                $form->i2_office_number = 'N/A';
            } else {
                $form->i2_office_number = $request->input('i2_office_number');
            }
            if($request->input('i2_home_number_na')){
                $form->i2_home_number = 'N/A';
            } else {
                $form->i2_home_number = $request->input('i2_home_number');
            }
            if($request->input('i2_mobile_number_na')){
                $form->i2_mobile_number = 'N/A';
            } else {
                $form->i2_mobile_number = $request->input('i2_mobile_number');
            }
            if($request->input('i2_email_address_na')){
                $form->i2_email_address = 'N/A';
            } else {
                $form->i2_email_address = $request->input('i2_email_address');
            }
            $form->i2_address1 = $request->input('i2_address1');
            $form->i2_address2 = $request->input('i2_address2');
            $form->i2_address3 = $request->input('i2_address3');
            $form->i2_town = $request->input('i2_town');
            $form->i2_county = $request->input('i2_county');
            $form->i2_pcode = $request->input('i2_pcode');
            $form->i2_country = $request->input('i2_country');
        }
        if($request->has('st_uk_tax_number_na')){
            $form->st_uk_tax_number = 'N/A';
        } else {
            $form->st_uk_tax_number = $request->input('st_uk_tax_number');
        }
        if($request->has('st_ni_number_na')){
            $form->st_ni_number = 'N/A';
        } else {
            $form->st_ni_number = $request->input('st_ni_number');
        }
        if($request->has('st_vat_number_na')){
            $form->st_vat_number = 'N/A';
        } else {
            $form->st_vat_number = $request->input('st_vat_number');
        }
        $form->st_personal_tax = $request->input('st_personal_tax');
        $form->st_personal_address1 = $request->input('st_personal_address1');
        $form->st_personal_address2 = $request->input('st_personal_address2');
        $form->st_personal_address3 = $request->input('st_personal_address3');
        $form->st_personal_town = $request->input('st_personal_town');
        $form->st_personal_county = $request->input('st_personal_county');
        $form->st_personal_pcode = $request->input('st_personal_pcode');
        $form->st_personal_country = $request->input('st_personal_country');
        $form->st_billing_address1 = $request->input('st_billing_address1');
        $form->st_billing_address2 = $request->input('st_billing_address2');
        $form->st_billing_address3 = $request->input('st_billing_address3');
        $form->st_billing_town = $request->input('st_billing_town');
        $form->st_billing_county = $request->input('st_billing_county');
        $form->st_billing_pcode = $request->input('st_billing_pcode');
        $form->st_billing_country = $request->input('st_billing_country');
        $form->st_business_address1 = $request->input('st_business_address1');
        $form->st_business_address2 = $request->input('st_business_address2');
        $form->st_business_address3 = $request->input('st_business_address3');
        $form->st_business_town = $request->input('st_business_town');
        $form->st_business_county = $request->input('st_business_county');
        $form->st_business_pcode = $request->input('st_business_pcode');
        $form->st_business_country = $request->input('st_business_country');
        $form->st_contact_name = $request->input('st_contact_name');
        if($request->has('st_office_number_na')){
            $form->st_office_number = 'N/A';
        } else {
            $form->st_office_number = $request->input('st_office_number');
        }
        if($request->has('st_home_number_na')){
            $form->st_home_number = 'N/A';
        } else {
            $form->st_home_number = $request->input('st_home_number');
        }
        if($request->has('st_mobile_number_na')){
            $form->st_mobile_number = 'N/A';
        } else {
            $form->st_mobile_number = $request->input('st_mobile_number');
        }
        if($request->has('st_fax_number_na')){
            $form->st_fax_number = 'N/A';
        } else {
            $form->st_fax_number = $request->input('st_fax_number');
        }
        if($request->has('st_email_address1_na')){
            $form->st_email_address1 = 'N/A';
        } else {
            $form->st_email_address1 = $request->input('st_email_address1');
        }
        if($request->has('st_email_address2_na')){
            $form->st_email_address2 = 'N/A';
        } else {
            $form->st_email_address2 = $request->input('st_email_address2');
        }
        if($request->has('st_website_na')){
            $form->st_website = 'N/A';
        } else {
            $form->st_website = $request->input('st_website');
        }
        if($request->has('p_partnership_ref_no_na')){
            $form->p_partnership_ref_no = 'N/A';
        } else {
            $form->p_partnership_ref_no = $request->input('p_partnership_ref_no');
        }
        if($request->has('p_uk_tax_number_na')){
            $form->p_uk_tax_number = 'N/A';
        } else {
            $form->p_uk_tax_number = $request->input('p_uk_tax_number');
        }
        if($request->has('p_ni_number_na')){
            $form->p_ni_number = 'N/A';
        } else {
            $form->p_ni_number = $request->input('p_ni_number');
        }
        if($request->has('p_vat_number_na')){
            $form->p_vat_number = 'N/A';
        } else {
            $form->p_vat_number = $request->input('p_vat_number');
        }
        $form->p_personal_tax = $request->input('p_personal_tax');
        $form->p_billing_address1 = $request->input('p_billing_address1');
        $form->p_billing_address2 = $request->input('p_billing_address2');
        $form->p_billing_address3 = $request->input('p_billing_address3');
        $form->p_billing_town = $request->input('p_billing_town');
        $form->p_billing_county = $request->input('p_billing_county');
        $form->p_billing_pcode = $request->input('p_billing_pcode');
        $form->p_billing_country = $request->input('p_billing_country');
        $form->p_business_address1 = $request->input('p_business_address1');
        $form->p_business_address2 = $request->input('p_business_address2');
        $form->p_business_address3 = $request->input('p_business_address3');
        $form->p_business_town = $request->input('p_business_town');
        $form->p_business_county = $request->input('p_business_county');
        $form->p_business_pcode = $request->input('p_business_pcode');
        $form->p_business_country = $request->input('p_business_country');
        if($request->has('p_office_number_na')){
            $form->p_office_number = 'N/A';
        } else {
            $form->p_office_number = $request->input('p_office_number');
        }
        if($request->has('p_fax_number_na')){
            $form->p_fax_number = 'N/A';
        } else {
            $form->p_fax_number = $request->input('p_fax_number');
        }
        if($request->has('p_email_address1_na')){
            $form->p_email_address1 = 'N/A';
        } else {
            $form->p_email_address1 = $request->input('p_email_address1');
        }
        if($request->has('p_email_address2_na')){
            $form->p_email_address2 = 'N/A';
        } else {
            $form->p_email_address2 = $request->input('p_email_address2');
        }
        if($request->has('p_website_na')){
            $form->p_website = 'N/A';
        } else {
            $form->p_website = $request->input('p_website');
        }
        $form->m1_address1 = $request->input('m1_address1');
        $form->m1_address2 = $request->input('m1_address2');
        $form->m1_address3 = $request->input('m1_address3');
        $form->m1_town = $request->input('m1_town');
        $form->m1_county = $request->input('m1_county');
        $form->m1_pcode = $request->input('m1_pcode');
        $form->m1_country = $request->input('m1_country');
        $form->m1_contact_name = $request->input('m1_contact_name');
        if($request->has('m1_home_number_na')){
            $form->m1_home_number = 'N/A';
        } else {
            $form->m1_home_number = $request->input('m1_home_number');
        }
        if($request->has('m1_mobile_number_na')){
            $form->m1_mobile_number = 'N/A';
        } else {
            $form->m1_mobile_number = $request->input('m1_mobile_number');
        }
        if($request->has('m1_email_address1_na')){
            $form->m1_email_address1 = 'N/A';
        } else {
            $form->m1_email_address1 = $request->input('m1_email_address1');
        }
        if($request->has('m1_email_address2_na')){
            $form->m1_email_address2 = 'N/A';
        } else {
            $form->m1_email_address2 = $request->input('m1_email_address2');
        }
        if($request->has('m2_na')) {
            $form->m2_address1 = '';
            $form->m2_address2 = '';
            $form->m2_address3 = '';
            $form->m2_town = '';
            $form->m2_county = '';
            $form->m2_pcode = '';
            $form->m2_country = '';
            $form->m2_contact_name = '';
            $form->m2_home_number = '';
            $form->m2_mobile_number = '';
            $form->m2_email_address1 = '';
            $form->m2_email_address2 = '';
            $form->m2_email_address2 = '';
        } else {
            $form->m2_address1 = $request->input('m2_address1');
            $form->m2_address2 = $request->input('m2_address2');
            $form->m2_address3 = $request->input('m2_address3');
            $form->m2_town = $request->input('m2_town');
            $form->m2_county = $request->input('m2_county');
            $form->m2_pcode = $request->input('m2_pcode');
            $form->m2_country = $request->input('m2_country');
            $form->m2_contact_name = $request->input('m2_contact_name');
            if ($request->input('m2_home_number_na')) {
                $form->m2_home_number = 'N/A';
            } else {
                $form->m2_home_number = $request->input('m2_home_number');
            }
            if ($request->input('m2_mobile_number_na')) {
                $form->m2_mobile_number = 'N/A';
            } else {
                $form->m2_mobile_number = $request->input('m2_mobile_number');
            }
            if ($request->input('m2_email_address1_na')) {
                $form->m2_email_address1 = 'N/A';
            } else {
                $form->m2_email_address1 = $request->input('m2_email_address1');
            }
            if ($request->input('m2_email_address2_na')) {
                $form->m2_email_address2 = 'N/A';
            } else {
                $form->m2_email_address2 = $request->input('m2_email_address2');
            }
        }
        if ($request->has('pen_ref_no_na')) {
            $form->pen_ref_no = 'N/A';
        } else {
            $form->pen_ref_no = $request->input('pen_ref_no');
        }
        if ($request->has('pen_tax_no')) {
            $form->pen_tax_no = 'N/A';
        } else {
            $form->pen_tax_no = $request->input('pen_tax_no');
        }
        $form->pen_tax_filing = $request->input('pen_tax_filing');
        $form->pen_tax_filing_date = $request->input('pen_tax_filing_date');
        $form->pen_billing_address1 = $request->input('pen_billing_address1');
        $form->pen_billing_address2 = $request->input('pen_billing_address2');
        $form->pen_billing_address3 = $request->input('pen_billing_address3');
        $form->pen_billing_town = $request->input('pen_billing_town');
        $form->pen_billing_county = $request->input('pen_billing_county');
        $form->pen_billing_pcode = $request->input('pen_billing_pcode');
        $form->pen_billing_country = $request->input('pen_billing_country');
        $form->pen_billing_contact_name = $request->input('pen_billing_contact_name');
        $form->pen_billing_position = $request->input('pen_billing_position');
        if ($request->has('pen_billing_office_number_na')) {
            $form->pen_billing_office_number = 'N/A';
        } else {
            $form->pen_billing_office_number = $request->input('pen_billing_office_number');
        }
        if ($request->has('pen_billing_home_number_na')) {
            $form->pen_billing_home_number = 'N/A';
        } else {
            $form->pen_billing_home_number = $request->input('pen_billing_home_number');
        }
        if ($request->has('pen_billing_mobile_number_na')) {
            $form->pen_billing_mobile_number = 'N/A';
        } else {
            $form->pen_billing_mobile_number = $request->input('pen_billing_mobile_number');
        }
        if ($request->has('pen_billing_fax_number_na')) {
            $form->pen_billing_fax_number = 'N/A';
        } else {
            $form->pen_billing_fax_number = $request->input('pen_billing_fax_number');
        }
        if ($request->has('pen_billing_email_address_na')) {
            $form->pen_billing_email_address = 'N/A';
        } else {
            $form->pen_billing_email_address = $request->input('pen_billing_email_address');
        }
        $form->pen_res_address1 = $request->input('pen_res_address1');
        $form->pen_res_address2 = $request->input('pen_res_address2');
        $form->pen_res_address3 = $request->input('pen_res_address3');
        $form->pen_res_town = $request->input('pen_res_town');
        $form->pen_res_county = $request->input('pen_res_county');
        $form->pen_res_pcode = $request->input('pen_res_pcode');
        $form->pen_res_country = $request->input('pen_res_country');
        $form->liq_company_registration_number = $request->input('liq_company_registration_number');
        $form->liq_tax_ref = $request->input('liq_tax_ref');
        $form->liq_d1_name = $request->input('liq_d1_name');
        $form->liq_d1_position = $request->input('liq_d1_position');
        if($request->has('liq_d1_office_number_na')){
            $form->liq_d1_office_number = 'N/A';
        } else {
            $form->liq_d1_office_number = $request->input('liq_d1_office_number');
        }
        if($request->has('liq_d1_home_number_na')){
            $form->liq_d1_home_number = 'N/A';
        } else {
            $form->liq_d1_home_number = $request->input('liq_d1_home_number');
        }
        if($request->has('liq_d1_mobile_number_na')){
            $form->liq_d1_mobile_number = 'N/A';
        } else {
            $form->liq_d1_mobile_number = $request->input('liq_d1_mobile_number');
        }
        if($request->has('liq_d1_email_address_na')){
            $form->liq_d1_email_address = 'N/A';
        } else {
            $form->liq_d1_email_address = $request->input('liq_d1_email_address');
        }
        $form->liq_d1_address1 = $request->input('liq_d1_address1');
        $form->liq_d1_address2 = $request->input('liq_d1_address2');
        $form->liq_d1_address3 = $request->input('liq_d1_address3');
        $form->liq_d1_town = $request->input('liq_d1_town');
        $form->liq_d1_county = $request->input('liq_d1_county');
        $form->liq_d1_pcode = $request->input('liq_d1_pcode');
        $form->liq_d1_country = $request->input('liq_d1_country');

        $form->liq_d2_name = $request->input('liq_d2_name');
        $form->liq_d2_position = $request->input('liq_d2_position');
        if($request->has('liq_d2_office_number_na')){
            $form->liq_d2_office_number = 'N/A';
        } else {
            $form->liq_d2_office_number = $request->input('liq_d2_office_number');
        }
        if($request->has('liq_d2_home_number_na')){
            $form->liq_d2_home_number = 'N/A';
        } else {
            $form->liq_d2_home_number = $request->input('liq_d2_home_number');
        }
        if($request->has('liq_d2_mobile_number_na')){
            $form->liq_d2_mobile_number = 'N/A';
        } else {
            $form->liq_d2_mobile_number = $request->input('liq_d2_mobile_number');
        }
        if($request->has('liq_d2_email_address_na')){
            $form->liq_d2_email_address = 'N/A';
        } else {
            $form->liq_d2_email_address = $request->input('liq_d2_email_address');
        }
        $form->liq_d2_address1 = $request->input('liq_d2_address1');
        $form->liq_d2_address2 = $request->input('liq_d2_address2');
        $form->liq_d2_address3 = $request->input('liq_d2_address3');
        $form->liq_d2_town = $request->input('liq_d2_town');
        $form->liq_d2_county = $request->input('liq_d2_county');
        $form->liq_d2_pcode = $request->input('liq_d2_pcode');
        $form->liq_d2_country = $request->input('liq_d2_country');
        if($request->has('x_tailored_emails')) {
            $form->x_tailored_emails = $request->input('x_tailored_emails');
        } else {
            $form->x_tailored_emails = 'No';
        }
        if($request->has('x_uk_mailings')) {
            $form->x_uk_mailings = $request->input('x_uk_mailings');
        } else {
            $form->x_uk_mailings = 'No';
        }
        if($request->has('x_how_did_you_hear') && $request->input('x_how_did_you_hear') == 'Other') {
            $form->x_how_did_you_hear = $request->input('x_how_did_you_hear2');
        } else {
            $form->x_how_did_you_hear = $request->input('x_how_did_you_hear');
        }
        $form->x_referrer_name = $request->input('x_referrer_name');
        $form->x_existing_account_name = $request->input('x_existing_account_name');
        $form->x_staff_member_name = $request->input('x_staff_member_name');
        $form->x_relationship_type1 = $request->input('x_relationship_type1');
        $form->x_client_name1 = $request->input('x_client_name1');
        $form->x_client_code1 = $request->input('x_client_code1');
        $form->x_relationship_type2 = $request->input('x_relationship_type2');
        $form->x_client_name2 = $request->input('x_client_name2');
        $form->x_client_code2 = $request->input('x_client_code2');
        $form->x_notes = $request->input('x_notes');
        $form->p_credit_limit = $request->input('p_credit_limit');
        $form->p_tax_type = $request->input('p_tax_type');
        $form->p_currency = $request->input('p_currency');
        if($request->has('p_invoice_email_only')) {
            $form->p_invoice_email_only = 'Yes';
        } else {
            $form->p_invoice_email_only = 'No';
        }
        if($request->has('p_email')) {
            $form->p_email = $request->input('p_email');
        }
        if($request->has('p_fee_quote')) {
            $form->p_fee_quote = $request->input('p_fee_quote');
        }
        $form->p_client_value = $request->input('p_client_value');
        $form->p_client_rating= $request->input('p_client_rating');
        $form->cdd_source = $request->input('cdd_source');
        $form->cdd_sort_of_business = $request->input('cdd_sort_of_business');
        if($request->has('cdd_date_and_place_na')){
            $form->cdd_date_and_place = 'N/A';
        } else {
            $form->cdd_date_and_place = $request->input('cdd_date_and_place');
        }
        $form->cr_company_search = $request->input('cr_company_search');
        $form->cr_copy_of_certificate = $request->input('cr_copy_of_certificate');
        $form->cr_list_of_directors = $request->input('cr_list_of_directors');
        $form->cr_list_of_names = $request->input('cr_list_of_names');
        $form->cr_photo_id = $request->input('cr_photo_id');
        $form->cr_due_diligence = $request->input('cr_due_diligence');
        if($request->input('ir_satisfied') == "Yes") {
            $form->ir_y_what_basis = $request->input('ir_y_what_basis');
        } else {
            $form->ir_y_what_basis = '';
        }
        $form->ir_photo_id_saved = $request->input('ir_photo_id');
        $form->ir_due_diligence = $request->input('ir_due_diligence');
        $form->pep_considered = $request->input('pep_considered');
        $form->pep_due_diligence = $request->input('pep_due_diligence');
        $form->pep_approved = $request->input('pep_approved');
        $form->ccr_business_understanding = $request->input('ccr_business_understanding');
        $form->ccr_services_understanding = $request->input('ccr_services_understanding');
        $form->ccr_concerns_ownership = $request->input('ccr_concerns_ownership');
        if($request->input('ccr_concerns_ownership') == "Yes") {
            $form->ccr_identify_concerns = $request->input('ccr_identify_concerns');
        } else {
            $form->ccr_identify_concerns = '';
        }
        $form->ccr_confirm_integrity = $request->input('ccr_confirm_integrity');
        $form->ccr_management_concerns = $request->input('ccr_management_concerns');
        if($request->input('ccr_management_concerns') == "Yes") {
            $form->ccr_identify_management_concerns = $request->input('ccr_identify_management_concerns');
        } else {
            $form->ccr_identify_management_concerns = $request->input('ccr_identify_management_concerns');
        }
        $form->ccr_financial_concerns = $request->input('ccr_financial_concerns');
        if($request->input('ccr_financial_concerns') == "Yes") {
            $form->ccr_identify_financial_concerns = $request->input('ccr_identify_financial_concerns');
        } else {
            $form->ccr_identify_financial_concerns = '';
        }
        $form->ccr_legal_environment = $request->input('ccr_legal_environment');
        if($request->input('ccr_legal_environment') == "Yes") {
            $form->ccr_confirm_legal_environment = $request->input('ccr_confirm_legal_environment');
        } else {
            $form->ccr_confirm_legal_environment = '';
        }
        $form->ccr_accountant = $request->input('ccr_accountant');
        if($request->input('ccr_accountant') == "Yes") {
            $form->ccr_accountant_concerns = $request->input('ccr_accountant_concerns');
        } else {
            $form->ccr_accountant_concerns = $request->input('ccr_accountant_concerns');
        }
        $form->ccr_accountant_frequency = $request->input('ccr_accountant_frequency');
        if($request->input('ccr_accountant_frequency') == "Yes") {
            $form->ccr_accountant_frequency_concerns = $request->input('ccr_accountant_frequency_concerns');
        } else {
            $form->ccr_accountant_frequency_concerns = '';
        }
        $form->ccr_firms = $request->input('ccr_firms');
        if($request->input('ccr_firms') == "Yes") {
            $form->ccr_firms_concerns = $request->input('ccr_firms_concerns');
        } else {
            $form->ccr_firms_concerns = '';
        }
        $form->ccr_risk = $request->input('ccr_risk');
        if($request->input('ccr_risk') == "Yes") {
            $form->ccr_risk_concerns = $request->input('ccr_risk_concerns');
        } else {
            $form->ccr_risk_concerns = '';
        }
        $form->ccr_confirm_engagement_director = $request->input('ccr_confirm_engagement_director');
        $form->ccr_confirm_engagement_manager = $request->input('ccr_confirm_engagement_manager');
        $form->ccr_exposure_regulated_business = $request->input('ccr_exposure_regulated_business');
        if($request->input('ccr_exposure_regulated_business') == "Yes") {
            $form->ccr_exposure_regulated_business_confirm = $request->input('ccr_exposure_regulated_business_confirm');
        } else {
            $form->ccr_exposure_regulated_business_confirm = '';
        }
        $form->ccr_availability_concerns = $request->input('ccr_availability_concerns');
        $form->ccr_partner_concerns = $request->input('ccr_partner_concerns');
        $form->ccr_staffing_engagement = $request->input('ccr_staffing_engagement');
        $form->ccr_client_connection = $request->input('ccr_client_connection');
        if($request->input('ccr_client_connection') == "Yes") {
            $form->ccr_client_connection_details = $request->input('ccr_client_connection_details');
        } else {
            $form->ccr_client_connection_details = '';
        }
        $form->ccr_already_service = $request->input('ccr_already_service');
        if($request->input('ccr_already_service') == "Yes") {
            $form->ccr_already_service_detail = $request->input('ccr_already_service_details');
        } else {
            $form->ccr_already_service_detail = '';
        }
        $form->ccr_staff_connection = $request->input('ccr_staff_connection');
        if($request->input('ccr_staff_connection') == "Yes") {
            $form->ccr_staff_connection_detail = $request->input('ccr_staff_connection_details');
        } else {
            $form->ccr_staff_connection_detail = '';
        }
        $form->ccr_previously_acted = $request->input('ccr_previously_acted');
        if($request->input('ccr_previously_acted') == "Yes") {
            $form->ccr_previously_acted_detail = $request->input('ccr_previously_acted_details');
        } else {
            $form->ccr_previously_acted_detail = '';
        }
        $form->ccr_potential_risk = $request->input('ccr_potential_risk');
        $form->client_authorization = $request->input('client_authorization');
        $form->client_process_completed = $request->input('client_process_completed');


        $form->save();

        $clientform = ClientForm::where('crf_form_id',$formid)->first();
//dd($form);
        $client = Client::where('id',$clientid)->first();

        $auth = User::where('id',$clientform->signed_by)->first();

        if(isset($clientform->signed) && $clientform->signed > 0) {
            $parameters = [
                'auth' => $auth,
                'date_signed' => Carbon::parse($clientform->signed_date)->format('Y-m-d'),
                'crf' => ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->get()
            ];
        } else {
            $parameters = [
                'auth' => '',
                'date_signed' => '',
                'crf' => ClientCRFForm::where('id',$formid)->where('client_id',$clientid)->get()
            ];
        }
//dd($auth);
        Storage::delete(storage_path('app/crf/'.preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf')));

        //$pdf = PDF::loadView('clients.forms.pdf', $parameters)->setPaper('a4');
        //$pdf->save(storage_path('app/crf/'.preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf')),$overwrite=true);



        //$cform->file =
        //return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');

        $cform = ClientForm::find($clientform->id);
        $cform->name = 'CRF Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name);
        $cform->crf_form_id = $form->id;
        $cform->form_type = "CRF Form";
        $cform->file = preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf');
        $cform->client_id = $clientid;
        $cform->save();

        return redirect()->route('clients.forms',['client'=>$clientid])->with(['flash_success'=>'Client CRF Form successfully updated.']);
    }

    public function signCrfForm($clientid,$formid){

        $now = now();

        DB::table('client_forms')->where('id',$formid)->update([
            'signed' => '1','signed_date' => $now,'signed_by'=>Auth::id(),'updated_at' => $now
        ]);

        $client_form = ClientForm::where('id',$formid)->first();

        $client = Client::where('id',$clientid)->first();
        $auth = Auth::user();

        $parameters = [
            'auth' => $auth,
            'date_signed' => Carbon::parse($now)->format('Y-m-d'),
            'crf' => ClientCRFForm::where('id',$client_form->crf_form_id)->where('client_id',$clientid)->get()
        ];

        Storage::delete(storage_path('app/crf/'.preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf')));

        $pdf = PDF::loadView('clients.forms.pdf', $parameters)->setPaper('a4');
        $pdf->save(storage_path('app/crf/'.preg_replace("/[^\w\-\.]/", '','CRF_Form - '.(isset($client->company) && $client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'.pdf')),$overwrite=true);



        //$cform->file =
        //return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');

        return redirect()->route('clients.forms',['client'=>$clientid])->with(['flash_success'=>'Client CRF Form successfully signed.']);
    }

    public function uploadforms(Client $client){
        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::orderBy('id','desc')->get();
        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();



        return view('clients.forms.upload')->with([
            'client' => $client->load('forms.user'),
            'process_progress' => $process_progress,
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray())
        ]);
    }

    public function storeuploadforms(StoreClientFormRequest $request,$clientid){

        if ($request->hasFile('file')) {
            $request->file('file')->store('crf');
        }

        $document = new ClientForm();
        if($request->input('form_type') == "CRF Form"){
            $document->name = "CRF Form";
            $document->form_type = "CRF Form";
        } else {
            $document->name = $request->input('name');
            $document->form_type = "Other";
        }
        $document->file = $request->file('file')->hashName();
        $document->user_id = auth()->id();
        $document->client_id = $clientid;

        $document->save();

        return redirect(route('clients.forms', $clientid))->with('flash_success', 'Form uploaded successfully');
    }

    public function edituploadforms(Client $client,$formid){
        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::where('id',$formid)->get();
        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();



        return view('clients.forms.editupload')->with([
            'client' => $client->load('forms.user'),
            'process_progress' => $process_progress,
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray())
        ]);
    }

    public function updateuploadforms(Request $request,$client, $form){

        $document = ClientForm::find($form);

        if ($request->hasFile('file')) {
            $request->file('file')->store('crf');
            $document->file = $request->file('file')->hashName();
        }

        if($request->input('form_type') == "CRF Form"){
            $document->name = "CRF Form";
            $document->form_type = "CRF Form";
        } else {
            $document->name = $request->input('name');
            $document->form_type = "Other";
        }
        $document->user_id = auth()->id();

        $document->save();

            return redirect(route('clients.forms', $client))->with('flash_success', 'Form updated successfully');

    }

    public function searchClients($search){
        $clients = Client::where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orWhere('company','like','%'.$search.'%')
            ->orWhere('id_number','like','%'.$search.'%')
            ->orWhere('email','like','%'.$search.'%')
            ->orWhere('contact','like','%'.$search.'%')
            ->get();

        $data = array();

        foreach ($clients as $client){
            array_push($data,[
                'id' => $client->id,
                'name' => ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name),
                'id_number' => ($client->id_number != null ? $client->id_number : '')
            ]);
        }

        return response()->json($data);
    }

    public function getClients(){
        $clients = Client::where('is_progressing',1)->orderBy('first_name')->get();

        $data = array();

        foreach ($clients as $client){
            array_push($data,[
                'id' => $client->id,
                'name' => ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name),
                'id_number' => ($client->id_number != null ? $client->id_number : '&nbsp;')
            ]);
        }

        return response()->json($data);
    }
}