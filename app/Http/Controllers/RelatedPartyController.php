<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Process;
use App\RelatedParty;
use App\RelatedPartyProcess;
use Illuminate\Http\Request;
use App\Client;
use App\Step;
use DB;
use App\Http\Requests\RelatedPartyRequest;
use App\User;
use App\Document;
use App\Template;
use App\EmailTemplate;
use App\Log;
use App\ActivityLog;
use App\ActionableTextData;
use App\RelatedPartyTextData;
use App\RelatedPartyTextareaData;
use App\RelatedPartyBooleanData;
use App\RelatedPartyDateData;
use App\ActionableDropdownData;
use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachment;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableDocumentData;
use App\ActionableTemplateEmail;
use App\ActionableTemplateEmailData;
use App\ActionableDocumentEmailData;
use App\ActionActivities;
use App\Actions;
use App\ActionsAssigned;
use App\ActivityComment;
use App\ClientActivity;
use App\ClientComment;
use App\ClientCRFForm;
use App\ClientUser;
use App\Config;
use App\EmailSignature;
use App\Events\NotificationEvent;
use Illuminate\Notifications\Action;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\TemplateMail;
use App\RelatedPartyDropdown;
use App\RelatedPartyDropdownData;

class RelatedPartyController extends Controller
{
    public function __construct()
    {
        $this->progress_colours = [
            'not_started' => 'background-color: rgba(64,159,255, 0.15)',
            'started' => 'background-color: rgba(255,255,70, 0.15)',
            'progress_completed' => 'background-color: rgba(60,255,70, 0.15)',
        ];
    }

    public function show($client_id){

        $config = Config::first();

        $client = Client::withTrashed()->find($client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order', 'asc')->get();
        $c_step_order = Step::where('id', $client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $client->process->getStageHex(0);
                $step_stage = 0;

                if ($c_step_order->order == $a_step->order) {
                    $progress_color = $client->process->getStageHex(1);
                    $step_stage = 1;
                }

                if ($c_step_order->order > $a_step->order) {
                    $progress_color = $client->process->getStageHex(2);
                    $step_stage = 2;
                }

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'stage' => $step_stage
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;

        $process_steps_with_names = null;
        $process_steps_by_order = null;
        if(isset($config->process_id)){
            $process_steps_with_names = Step::where('process_id', '=', $config->process_id)->orderBy('order')->pluck('name', 'id');
            $process_steps_by_order = Step::where('process_id', '=', $config->process_id)->orderBy('order')->pluck('id');
        }

        $related_parties = [];
        $this->getRelatedParties($client_id, $related_parties, 0);
        //dd($related_parties);

        $process = Process::where('process_type_id', '=', 2)->orderBy('id', 'desc')->first();

        $max_step = Step::where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('id', 'desc')->first();
//dd($related_parties);
        $parameters = [
            'client' => $client,
            'r' => (isset($config)?Step::where('process_id',$config->related_party_process)->orderBy('order','asc')->take(1)->first()->id:0),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'firststep' => 1,
            'process_steps_with_names' => ($process_steps_with_names != null ? $process_steps_with_names : ''),
            'process_steps_by_order' => ($process_steps_by_order != null ? $process_steps_by_order : ''),
            'related_parties' => $related_parties,
            'process_id' => isset($process->id)?$process->id:0,
            'max_step' => isset($max_step->id)?$max_step->id:0,

        ];
//dd($parameters);
        return view('relatedparties.related_parties')->with($parameters);
    }

    public function add($client_id, $related_party_id, $level_id){

        $client = Client::withTrashed()->find($client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order', 'asc')->get();
        $c_step_order = Step::where('id', $client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $client->process->getStageHex(0);

                if ($c_step_order->order == $a_step->order)
                    $progress_color = $client->process->getStageHex(1);

                if ($c_step_order->order > $a_step->order)
                    $progress_color = $client->process->getStageHex(2);

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;

        $clients_drop_down = Client::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id')->prepend('Select Client', '');
        $config = RelatedPartyProcess::first();

        $process = Process::where('process_type_id', '=', 2)->orderBy('id', 'desc')->first();

        //Next Level
        $next_step = Step::where('id', '>', $level_id)->where('process_id', '=', isset($process->id)?$process->id:0)->first();
        $this_level_id = isset($next_step->id)?$next_step->id:0;

        $max_step = Step::where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('id', 'desc')->first();
        //$step_levels = Step::where('process_id', '=', $process->id)->pluck('id');

        if($level_id == 0){
            $first_step = Step::where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('id')->first();
            $next_step = Step::where('id', '>', isset($first_step->id)?$first_step->id:0)->where('process_id', '=', isset($process->id)?$process->id:0)->first();
            $this_level_id = isset($next_step->id)?$next_step->id:0;
        }

        //dd($level_id);

        $step = Step::with('activities')->where('id', '=', $this_level_id)->first();

        $activities_array = [];
        $step_activities = isset($step)?$step->activities:[];
        foreach ($step_activities as $activity) {

            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'type' => $activity->getTypeName(),
                'client' => $activity->client_activity,
                'kpi' => $activity->kpi,
                'stage' => $activity->stage,
                'type_display' => $activity->actionable_type,
                'comment' => $activity->comment,
                'weight' => $activity->weight,
                'dropdown_item' => '',
                'dropdown_items' => [],
                'dropdown_values' => [],
                'is_dropdown_items_shown' => false,
                'max_step' => isset($max_step->id)?$max_step->id:0,
                'user' => $activity->user_id ?? 0
            ];

            if ($activity->getTypeName() == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name')->toArray();
            }

            array_push($activities_array, $activity_array);
        }

        //dd($activities_array);



        $process_activities_drop_down = Step::Where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('order')->pluck('name', 'id')->prepend('Select Step', '');

        $parameters = [
            'client' => $client,
            'related_party_id' => $related_party_id,
            'level_id' => $level_id,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'clients_drop_down' => $clients_drop_down,
            'process_activities_drop_down' => $process_activities_drop_down,
            'add_relative_party' => 'active',
            'step_level_id' => $this_level_id,//Fix
            'step' => $step,

            'step_dropdown' => Step::where('process_id',$client->process_id)->pluck('name','id'),
            'process' => Process::whereHas('steps')->orderBy('name','asc')->pluck('name','id')->prepend('Please Select','0'),
            'activities' => ($activities_array != null ? $activities_array : []),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'template_email_options' => EmailTemplate::orderBy('name')->pluck('name', 'id')
        ];

        return view('relatedparties.add')->with($parameters);

    }

    public function edit($process_id,$related_party_id){

        $related_party = RelatedParty::find($related_party_id);
        $related_party_min_step = Step::where('process_id',$process_id)->orderBy('order','asc')->first();
//dd($related_party_min_step);
        $client = Client::withTrashed()->find($related_party->client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();

        $step_data = [];

        $c_step_order = Step::where('id', $client->step_id)->where('process_id', $client->process_id)->withTrashed()->first();

        foreach ($steps as $a_step) {

            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if ($c_step_order["order"] == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if ($c_step_order["order"] > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }

            /*if ($c_step_order["order"] == $a_step->order && $client->completed_at != null && $a_step->process_id == $client->process_id) {
                $progress_color = $client->process->getStageHex(2);
            }*/

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'progress_color' => $progress_color,
                'process_id' => $a_step->process_id,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);
        }


        $process = Process::where('process_type_id', '=', 2)->orderBy('id', 'desc')->first();

        $max_step = Step::where('process_id', '=', $process->id)->orderBy('id', 'desc')->first();
        //$step_levels = Step::where('process_id', '=', $process->id)->pluck('id');

        $step = Step::with('activities.actionable.data')->where('process_id',$client->process_id)->first();
        $related_party_steps = Step::with('activities.actionable.data')->where('process_id',$process_id)->get();

        $related_party_step_data = [];

        foreach ($related_party_steps as $a_step) {
            $progress_color = $client->process->getStageHex(0);
            $step_stage2 = 0;

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'progress_color' => $progress_color,
                'process_id' => $a_step->process_id,
                'order' => $a_step->order,
                'step2' => $step_stage2
            ];

            array_push($related_party_step_data, $tmp_step);
        }

        $process_activities_drop_down = Step::Where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('order')->pluck('name', 'id')->prepend('Select Step', '');

        $parameters = [
            'client' => $client,
            'related_party_id' => $related_party->level_id,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'process_activities_drop_down' => $process_activities_drop_down,
            'add_relative_party' => 'active',
            'step' => $step,
            'related_party_steps' => $related_party_step_data,
            'process_progress' => $client->getRelatedPartyProcessStepProgress(Step::find($related_party_min_step->id)),
            'related_party' => $related_party,
            'step_dropdown' => Step::where('process_id',$client->process_id)->pluck('name','id'),
            'process' => Process::whereHas('steps')->orderBy('name','asc')->pluck('name','id')->prepend('Please Select','0'),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'template_email_options' => EmailTemplate::orderBy('name')->pluck('name', 'id')
        ];

        return view('relatedparties.edit')->with($parameters);

    }

    public function related_party($client_id,$process_id,$step_id,$related_party_id){

        $related_party = RelatedParty::find($related_party_id);
        $related_party_min_step = Step::where('process_id',$process_id)->orderBy('order','asc')->first();
        //dd($related_party_min_step);
        $client = Client::withTrashed()->find($related_party->client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();

        $step_data = [];

        $c_step_order = Step::where('id', $client->step_id)->where('process_id', $client->process_id)->withTrashed()->first();

        foreach ($steps as $a_step) {

            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if ($c_step_order["order"] == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if ($c_step_order["order"] > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }

            /*if ($c_step_order["order"] == $a_step->order && $client->completed_at != null && $a_step->process_id == $client->process_id) {
                $progress_color = $client->process->getStageHex(2);
            }*/

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'progress_color' => $progress_color,
                'process_id' => $a_step->process_id,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);
        }


        $process = Process::where('process_type_id', '=', 2)->orderBy('id', 'desc')->first();

        $max_step = Step::where('process_id', '=', $process->id)->orderBy('id', 'desc')->first();
        //$step_levels = Step::where('process_id', '=', $process->id)->pluck('id');

        $step = Step::with('activities.related_party.data')->where('process_id',$client->process_id)->first();
        $related_party_steps = Step::with(['activities.related_party.data'=>function($q) use ($related_party_id){
            $q->where('related_party_id',$related_party_id);
        }])->where('process_id',$process_id)->get();

        $related_party_step_data = [];

        foreach ($related_party_steps as $a_step) {
            $progress_color = $client->process->getStageHex(0);
            $step_stage2 = 0;

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'progress_color' => $progress_color,
                'process_id' => $a_step->process_id,
                'order' => $a_step->order,
                'stage2' => $step_stage2
            ];

            array_push($related_party_step_data, $tmp_step);
        }

        $process_activities_drop_down = Step::Where('process_id', '=', isset($process->id)?$process->id:0)->orderBy('order')->pluck('name', 'id')->prepend('Select Step', '');

        $parameters = [
            'client' => $client,
            'related_party_id' => $related_party_id,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'process_activities_drop_down' => $process_activities_drop_down,
            'add_relative_party' => 'active',
            'step' => $step,
            'related_party_steps' => $related_party_step_data,
            'process_progress' => $client->getRelatedPartyProcessStepProgress(($step_id == Step::find($related_party_min_step->id) ? Step::find($related_party_min_step->id) : Step::find($step_id)),$related_party_id),
            'related_party' => $related_party,
            'step_dropdown' => Step::where('process_id',$client->process_id)->pluck('name','id'),
            'process' => Process::whereHas('steps')->orderBy('name','asc')->pluck('name','id')->prepend('Please Select','0'),
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id'),
            'template_email_options' => EmailTemplate::orderBy('name')->pluck('name', 'id')
        ];

        return view('relatedparties.show')->with($parameters);

    }

    public function store(Request $request){

        $related_party = new RelatedParty();
        $related_party->description = $request->input('description');
        $related_party->client_id = $request->has('client_id')?$request->input('client_id'):0;
        $related_party->related_party_parent_id = $request->has('related_party_id')?$request->input('related_party_id'):0;
        $related_party->save();

        return response()->json("success");
    }

    public function update($related_party_id, Request $request){
//dd($request);
        $client = Client::find($request->has('client_id')?$request->input('client_id'):0);

        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;
            $step = Step::find($request->input('step_id'));
            $step->load(['activities.actionable.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);

            $id = $request->has('client_id')?$request->input('client_id'):0;
            $step = Step::find($request->input('step_id'));
            $step->load(['activities.related_party.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);
//dd($step);
            $all_activities_completed = false;
            foreach ($step->activities as $activity) {
                if(is_null($request->input($activity->id))){
                    if($request->input('old_'.$activity->id) != $request->input($activity->id)){
                        if(is_array($request->input($activity->id))){

                            $old = explode(',',$request->input('old_'.$activity->id));
                            $diff = array_diff($old,$request->input($activity->id));
                            //dd($diff);

                            foreach($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_'.$activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_'.$activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\RelatedPartyText':
                                RelatedPartyTextData::where('related_party_text_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\RelatedPartyTextarea':
                                RelatedPartyTextareaData::where('related_party_textarea_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\RelatedPartyDropdown':
                                RelatedPartyDropdownData::where('related_party_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\RelatedPartyBoolean':
                                RelatedPartyBooleanData::where('related_party_boolean_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\RelatedPartyDate':
                                RelatedPartyDateData::where('related_party_date_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if($request->input('old_'.$activity->id) == $request->input($activity->id)){
                        continue;
                    }
                    if(is_array($request->input($activity->id))){

                        $old = explode(',',$request->input('old_'.$activity->id));
                        $diff = array_diff($old,$request->input($activity->id));
                        //dd($diff);

                        foreach($request->input($activity->id) as $key => $value) {
                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_'.$activity->id);

                        $activity_log = new ActivityLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->activity_id = $activity->id;
                        $activity_log->activity_name = $activity->name;
                        $activity_log->old_value = $request->input('old_'.$activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    switch ($activity->actionable_type) {
                        case 'App\RelatedPartyBoolean':
                            RelatedPartyBooleanData::where('client_id',$client->id)->where('related_party_boolean_id',$activity->actionable_id)->where('data',$old)->delete();

                            RelatedPartyBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'related_party_boolean_id' => $activity->actionable_id,
                                'related_party_id' => $related_party_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\RelatedPartyText':

                            RelatedPartyTextData::insert([
                                'data' => $request->input($activity->id),
                                'related_party_text_id' => $activity->actionable_id,
                                'related_party_id' => $related_party_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\RelatedPartyTextarea':

                            RelatedPartyTextareaData::insert([
                                'data' => $request->input($activity->id),
                                'related_party_textarea_id' => $activity->actionable_id,
                                'related_party_id' => $related_party_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\RelatedPartyDate':

                            RelatedPartyDateData::insert([
                                'data' => $request->input($activity->id),
                                'related_party_date_id' => $activity->actionable_id,
                                'related_party_id' => $related_party_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\RelatedPartyDropdown':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    RelatedPartyDropdownData::insert([
                                        'related_party_dropdown_id' => $activity->actionable_id,
                                        'related_party_dropdown_item_id' => $value,
                                        'related_party_id' => $related_party_id,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    RelatedPartyDropdownData::where('client_id',$client->id)->where('related_party_dropdown_id',$activity->actionable_id)->whereIn('related_party_dropdown_item_id',$diff)->delete();
                                }



                            }
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }

            //Move process step to the next step if all activities completed
            $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

            $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

            //Handle files
            foreach($request->files as $key => $file):
                $file_activity = Activity::find($key);
                switch($file_activity->actionable_type){
                    case 'App\ActionableDocument':
                        $afile = $request->file($key);
                        $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$afile->getClientOriginalExtension();
                        $stored = $afile->storeAs('documents', $name);

                        $document = new Document;
                        $document->name = $file_activity->name;
                        $document->file = $name;
                        $document->user_id = auth()->id();
                        $document->client_id = $client->id;
                        $document->save();

                        ActionableDocumentData::insert([
                            'actionable_document_id' => $file_activity->actionable_id,
                            'document_id' => $document->id,
                            'client_id' => $client->id,
                            'user_id' => auth()->id(),
                            'duration' => 120
                        ]);
                        break;
                    default:
                        //todo capture detaults
                        break;
                }

            endforeach;

        }



        return redirect(route('relatedparty.show', $request->has('client_id')?$request->input('client_id'):null))->with('flash_success', 'Related party updated successfully.');
    }

    public function activities($client_id, $related_party_id){
        $client = Client::withTrashed()->find($client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order', 'asc')->get();
        $c_step_order = Step::where('id', $client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $client->process->getStageHex(0);
                $step_stage2 =0;

                if ($c_step_order->order == $a_step->order)
                    $progress_color = $client->process->getStageHex(1);

                if ($c_step_order->order > $a_step->order)
                    $progress_color = $client->process->getStageHex(2);

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'step2' => $step_stage2
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;

        $parameters = [
            'client' => $client,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
        ];
        return view('relatedparties.activities')->with($parameters);
    }

    public function getClient($client_id){

        $client = Client::find($client_id);
        return response()->json($client);

    }

    public function getRelatedParties($client_id, &$related_parties, $related_party_parent_id){
        $tmp_related_parties = RelatedParty::where('client_id', '=', $client_id)->where('related_party_parent_id', '=', $related_party_parent_id)->orderBy('related_party_parent_id')->get();

        $related_parties[$related_party_parent_id] = !$tmp_related_parties->isEmpty()?$tmp_related_parties:null;

        if(isset($related_parties[$related_party_parent_id]))
            foreach($related_parties[$related_party_parent_id] as $related_party):
                $this->getRelatedParties($client_id,$related_parties, $related_party->id);
            endforeach;
    }

    public function addActivities($client_id, $process_id, $step_id/*Request $request,$client_id, Process $process, Step $step*/)
    {

        $step = Step::with('activities')->where('id', '=', $step_id)->first();

        $client = Client::withTrashed()->find($client_id);

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user');

        $steps = Step::where('process_id', $client->process_id)->orderBy('order', 'asc')->get();
        $c_step_order = Step::where('id', $client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $client->process->getStageHex(0);

                if ($c_step_order->order == $a_step->order)
                    $progress_color = $client->process->getStageHex(1);

                if ($c_step_order->order > $a_step->order)
                    $progress_color = $client->process->getStageHex(2);

                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;

        $activities_array = [];
        foreach ($step->activities as $activity) {

            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'type' => $activity->getTypeName(),
                'client' => $activity->client_activity,
                'kpi' => $activity->kpi,
                'stage' => $activity->stage,
                'type_display' => $activity->actionable_type,
                'comment' => $activity->comment,
                'weight' => $activity->weight,
                'dropdown_item' => '',
                'dropdown_items' => [],
                'is_dropdown_items_shown' => false,
                'user' => $activity->user_id ?? 0
            ];

            if ($activity->getTypeName() == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name')->toArray();
            }

            array_push($activities_array, $activity_array);
        }

        $parameters = [
            'client' => $client,
            'step' => $step,
            'activities' => $activities_array,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'add_relative_party' => 'active',
        ];
        return view('relatedparties.addactivities')->with($parameters);

    }

}
