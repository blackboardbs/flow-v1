<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\ActionableDropdownData;
use App\Activity;
use App\Config;
use App\Process;
use App\Template;
use Illuminate\Http\Request;
use App\CustomReport;
use App\CustomReportColumns;
use Illuminate\Support\Facades\Auth;
use App\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Step;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class CustomReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $parameters = [
            'reports' => CustomReport::orderBy('name')->with('user')->get()
        ];
        return view('customreports.index')->with($parameters);
    }

    public function create(){

        $configs = Config::first();

        $parameters = [
            'process' => Process::all()->pluck('name','id')
        ];
        return view('customreports.create')->with($parameters);
    }

    public function store(Request $request){
        $creport = new CustomReport();
        $creport->name = $request->input('name');
        $creport->process_id = $request->input('process');
        $creport->user_id = Auth::id();
        $creport->save();

        $creport_id = $creport->id;

        foreach($request->input('activity') as $key => $value){
            $activity = new CustomReportColumns();
            $activity->custom_report_id = $creport_id;
            $activity->activity_id = $value;
            $activity->save();
        }

        return redirect(route('custom_report.index'))->with('flash_success', 'Custom report created successfully');
    }

    public function edit($custom_report_id){

        $report = CustomReport::where('id',$custom_report_id)->get();

        $parameters = [
            'reports' => $report,
            'process' => Process::all()->pluck('name','id')

        ];

        return view('customreports.edit')->with($parameters);
    }

    public function update($custom_report_id, Request $request){
        $creport = CustomReport::find($custom_report_id);
        $creport->name = $request->input('name');
        $creport->process_id = $request->input('process');
        $creport->user_id = Auth::id();
        $creport->save();

        CustomReportColumns::where('custom_report_id',$custom_report_id)->delete();

        foreach($request->input('activity') as $key => $value){
            $activity = new CustomReportColumns();
            $activity->custom_report_id = $custom_report_id;
            $activity->activity_id = $value;
            $activity->save();
        }

        return redirect(route('custom_report.index'))->with('flash_success', 'Custom report updated successfully');
    }

    public function show(Request $request,$custom_report_id){

        $request->session()->forget('path_route');

        $configs = Config::first();

        $clients = Client::with('process.steps.activities.actionable.data')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'))
        ->where('is_progressing','1');

        $clients = $clients->get();

        $report_name = '';
        $report_columns = array();
        $activity_id = array();
        $data = array();

        $report = CustomReport::with('custom_report_columns.activity_name')->where('id',$custom_report_id)->withTrashed()->get();

        foreach($report as $value){
            $report_name = $value->name;
            foreach($value->custom_report_columns as $report_activity) {
                array_push($report_columns, $report_activity->activity_name["name"]);
                array_push($activity_id, $report_activity->activity_name["id"]);
            }
        }

        //dd($report_columns);

            $client_data = new Collection();

            foreach ($clients as $client) {
                $data = [];

                foreach ($activity_id as $key => $value) {
                    $activity = Activity::where('id', $value)->first();
                    //dd($activity);

                    switch ($activity["actionable_type"]) {
                        case 'App\ActionableBoolean':
                            $yn_value = '';

                             $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                            if (isset($data2["data"]) && $data2["data"] == '1') {
                                $yn_value = "Yes";
                            }
                            if (isset($data2["data"]) && $data2["data"] == '0') {
                                $yn_value = "No";
                            }
                            array_push($data, $yn_value);
                            break;
                        case 'App\ActionableDate':
                            $data_value = '';

                            $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                $data_value = $data2["data"];

                            array_push($data, $data_value);
                            break;
                        case 'App\ActionableText':
                            $data_value = '';

                            $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                 $data_value = $data2["data"];

                                 array_push($data, $data_value);
                            break;
                        case 'App\ActionableDropdown':
                            $data_value = '';

                            /*if ($request->has('s') && $request->input('s') != '') {
                                $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity->actionable_id)
                                    ->where('client_id', $client->id)
                                    ->select('actionable_dropdown_item_id')
                                    ->get()->toArray();

                                foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                                    //dd($selected_drop_down_item);
                                    if (in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                        if ($key == sizeof($selected_drop_down_items) - 1)
                                            $data_value .= $selected_drop_down_item['item']['name'];
                                        else
                                            $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                                    }
                                endforeach;
                            }*/
                            $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                                foreach ($data2 as $key => $value):
                                    if(count($data2) > 1) {
                                        $data_value .= $value["item"]["name"] . ', ';
                                    } else {
                                        $data_value .= $value["item"]["name"];
                                    }
                                endforeach;
                                array_push($data, $data_value);
                                break;
                            case 'App\ActionableDocument':
                                $yn_value = '';

                                $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                if (isset($data2["data"]) && $data2["data"] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($data2["data"]) && $data2["data"] == '0') {
                                    $yn_value = "No";
                                }
                                array_push($data, $yn_value);
                                break;
                            case 'App\ActionableTemplateEmail':
                                $yn_value = '';

                                $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                if (isset($data2["data"]) && $data2["data"] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($data2["data"]) && $data2["data"] == '0') {
                                    $yn_value = "No";
                                }
                                array_push($data, $yn_value);
                                break;
                            case 'App\ActionableNotification':
                                $yn_value = '';

                                $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                if (isset($data2["data"]) && $data2["data"] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($data2["data"]) && $data2["data"] == '0') {
                                    $yn_value = "No";
                                }
                                array_push($data, $yn_value);
                                break;
                            case 'App\ActionableMultipleAttachment':
                                $yn_value = '';

                                $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                                if (isset($data2["data"]) && $data2["data"] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($data2["data"]) && $data2["data"] == '0') {
                                    $yn_value = "No";
                                }
                                array_push($data, $yn_value);
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                    }
                    $client_data->push([
                        'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                        'id' => $client->id,
                        'email' => $client->email,
                        'contact' => $client->contact,
                        'process' => $client->process->name,
                        'created_at' => $client->created_at->toDateString(),
                        'completed_at' => ($client->completed_at != null ? $client->completed_at->toDateString() : ''),
                        'completed_days' => $client->completed_days,
                        'step_id' => $client->step_id,
                        'step' => $client->getCurrentStep()->name,
                        'is_progressing' => $client->is_progressing,
                        'needs_approval' => $client->needs_approval,
                        'data' => $data
                ]);
            }
        //}

        if ($request->has('s') && $request->input('s') != '') {
            $step_input = $request->input('s');

            $client_data = $client_data->filter(function ($client_data) use ($step_input) {
                $search = array();

                if(stristr($client_data["company"],$step_input) !== false){
                    array_push($search,$client_data["id"]);
                }

                if(stristr($client_data["email"],$step_input) !== false){
                    array_push($search,$client_data["id"]);
                }

                foreach ($client_data["data"] as $key => $value){
                    if(stristr($value,$step_input) !== false){
                        array_push($search,$client_data["id"]);
                    }
                }
                //dd($search);
                return $client_data["id"] == in_array($client_data["id"],$search);
            });
        }

        if($request->has('step') && $request->input('step') !='' ){
            $step_input = $request->input('step');

                $client_data = $client_data->filter(function ($client_data) use ($step_input) {
                    return $client_data['step_id'] == $step_input;
                });
        }

        $client_data = $client_data->sortBy(function ($client_data, $key) {
            return $client_data['company'];
        });
        /*usort(collect($client_data)->toArray(), function ($item1, $item2) {
            return $item1['company'] <=> $item2['company'];
        });*/

        //dd($client_data);
        $parameters = [
            'report_id' => $custom_report_id,
            'report_name' => $report_name,
            'fields' => $report_columns,
            'clients' => $client_data,
            'steps' => Step::orderBy('process_id')->orderBy('order')->pluck('name', 'id')->prepend('All steps', ''),
            'activity' => ''
        ];
        return view('customreports.show')->with($parameters);
    }

    public function destroy($custom_report_id){
        CustomReport::destroy($custom_report_id);
        return redirect()->route("custom_report.index")->with('flash_success','Custom report deleted successfully');
    }

    public function pdfexport($custom_report_id,Request $request)
    {
        $configs = Config::first();

        $clients = Client::with('process.steps.activities.actionable.data')
            ->select('*', DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) as full_name"), DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'))
            ->where('is_progressing','1');

        $clients = $clients->get();

        $report_name = '';
        $report_columns = array();
        $activity_id = array();
        $data = array();

        $report = CustomReport::with('custom_report_columns.activity_name')->where('id',$custom_report_id)->get();
        //$report = CustomReport::leftJoin('custom_report_columns','custom_report.id','custom_report_columns.custom_report_id')->get();
        foreach($report as $value){
            $report_name = $value->name;
            foreach($value->custom_report_columns as $report_activity) {
                array_push($report_columns, $report_activity->activity_name["name"]);
                array_push($activity_id, $report_activity->activity_name["id"]);
            }
        }

        $client_data = new Collection();
        $activity_value = '';
        $completed_values = [];
        foreach ($clients as $client) {
            $data = [];

            foreach ($activity_id as $key => $value) {
                $activity = Activity::where('id', $value)->first();
                //$data_value = isset($completed_activity_clients_data[$client->id]) && trim($completed_activity_clients_data[$client->id]) != '' ? $completed_activity_clients_data[$client->id] : '';
                $completed_value = '';
                $selected_drop_down_names = '';

                $data2 = '';

                switch ($activity["actionable_type"]) {
                    case 'App\ActionableBoolean':
                        $yn_value = '';

                        $data2 = ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        if (isset($data2["data"]) && $data2["data"] == '1') {
                            $yn_value = "Yes";
                        }
                        if (isset($data2["data"]) && $data2["data"] == '0') {
                            $yn_value = "No";
                        }
                        array_push($data, $yn_value);
                        break;
                    case 'App\ActionableDate':
                        $data_value = '';

                        $data2 = ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        $data_value = $data2["data"];

                        array_push($data, $data_value);
                        break;
                    case 'App\ActionableText':
                        $data_value = '';

                        $data2 = ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        $data_value = $data2["data"];

                        array_push($data, $data_value);
                        break;
                    case 'App\ActionableDropdown':
                        $data_value = '';

                        /*if ($request->has('s') && $request->input('s') != '') {
                            $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity->actionable_id)
                                ->where('client_id', $client->id)
                                ->select('actionable_dropdown_item_id')
                                ->get()->toArray();

                            foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                                //dd($selected_drop_down_item);
                                if (in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                    if ($key == sizeof($selected_drop_down_items) - 1)
                                        $data_value .= $selected_drop_down_item['item']['name'];
                                    else
                                        $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                                }
                            endforeach;
                        }*/
                        $data2 = ActionableDropdownData::with('item')->where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->get();

                        foreach ($data2 as $key => $value):
                            if(count($data2) > 1) {
                                $data_value .= $value["item"]["name"] . ', ';
                            } else {
                                $data_value .= $value["item"]["name"];
                            }
                        endforeach;
                        array_push($data, $data_value);
                        break;
                    case 'App\ActionableDocument':
                        $yn_value = '';

                        $data2 = ActionableDocumentData::where('client_id', $client->id)->where('actionable_document_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        if (isset($data2["data"]) && $data2["data"] == '1') {
                            $yn_value = "Yes";
                        }
                        if (isset($data2["data"]) && $data2["data"] == '0') {
                            $yn_value = "No";
                        }
                        array_push($data, $yn_value);
                        break;
                    case 'App\ActionableTemplateEmail':
                        $yn_value = '';

                        $data2 = ActionableTemplateEmailData::where('client_id', $client->id)->where('actionable_template_email_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        if (isset($data2["data"]) && $data2["data"] == '1') {
                            $yn_value = "Yes";
                        }
                        if (isset($data2["data"]) && $data2["data"] == '0') {
                            $yn_value = "No";
                        }
                        array_push($data, $yn_value);
                        break;
                    case 'App\ActionableNotification':
                        $yn_value = '';

                        $data2 = ActionableNotificationData::where('client_id', $client->id)->where('actionable_notification_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        if (isset($data2["data"]) && $data2["data"] == '1') {
                            $yn_value = "Yes";
                        }
                        if (isset($data2["data"]) && $data2["data"] == '0') {
                            $yn_value = "No";
                        }
                        array_push($data, $yn_value);
                        break;
                    case 'App\ActionableMultipleAttachment':
                        $yn_value = '';

                        $data2 = ActionableMultipleAttachmentData::where('client_id', $client->id)->where('actionable_ma_id', $activity->actionable_id)->orderBy('created_at','desc')->take(1)->first();

                        if (isset($data2["data"]) && $data2["data"] == '1') {
                            $yn_value = "Yes";
                        }
                        if (isset($data2["data"]) && $data2["data"] == '0') {
                            $yn_value = "No";
                        }
                        array_push($data, $yn_value);
                        break;
                    default:
                        //todo capture defaults
                        break;
                }

                //dd($completed_value);
                //add to array

            }
            $client_data->push([
                'company' => ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name),
                'id' => $client->id,
                'email' => $client->email,
                'contact' => $client->contact,
                'process' => $client->process->name,
                'created_at' => $client->created_at->toDateString(),
                'completed_at' => ($client->completed_at != null ? $client->completed_at->toDateString() : ''),
                'completed_days' => $client->completed_days,
                'step_id' => $client->step_id,
                'step' => $client->getCurrentStep()->name,
                'is_progressing' => $client->is_progressing,
                'needs_approval' => $client->needs_approval,
                'data' => $data
            ]);
        }
        //}

        if ($request->has('pdf_s_selected') && $request->input('pdf_s_selected') != '') {
            $step_input = $request->input('pdf_s_selected');

            $client_data = $client_data->filter(function ($client_data) use ($step_input) {
                $search = array();

                if(stristr($client_data["company"],$step_input) !== false){
                    array_push($search,$client_data["id"]);
                }

                if(stristr($client_data["email"],$step_input) !== false){
                    array_push($search,$client_data["id"]);
                }

                foreach ($client_data["data"] as $key => $value){
                    if(stristr($value,$step_input) !== false){
                        array_push($search,$client_data["id"]);
                    }
                }
                //dd($search);
                return $client_data["id"] == in_array($client_data["id"],$search);
            });
        }

        if($request->has('pdf_process_step_selected') && $request->input('pdf_process_step_selected') !='' ){
            $step_input = $request->input('pdf_process_step_selected');

            $client_data = $client_data->filter(function ($client_data) use ($step_input) {
                return $client_data['step_id'] == $step_input;
            });

            /*$client_data = array_filter(collect($client_data)->toArray(),function ($var) use ($request){
                return (strpos($var,$request->input('step')) === false);
            });*/
        }

        $client_data = $client_data->sortBy(function ($client_data, $key) {
            return $client_data['company'];
        });


        $parameters = [
            'report_id' => $custom_report_id,
            'report_name' => $report_name,
            'fields' => $report_columns,
            'clients' => $client_data,
            'steps' => Step::orderBy('process_id')->orderBy('order')->pluck('name', 'id')->prepend('All steps', ''),
            'activity' => ''
        ];

        $pdf = PDF::loadView('pdf.customreport2', $parameters)->setPaper('a4')->setOrientation('landscape');
        return $pdf->download('clients_'.date('Y_m_d_H_i_s').'.pdf');
    }

    public function getActivities($processid){

        $step_arr = array();
        $activities_arr = array();

        $steps = Step::where('process_id',$processid)->get();

        foreach($steps as $step){
            $step_arr2 = array();

            $step_arr2['id'] = $step->id;
            $step_arr2['name'] = $step->name;

            //array_push($step_arr, $step_arr2);

            $activities = Activity::select('activities.id','activities.name','activities.actionable_type')->leftJoin('steps','steps.id','activities.step_id')->where('steps.process_id',$processid)->where('steps.deleted_at',null)->where('activities.deleted_at',null)->where('activities.step_id',$step->id)->orderBy('activities.step_id','asc')->orderBy('activities.order','asc')->get();
            //dd($activities);
            $step_arr2['activity'] = array();
            foreach($activities as $activity){


                array_push($step_arr2['activity'], [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->actionable_type,
                    'step' => $activity->step_id
                ]);


            }

            array_push($step_arr,$step_arr2);
        }



        return response()->json($step_arr);
    }

    public function getSelectedActivities($custom_report_id){

        $sa = array();

        $process = CustomReport::where('id',$custom_report_id)->first();

        $selected_activities = CustomReportColumns::select('activity_id')->where('custom_report_id',$custom_report_id)->get();

        foreach($selected_activities as $result){
            array_push($sa,$result->activity_id);
        }

        $step_arr = array();
        $activities_arr = array();

        $steps = Step::where('process_id',$process->process_id)->orderBy('order','asc')->get();

        foreach($steps as $step){
            $step_arr2 = array();

            $step_arr2['id'] = $step->id;
            $step_arr2['name'] = $step->name;

            //array_push($step_arr, $step_arr2);

            $activities = Activity::select('activities.id','activities.name','activities.actionable_type')->leftJoin('steps','steps.id','activities.step_id')->where('activities.step_id',$step->id)->where('steps.process_id',$process->process_id)->where('steps.deleted_at',null)->where('activities.deleted_at',null)->where('activities.step_id',$step->id)->orderBy('activities.step_id','asc')->orderBy('activities.order','asc')->get();

            $step_arr2['activity'] = array();
            foreach($activities as $activity){

                if(($key = array_search($activity->id, $sa)) === false) {
                    array_push($step_arr2['activity'], [
                        'id' => $activity->id,
                        'name' => $activity->name,
                        'type' => $activity->actionable_type,
                        'selected' => '0'
                    ]);
                } else {
                    array_push($step_arr2['activity'], [
                        'id' => $activity->id,
                        'name' => $activity->name,
                        'type' => $activity->actionable_type,
                        'selected' => '1'
                    ]);
                }
/*
                array_push($step_arr2['activity'], [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->actionable_type,
                    'step' => $activity->step_id
                ]);*/


            }

            array_push($step_arr,$step_arr2);
        }

        /*$activities = Activity::select('activities.id','activities.name','activities.actionable_type')->leftJoin('steps','steps.id','activities.step_id')->where('process_id',$process->process_id)->where('steps.deleted_at',null)->where('activities.deleted_at',null)->orderBy('activities.step_id','asc')->orderBy('activities.order','asc')->get();
        //dd($activities);
        foreach($activities as $activity){
            if(($key = array_search($activity->id, $sa)) === false) {
                array_push($activities_arr, [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->actionable_type,
                    'selected' => '0'
                ]);
            } else {
                array_push($activities_arr, [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->actionable_type,
                    'selected' => '1'
                ]);
            }
        }*/

        return response()->json($step_arr);
    }
}
