<?php
/**
 * Created by PhpStorm.
 * User: Klaas
 * Date: 2018/07/18
 * Time: 7:07 PM
 */

namespace App\Exports;
use Illuminate\Support\Collection;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DynamicReportExport implements FromCollection, WithHeadings
{
    public function __construct(Collection $clients)
    {
        $this->clients = $clients;
    }

    public function headings(): array
    {
        return [
            'Client ID',
            'Company',
            'Referrer',
            'Director',
            'Onboarding Lead',
            'Onboarding Member In Charge',
            'Email',
            'Contact',
            'Activity Name',
            'Activity Value',
            //'Selected DropDowns',
            /*'Activity Name',*/
            'Process',
            'Created At',
            'Completed At',
            'Completed Days',
            'Step'
        ];
    }

    public function collection()
    {
        //$collecton = $this->clients->only(['client_id', 'company']);
        return $this->clients;
    }
}