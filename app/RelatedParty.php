<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RelatedParty extends Model
{
    use SoftDeletes;
    private $related_parties = [];

    protected $dates = ['deleted_at'];

    public function actionable()
    {
        return $this->morphTo();
    }

    public function getRelatedParties($client_id, $level_id){
        return $this->where('client_id', '', $client_id)->where('level_id', '=', $level_id)->get();
    }

}
