<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function offices()
    {
        return $this->hasMany('App\Office');
    }
}
