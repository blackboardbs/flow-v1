<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('login');
});

Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('mlogin', 'Auth\LoginController@login')->name('mlogin');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::get('create/{client_id}', ['as' => 'messages.client', 'uses' => 'MessagesController@client_create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

Route::get('powerpoint', 'ReportController@powerpoint')->name('powerpoint');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('profile/{user?}', 'UserController@profile')->name('profile');
Route::get('settings', 'UserController@settings')->name('settings');
Route::post('settings/password', 'UserController@handlePassword')->name('settings.password');
Route::post('settings/profile', 'UserController@handleProfile')->name('settings.profile');
Route::post('settings/notifications', 'UserController@handleNotifications')->name('settings.notifications');

Route::get('/', 'HomeController@index')->name('home');
Route::get('recents', 'HomeController@recents')->name('recents');
Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('calendar', 'HomeController@calendar')->name('calendar');
Route::get('help', 'HelpController@create')->name('help.create');
Route::post('help', 'HelpController@store')->name('help.store');

Route::resource('emaillogs', 'EmailLogsController');
Route::get('emaillogs/{email_id}', 'EmailLogsController@show')->name('emaillogs.show');

Route::get('storage/crf', 'AssetController@getCrf')->name('crf_client');
Route::get('storage/avatar', 'AssetController@getAvatar')->name('avatar');
Route::get('storage/document', 'AssetController@getDocument')->name('document');
Route::get('storage/template', 'AssetController@getTemplate')->name('template');

Route::get('clients/amlreviewed', 'ClientController@amlReviewed')->name('clients.amlreviewed');
Route::get('clients/amlapproved', 'ClientController@amlApproved')->name('clients.amlapproved');
Route::get('clients/amlreviewedandapproved', 'ClientController@amlReviewedAndApproved')->name('clients.amlreviewedandapproved');
Route::get('clients/completed/{step_id}', 'ClientController@completed')->name('clients.completed');

Route::resource('clients', 'ClientController');
Route::get('clients/{client}/progress','ClientController@progress')->name('clients.progress');
Route::post('clients/{client}/progress','ClientController@storeProgress')->name('clients.storeprogress');
Route::post('clients/{client}/actions','ClientController@storeActions')->name('clients.storeactions');
Route::post('clients/{client}/progress/{process?}/{step?}','ClientController@completeStep')->name('clients.completestep');
Route::post('clients/{client}/progress2/{process?}/{step?}','ClientController@completeStep2')->name('clients.completestep2');
Route::get('clients/{client}/progress/{process}/{step}','ClientController@stepProgress')->name('clients.stepprogress');
Route::get('clients/{client}/progressaction/{step}/{action_id}','ClientController@stepProgressAction')->name('clients.stepprogressaction');
Route::get('clients/{client}/documents','ClientController@documents')->name('clients.documents');
Route::get('clients/{client}/forms','ClientController@forms')->name('clients.forms');
Route::get('clients/{client}/forms/uploadform','ClientController@uploadforms')->name('forms.uploadforms');
Route::get('clients/{client}/forms/editform/{formid}','ClientController@edituploadforms')->name('forms.editforms');
Route::post('clients/{client}/forms/storeform','ClientController@storeuploadforms')->name('forms.uploadstore');
Route::post('clients/{clientid}/forms/{formid}/formupdate','ClientController@updateuploadforms')->name('forms.formupdate');
Route::get('clients/{client}/forms/createcrf','ClientController@createCrfForm')->name('forms.createcrf');
Route::get('clients/{client}/forms/editcrf/{formid}','ClientController@editCrfForm')->name('forms.editcrf');
Route::get('clients/{client}/forms/signcrf/{formid}','ClientController@signCrfForm')->name('forms.signcrf');
Route::post('clients/{clientid}/forms/{formid}/updatecrf','ClientController@updateCrfForm')->name('forms.updatecrf');
Route::post('clients/{client}/forms/storecrf','ClientController@saveCrfForm')->name('forms.storecrf');
Route::get('clients/{client}/forms/{formid}/generatecrf','ClientController@generateCrfForm')->name('forms.generatecrf');
Route::get('clients/{client}/forms/upload','ClientController@saveCrfForm')->name('forms.upload');
Route::get('clients/{client}/forms/delete/{formid}','ClientController@deleteCrfForm')->name('crfforms.destroy');
Route::get('clients/{client_id}/completed/{step_id}/{newdate}','ClientController@complete')->name('clients.complete');
Route::get('clients/{client_id}/uncompleted/{step_id}','ClientController@uncomplete')->name('clients.uncomplete');
Route::get('clients/{client_id}/changecompleted/{step_id}/{newdate}','ClientController@changecomplete')->name('clients.changecomplete');
Route::post('clients/{client}/sendnotification/{activity?}','ClientController@sendNotification')->name('clients.sendnotification');
Route::post('clients/{client}/sendtemplate/{activity?}','ClientController@sendTemplate')->name('clients.sendtemplate');
Route::post('clients/{client}/senddocument/{activity?}','ClientController@sendDocument')->name('clients.senddocument');
Route::get('clients/{client}/viewtemplate/{template?}','ClientController@viewTemplate')->name('clients.viewtemplate');
Route::get('clients/{client}/viewdocument/{document?}','ClientController@viewDocument')->name('clients.viewdocument');
Route::put('clients/{client}/follow','ClientController@follow')->name('clients.follow');
Route::get('clients/{client}/activityprogress/{step}','ClientController@activityProgress')->name('clients.activityprogress');
Route::post('clients/{client}/storecomment','ClientController@storeComment')->name('clients.storecomment');
Route::post('clients/{client}/progressing','ClientController@storeProgressing')->name('clients.progressing');
Route::post('clients/{client}/approval','ClientController@approval')->name('clients.approval');
Route::post('clients/{client}/senddocuments/{activity?}','ClientController@sendDocuments')->name('clients.senddocuments');
Route::get('clients/{client}/delete','ClientController@destroy')->name('clients.delete');
Route::get('clients/show/{clientid}','ClientController@show')->name('clients.show');
Route::get('clients/{clientid}/restore','ClientController@restore')->name('clients.restore');
Route::post('clients/{clientid}/addcomment/{activityid}','ActivityCommentController@addComment');
Route::post('clients/{clientid}/showcomment/{activityid}','ActivityCommentController@showComment');
Route::post('clients/deletecomment/{commentid}','ActivityCommentController@destroyComment');
Route::post('clients/editcomment/{commentid}','ActivityCommentController@editComment');
Route::post('clients/updatecomment/{commentid}','ActivityCommentController@updateComment');

Route::get('clients/{client}/actions','ClientController@actions')->name('clients.actions');
Route::post('/clients/{client}/assignactivity','ActionsController@assignActivityToUser')->name('clients.assignactivity');
Route::get('/clients/getfirststep/{clientid}/{processid}','ProcessController@getProcessFirstStep');

Route::get('storeclientactivity/{token}','ClientController@createClientActivity')->name('clients.createclientactivity');
Route::post('storeclientactivity/{token}','ClientController@storeClientActivity')->name('clients.storeclientactivity');

Route::get('/clients/{clientid}/autocomplete_process/{processid}/{newprocess}','ClientController@autocompleteClientProcess');
Route::get('/clients/{clientid}/keep_process/{processid}/{newprocess}','ClientController@keepClientProcess');

Route::resource('referrers', 'ReferrerController');
Route::resource('documents', 'DocumentController');
Route::delete('documents/{id}/{client_id}', 'DocumentController@destroy')->name('documents.destroy');

Route::resource('templates', 'TemplateController');
Route::get('/templates/activities/{process_id}', 'TemplateController@getVars');

Route::get('roles', 'RoleController@index')->name('roles.index');
Route::get('roles/create', 'RoleController@create')->name('roles.create');
Route::post('roles', 'RoleController@store')->name('roles.store');
Route::put('roles', 'RoleController@update')->name('roles.update');
Route::delete('roles/{role?}', 'RoleController@destroy')->name('roles.destroy');

Route::resource('processes', 'ProcessController');
Route::get('processes/step_count/{process_id}', 'ProcessController@processStepCount');
Route::delete('processes/{process}/{processid}', 'ProcessController@destroy')->name('processes.destroy');
Route::get('processes/{process}/show', 'ProcessController@show')->name('processes.show');
Route::get('processes/{process}/steps/create', 'StepController@create')->name('steps.create');
Route::post('processes/{process}/steps/create', 'StepController@store')->name('steps.store');
Route::get('steps/{step}/edit', 'StepController@edit')->name('steps.edit');
Route::put('steps/{step}/edit', 'StepController@update')->name('steps.update');
Route::delete('steps/{step}', 'StepController@destroy')->name('steps.destroy');
Route::post('steps/{step}/move', 'StepController@move')->name('steps.move');

Route::resource('actions', 'ActionsController');
Route::get('actions', 'ActionsController@index')->name('action.index');
Route::post('actions/save', 'ActionsController@store')->name('action.save');
Route::post('actions/save_send', 'ActionsController@storeSend')->name('action.save_send');
Route::get('actions/{action_id}/edit', 'ActionsController@edit')->name('action.edit');
Route::post('actions/{action_id}/update', 'ActionsController@update')->name('action.update');
Route::post('actions/{action_id}/update_send', 'ActionsController@updateSend')->name('action.update_send');
Route::post('/get_process_steps/{process_id}','ActionsController@getProcessSteps');
Route::post('/get_action_process_activities/{process_id}/{step_id}','ActionsController@getActionActivity');
Route::post('/get_action_process_selected_activities/{process_id}/{step_id}','ActionsController@getSelectedActionActivity');
Route::post('/get_addaction_process_selected_activities/{process_id}/{step_id}','ActionsController@getSelectedAddActionActivity');
Route::post('/clear_addaction_activities','ActionsController@clearAddActionActivity');
Route::post('/set_addaction_activities/{activityid}','ActionsController@setAddActionActivity');
Route::post('/get_action_activities/{process_id}/{step_id}/{action_id}','ActionsController@getActionActivities');
Route::post('/get_edit_action_process_activities/{process_id}/{step_id}/{activity_id}','ActionsController@getEditActionActivity');
Route::post('/search_action_process_activities/{search}','ActionsController@searchActionActivity');
Route::post('/search_action_activities/{process_id}/{step_id}/{search}','ActionsController@searchActionActivities');
Route::post('/search_addaction_activities/{process_id}/{step_id}/{search}','ActionsController@searchAddActionActivities');
Route::post('/store_client_action/{client_id}','ActionsController@storeClientAction');
Route::post('/search_action/{search}','ActionsController@searchAction');
Route::post('/store_action_activity/{activity_id}','ActionsController@storeActionActivity');
Route::post('/store_addaction_activity/{activity_id}','ActionsController@storeAddActionActivity');
Route::get('actions/{id}/deactivate', 'ActionsController@deactivate')->name('action.deactivate');
Route::get('actions/{id}/activateuser', 'ActionsController@activate')->name('action.activate');
Route::get('/search_clients/{search}','ClientController@searchClients');
Route::get('/get_clients','ClientController@getClients');

Route::resource('custom_report', 'CustomReportController');
Route::get('custom_report/create', 'CustomReportController@create')->name('custom_report.create');
Route::get('custom_report/{custom_report_id}', 'CustomReportController@show')->name('custom_report.show');
Route::get('custom_report/{custom_report_id}/edit', 'CustomReportController@edit')->name('custom_report.edit');
Route::post('custom_report/{custom_report_id}/update', 'CustomReportController@update')->name('custom_report.update');
Route::get('custom_report/{custom_report_id}/pdfexport', 'CustomReportController@pdfexport')->name('custom_report.pdfexport');
Route::delete('custom_report/{custom_report_id}', 'CustomReportController@destroy')->name('custom_report.destroy');


Route::get('/get_report_activities/{process_id}','CustomReportController@getActivities');
Route::get('/get_report_selected_activities/{custom_report_id}','CustomReportController@getSelectedActivities');

//Route::resource('reports', 'ReportController');
Route::prefix('reports')->group(function () {
    Route::get('', 'ReportController@index')->name('reports.index');
    Route::post('store', 'ReportController@store')->name('reports.store');
    Route::get('create', 'ReportController@create')->name('reports.create');
    Route::delete('destroy/{report}', 'ReportController@destroy')->name('reports.destroy');
    Route::put('update/{report}', 'ReportController@update')->name('reports.update');
    //Todo - Invetigate why the resource statement above breaks the below report, thus the resource hardcode blog above
    Route::get('converted', 'ReportController@converted')->name('reports.converted');
    Route::get('fees', 'ReportController@fees')->name('reports.fees');
    Route::get('conversion', 'ReportController@conversion')->name('reports.conversion');
    Route::get('cch', 'ReportController@cch')->name('reports.cch');
    Route::get('feeproposalsent', 'ReportController@feeProposalSent')->name('reports.feeproposalsent');
    Route::get('aml', 'ReportController@aml')->name('reports.aml');
    Route::get('loe', 'ReportController@loe')->name('reports.loe');
    Route::get('loa', 'ReportController@loa')->name('reports.loa');
    Route::get('crf', 'ReportController@crf')->name('reports.crf');
    Route::get('referrer', 'ReportController@referrer')->name('reports.referrer');
    Route::get('clientreports', 'ReportController@clientReports')->name('reports.clientreports');
    Route::get('{activity}/show', 'ReportController@show')->name('reports.show');
    Route::get('{reportid}/edit', 'ReportController@edit')->name('reports.edit');
    Route::post('{reportid}/update', 'ReportController@update')->name('reports.update');
    Route::get('{activity}/export', 'ReportController@export')->name('reports.export');
    Route::get('{activity}/pdfexport', 'ReportController@pdfexport')->name('reports.pdfexport');
    Route::get('assigned_actions', 'ReportController@assignedactivities')->name('reports.assigned_actions');
    Route::post('powerpointexport', 'ReportController@powerpointExport')->name('reports.powerpointexport');
    Route::post('generate_report_export', 'ReportController@generateReportExport')->name('reports.generate_report_export');
    //Route::get('generatepowerpoint', 'ReportController@generatePowerPoint')->name('reports.powerpointgenerate');
    //Route::get('powerpointshow', 'ReportController@powerpointShow')->name('reports.powerpointshow');
    Route::get('generate_report', 'ReportController@generateReport')->name('reports.generate_report');
    Route::get('auditreport', 'ReportController@auditReport')->name('reports.auditreport');
});
Route::get('assigned_actions/{assignedactionid}/delete/{activityid}', 'ActionsController@deleteAssignedAction')->name('assignedactions.delete');
Route::get('assigned_actions/{assignedactionid}/complete/{activityid}', 'ActionsController@completeAssignedAction')->name('assignedactions.complete');


Route::prefix('graphs')->group(function () {
    Route::get('newclients', 'GraphController@newClients')->name('graphs.newclients');
    Route::get('targetdata', 'GraphController@targetData')->name('graphs.targetdata');
    Route::get('yearlycomparison', 'GraphController@yearlyComparison')->name('graphs.yearlycomparison');
    Route::get('qtrcomparison', 'GraphController@grtCompparison')->name('graphs.qtrcomparison');
    Route::get('gtrfeestotal', 'GraphController@gtrFeesTotal')->name('graphs.gtrfeestotal');
});

Route::resource('calendarevents', 'CalendarEventController');
Route::get('calendarevents/accept/{id}', 'CalendarEventController@accept')->name('calendarevents.accept');
Route::get('calendarevents/reject/{id}', 'CalendarEventController@reject')->name('calendarevents.reject');

Route::get('locations','LocationController@index')->name('locations.index');
Route::resource('divisions', 'DivisionController');
Route::resource('regions', 'RegionController');
Route::resource('areas', 'AreaController');
Route::resource('offices', 'OfficeController');

Route::resource('users', 'UserController');
Route::get('users/{user}/deactivate', 'UserController@deactivate')->name('users.deactivate');
Route::get('users/{user}/activateuser', 'UserController@activateuser')->name('users.activateuser');

Route::get('insight','InsightController@index')->name('insight.index');

Route::get('configs','ConfigController@index')->name('configs.index');
Route::put('configs','ConfigController@update')->name('configs.update');
Route::get('/get_process_steps/{process_id}','ConfigController@getProcessSteps');
Route::get('/get_process_avg_steps/{process_id}','ConfigController@getProcessAvgSteps');
Route::get('/get_outstanding_step/{process_id}','ConfigController@getOutstandingStep');
Route::get('/get_outstanding_activities/{step_id}','ConfigController@getOutstandingActivities');
Route::get('/get_process_steps_for_ageing/{process_id}','ConfigController@getProcessStepToCalculateAgeFrom');

Route::get('signin', 'AuthController@signin');
Route::get('/authorize', 'AuthController@gettoken');

Route::get('outlook/mail', 'OutlookController@mail')->name('outlook.mail');
Route::get('outlook/calendar', 'OutlookController@calendar')->name('outlook.calendar');


Route::get('activitieslog/{id}', 'LogController@activityLog')->name('activitieslog');

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/check_password/{password}', 'UserController@verifyPassword');

Route::resource('emailtemplates','EmailTemplateController');
Route::get('emailtemplates/create','EmailTemplateController@create')->name('emailtemplates.create');
Route::post('emailtemplates/store','EmailTemplateController@store')->name('emailtemplates.store');
Route::get('emailtemplates/{id}/edit','EmailTemplateController@edit')->name('emailtemplates.edit');
Route::post('emailtemplates/{id}/update','EmailTemplateController@update')->name('emailtemplates.update');
Route::delete('emailtemplates/{id}/destroy','EmailTemplateController@destroy')->name('emailtemplates.destroy');
Route::get('editemail/{id}','EmailTemplateController@ajaxedit');
Route::post('updateemail/{id}','EmailTemplateController@ajaxupdate');
Route::post('getsubject/{id}','EmailTemplateController@getSubject');

Route::resource('emailsignatures','EmailSignatureController');
Route::get('emailsignatures/create','EmailSignatureController@create')->name('emailsignatures.create');
Route::post('emailsignatures/store','EmailSignatureController@store')->name('emailsignatures.store');
Route::get('emailsignatures/{id}/edit','EmailSignatureController@edit')->name('emailsignatures.edit');
Route::post('emailsignatures/{id}/update','EmailSignatureController@update')->name('emailsignatures.update');
Route::delete('emailsignatures/{id}/destroy','EmailSignatureController@destroy')->name('emailsignatures.destroy');

Route::post('/readnotifications','UserController@readNotifications');
Route::post('/readnotificationshistory','UserController@readNotificationsHistory');
Route::post('/markallnotifications','UserController@markAllNotifications');

Route::resource('notifications', 'NotificationHistoryController');
Route::post('/getnotificationscount','UserController@getNotificationsCount');

Route::post('/getmessagecount','MessagesController@getMessageCount');
Route::post('/getmessages','MessagesController@getMessages');

Route::get('search','SearchController@getResults');

Route::resource('relatedparty','RelatedPartyController');
Route::post('relatedparty/save/{client_id}/{related_party_id}', 'RelatedPartyController@store');
Route::get('relatedparty/{client_id}/{process_id}/{step_id}/related_party/{related_party_id}', 'RelatedPartyController@related_party')->name('relatedparty.related_party');
Route::get('relatedparty/{process_id}/edit/{related_party_id}', 'RelatedPartyController@edit')->name('relatedparty.edit');
Route::post('relatedparty/update/{related_party_id}', 'RelatedPartyController@update')->name('relatedparty.update');
/*Route::get('relatedparty/add/{client_id}/{related_party_id}/{level_id}', 'RelatedPartyController@add')->name('relatedparty.add');*/
Route::get('relatedparty/activities/{client_id}/{related_party_id}', 'RelatedPartyController@activities')->name('relatedparty.activities');
Route::get('relatedparty/getclient/{client_id}', 'RelatedPartyController@getClient')->name('relatedparty.getclient');
Route::get('relatedparty/addactivities/{client_id}/{process_id}/{step_id}', 'RelatedPartyController@addActivities')->name('relatedparty.addactivities');
/*Route::get('relatedparty/editfirststep/{client_id}', 'RelatedPartyController@editFirstStep')->name('relatedparty.editfirststep');*/
/*Route::put('relatedparty/updatefirstlevel/{client_id}', 'RelatedPartyController@updateFirstLevel')->name('relatedparty.updatefirstlevel');*/

/*Route::resource('relatedpartyprocess','RelatedPartyProcessController');*/
