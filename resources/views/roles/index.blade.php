@extends('adminlte.default')

@section('title') Roles @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('roles.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Role</a>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','company'=>'Branch'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('roles.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    {{Form::open(['url' => route('roles.update'), 'method' => 'put','class'=>'mt-3 mb-3'])}}

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                @foreach($permissions as $permission)
                    <th>{{$permission->display_name}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @forelse($roles as $role)
                <tr>
                    <td>{{$role->display_name}}</td>
                    @foreach($permissions as $permission)
                        <td>
                            <div class="custom-control custom-checkbox">
                                <input name="permission[{{$role->id.']['.$permission->id}}]" {{($role->permissions->where('id',$permission->id)->count() > 0) ? 'checked' : ''}} type="checkbox" class="custom-control-input" id="permission[{{$role->id.']['.$permission->id}}]" value="{{$permission->id}}">
                                <label class="custom-control-label" for="permission[{{$role->id.']['.$permission->id}}]">&nbsp;</label>
                            </div>
                        </td>
                    @endforeach
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No roles match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

    <div class="blackboard-fab mr-3 mb-3">
        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
    </div>

    {{Form::close()}}
</div>
@endsection
