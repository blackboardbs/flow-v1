@extends('adminlte.default')

@section('title') Recents @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="table-responsive ">
                <table class="table table-sm table-bordered blackboard-recents">
                    <thead>
                    <tr>
                        <th>
                            Clients
                            <a href="{{route('clients.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i> View all</a>
                        </th>
                    </tr>
                    </thead>
                    @forelse($clients as $client)
                        <tr>
                            <td>
                                <a href="{{route('clients.show',$client)}}">{{($client->company != null ? $client->company : $client->first_name.' '.$client->last_name)}}</a>
                                <span class="float-right text-muted"><small><i class="fa fa-clock-o"></i> {{$client->updated_at}}</small></span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No recent clients</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{--<div class="table-responsive">
                <table class="table table-sm table-bordered blackboard-recents">
                    <thead>
                    <tr>
                        <th>
                            Referrers
                            <a href="{{route('referrers.index')}}" class="btn btn-sm btn-dark float-right"><i class="fa fa-eye"></i> View all</a>
                        </th>
                    </tr>
                    </thead>
                    @forelse($referrers as $referrer)
                        <tr>
                            <td>
                                <a href="{{route('referrers.show',$referrer)}}">{{$referrer->name()}}</a>
                                <span class="float-right text-muted"><small><i class="fa fa-clock-o"></i> {{$referrer->updated_at}}</small></span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No recent referrers</td>
                        </tr>
                    @endforelse
                </table>
            </div>--}}
            <div class="table-responsive">
                <table class="table table-sm table-bordered blackboard-recents">
                    <thead>
                    <tr>
                        <th>
                            Documents
                            <a href="{{route('documents.index')}}" class="btn btn-sm btn-dark float-right"><i class="fa fa-eye"></i> View all</a>
                        </th>
                    </tr>
                    </thead>
                    @forelse($documents as $document)
                        <tr>
                            <td>
                                <a href="{{route('document',['q'=>$document->file])}}" target="_blank">{{$document->name}}</a>
                                <span class="float-right text-muted"><small><i class="fa fa-clock-o"></i> {{$document->updated_at}}</small></span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No recent documents</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered blackboard-recents">
                    <thead>
                    <tr>
                        <th>
                            Email Logs
                            <a href="{{route('emaillogs.index')}}" class="btn btn-sm btn-dark float-right"><i class="fa fa-eye"></i> View all</a>
                        </th>
                    </tr>
                    </thead>
                    @forelse($emails as $email)
                        <tr>
                            <td>
                                <a href="{{route('emaillogs.show',$email->id)}}" target="_blank" data-toggle="tooltip" data-html="true" data-placement="right" title="{{$email->to}}">{{$email->subject}}</a>
                                <span class="float-right text-muted"><small><i class="fa fa-clock-o"></i> {{$email->date}}</small></span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No recent emails</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
