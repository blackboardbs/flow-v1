@extends('adminlte.default')

@section('title') Email Templates @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    <a href="{{route('emailtemplates.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Email Template</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','company'=>'Branch'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('emailtemplates.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th class="last">Action</th>
            </tr>
            </thead>
            <tbody class="blackboard-locations">
            @foreach($template as $result)
                <tr>
                    <td><a href="{{route('emailtemplates.show',$result)}}">{{$result->name}}</a></td>
                    <td class="last">
                        <a href="{{route('emailtemplates.edit',$result)}}" class="btn btn-success btn-sm">Edit</a>
                        {{ Form::open(['method' => 'DELETE','route' => ['emailtemplates.destroy','id'=>$result],'style'=>'display:inline']) }}
                        <a href="#" class="delete deleteDoc btn btn-danger btn-sm">Delete</a>
                        {{Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
@endsection
