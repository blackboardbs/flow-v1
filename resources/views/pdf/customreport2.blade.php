<html>
<head>
    <title>Custom Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table{
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 5px;
            font-size:12px;
        }

        th{
            background:#ccc;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<body>
<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead class="btn-dark">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Contact</th>
            <th>Process</th>
            @foreach($fields as $key => $val)
                @if($val != null)
                    <th>{{$val}}</th>
                @endif
            @endforeach
            <th>Created</th>
            <th>Completed</th>
            <th><abbr title="Days taken to complete a clients process.">Duration</abbr></th>
            <th>Step</th>
        </tr>
        </thead>
        <tbody>
        @forelse($clients as $client)
            <tr>
                <td>{{$client['company']}}</td>
                <td>{{$client['email']}}</td>
                <td>{{$client['contact']}}</td>
                <td>{{$client['process']}}</td>
                @foreach($client["data"] as $key => $val)
                    <td>{{ $val }}</td>
                @endforeach
                <td>{{$client['created_at']}}</td>
                <td>{{$client['completed_at']}}</td>
                <td>{{$client['completed_days']}}</td>
                <td>{{$client['step']}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>