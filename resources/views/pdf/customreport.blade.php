<html>
<head>
    <title>Custom Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table{
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 5px;
            font-size:12px;
        }

        th{
            background:#ccc;
        }
    </style>
</head>
<body>
    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th>Referrer</th>
                <th>Director</th>
                <th>Onboarding Lead</th>
                <th>Onboarding Member In Charge</th>
                <th>Activity Name: {{$activity->name}}</th>
                {{--{!! $activity->actionable_type == "App\ActionableText" || $activity->actionable_type == "App\ActionableDate" || $activity->actionable_type == "App\ActionableDropdown"/*|| $activity->actionable_type == "App\ActionableDocument"*/ ? '<th>Activity Value</th>' : '' !!}--}}
                <th>Email</th>
                <th>Contact</th>
                <th>Process</th>
                <th>Created</th>
                <th>Completed</th>
                <th><abbr title="Days taken to complete a clients process.">Duration</abbr></th>
                <th>Step</th>
                {{--<th><abbr title="Is the client progressing or not."><i class="fa fa-tasks"></i></abbr></th>
                <th><abbr title="Does the client need approval."><i class="fa fa-tasks"></i></abbr></th>--}}
            </tr>
            </thead>
            <tbody>
            @forelse($clients as $client)
                <tr>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['company']}}</a></td>
                    <td>{{$client['referrer']}}</td>
                    <td>{{$client['director']}}</td>
                    <td>{{$client['onboardingl']}}</td>
                    <td>{{$client['onboardingm']}}</td>
                    {!! $activity->actionable_type == "App\ActionableDropdown" ? '<td>'.$client["activity_data"].'</td>' : '' !!}
                    {!! $activity->actionable_type == "App\ActionableDocument" || $activity->actionable_type == "App\ActionableBoolean" ? '<td>'.$client['completed_yn'].'</td>' : '' !!}
                    {!! $activity->actionable_type == "App\ActionableText" || $activity->actionable_type == "App\ActionableDate" ? '<td>'.$client['data_value'].'</td>' : '' !!}
                    <td>{{$client['email']}}</td>
                    <td>{{$client['contact']}}</td>
                    <td>{{$client['process']}}</td>
                    <td>{{$client['created_at']}}</td>
                    <td>{{$client['completed_at']}}</td>
                    <td>{{$client['completed_days']}}</td>
                    <td>{{$client['step']}}</td>
                    {{--<td>{!!($client['is_progressing'] ? '' : '<i class="fa fa-exclamation-triangle text-danger"></i>')!!}</td>
                    <td>{!!(!$client['needs_approval'] ? '' : '<i class="fa fa-asterisk text-danger"></i>')!!}</td>--}}
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</body>
</html>