@extends('adminlte.default')

@section('title') Templates @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('templates.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Template</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','country'=>'Country wide','region'=>'Region wide','office'=>'Office wide'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; of &nbsp;
        {{Form::select('p',['all'=>'All Time','week'=>'Last Week','month'=>'Last Month','year'=>'Last Year'],old('period'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}

        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('templates.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Size</th>
                <th>Uploader</th>
                <th>Added</th>
                <th class="last">Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($templates as $template)
                <tr>
                    <td><a href="{{route('template',['q'=>$template->file])}}" target="_blank" download="{{$template->name}}.{{$template->type()}}">{{$template->name}}</a></td>
                    <td>{{$template->type()}}</td>
                    <td>{{$template->size()}}</td>
                    <td><a href="{{route('profile',$template->user)}}" title="{{$template->user->name()}}"><img src="{{route('avatar',['q'=>$template->user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline" alt="{{$template->user->name()}}"/></a></td>
                    <td>{{$template->created_at->diffForHumans()}}</td>
                    <td class="last">
                        <a href="{{route('templates.edit',$template)}}" class="btn btn-success btn-sm">Edit</a>
                        {{ Form::open(['method' => 'DELETE','route' => ['templates.destroy', $template],'style'=>'display:inline']) }}
                        <a href="#" class="delete deleteDoc btn btn-danger btn-sm">Delete</a>
                        {{Form::close() }}
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No templates match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    </div>
@endsection
