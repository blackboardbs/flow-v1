@extends('adminlte.default')

@section('title') Actions @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('actions.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Action</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{--<form class="form-inline mt-3">
            Show &nbsp;
            {{Form::select('s',['all'=>'All','mine'=>'My','country'=>'Country wide','region'=>'Region wide','office'=>'Office wide','deleted'=>'Deleted'],old('selection'),['class'=>'form-control form-control-sm'])}}
            &nbsp; for &nbsp;
            {{Form::select('p',$processes ,old('p'),['class'=>'form-control form-control-sm'])}}
            &nbsp; for &nbsp;
            {{Form::select('c',['all'=>'All completions','no'=>'Not completed','yes'=>'Completed','progress'=>'Not progressing','approval'=>'Needs approval'],old('c'),['class'=>'form-control form-control-sm'])}}
            &nbsp; in &nbsp;
            {{Form::select('step',$steps ,old('step'),['class'=>'form-control form-control-sm'])}}
            &nbsp; from &nbsp;
            {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
            &nbsp; to &nbsp;
            {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
            &nbsp; matching &nbsp;
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
            </div>
            <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
            <a href="{{route('clients.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
        </form>--}}

        <hr>

        <div class="table-responsive">
            <table class="table table-bordered table-hover table-sm">
                <thead class="btn-dark">
                <tr>
                    <th>@sortablelink('name', 'Name')</th>
                    <th>@sortablelink('description', 'Description')</th>
                    <th>@sortablelink('activities', 'Activities Included')</th>
                    <th>@sortablelink('created_by', 'Added By')</th>
                    <th style="width: 150px;">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($actions as $action)
                    <tr>
                        <td>{{$action["name"]}}</td>
                        <td>{{$action["description"]}}</td>
                        <td>
                            @foreach($action["activity"] as $activity)
                                {{$activity}} <br/>
                            @endforeach
                        </td>
                        <td>
                            @foreach($action["created_by"] as $user)
                                {{$user}} <br/>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{route('action.edit',$action["id"])}}" class="btn btn-sm btn-success">Edit</a>
                            @if($action["status"] != 1)
                                <a class="reactivate btn btn-outline-danger btn-sm" href="{{route('action.activate',$action["id"])}}">Activate</a>
                            @else
                                <a class="deactivate btn btn-danger btn-sm" href="{{route('action.deactivate',$action["id"])}}">Deactivate</a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center"><small class="text-muted">No actions match those criteria.</small></td></td>
                    </tr>
                @endforelse
                </tbody>

            </table>
        </div>

        <small class="text-muted">Found <b>{{count($actions)}}</b> actions matching those criteria.</small>
    </div>
@endsection
