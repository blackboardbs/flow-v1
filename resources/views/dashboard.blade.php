@extends('adminlte.default')

@section('title') Dashboard @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <!--
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('l',['all'=>'All','mine'=>'My','company'=>'Branch'],old('l'),['class'=>'form-control form-control-sm'])}}
        &nbsp; per &nbsp;
        {{Form::select('r',['day'=>'Day','week'=>'Week','month'=>'Month','year'=>'Year'],old('r'),['class'=>'form-control form-control-sm'])}}
        &nbsp; from &nbsp;
        {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
        &nbsp; to &nbsp;
        {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
        &nbsp; for &nbsp;
        {{Form::select('p',$processes,old('p'),['class'=>'form-control form-control-sm','placeholder'=>'Please select'])}}
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('dashboard')}}" class="btn btn-sm btn-outline-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>
    -->

        <hr>
        @php
            $style = array('0'=>'bg-danger-gradient','1'=>'bg-warning-gradient','2'=>'bg-success-gradient','3'=>'bg-info-gradient','4'=>'bg-primary-gradient','5'=>'bg-secondary-gradient','6'=>'bg-danger-gradient','7'=>'bg-warning-gradient','8'=>'bg-success-gradient','9'=>'bg-info-gradient','10'=>'bg-primary-gradient');
        @endphp
        <div class="row mt-3 dashboard-region">
            @for($i = 0;$i < count($regions);$i++)

                {{-- Close the row off and start a new one for every 5 regions --}}
                @if($i == 5)
        </div>
        <div class="row mt-3 dashboard-region">
            <div class="col-lg col-md-6">
                @else
                    <div class="col-lg col-md-6">
                        @endif

                        <div class="card text-white {{--@php echo $style[$i] @endphp--}} blackboard-region" style="background:{{(isset($regions[$i]['colour']) ? $regions[$i]['colour'] : '')}}">
                            <div class="card-body">
                                @php $id = $regions[$i]['id'] @endphp
                                <h4><i class="fa fa-chart-line"></i> {{(!empty($client_step_counts[$id]) ? $client_step_counts[$id] : '0')}}</h4>
                                <p class="d-inline-block">{{$regions[$i]["name"]}}</p>
                                @if($regions[$i]['id'] == '5')
                                    <span class="float-right d-inline-block"><a href="{{route('clients.index',['step'=>$regions[$i]['id']])}}&c=no" class="btn btn-sm btn-outline-light"><i class="fa fa-share"></i> View</a></span>
                                @else
                                    <span class="float-right d-inline-block"><a href="{{route('clients.index',['step'=>$regions[$i]['id']])}}&c=all" class="btn btn-sm btn-outline-light"><i class="fa fa-share"></i> View</a></span>
                                @endif

                            </div>
                        </div>
                    </div>


                    @endfor
                    <div class="col-lg col-md-6">
                        <div class="card text-white completed-region blackboard-region">
                            <div class="card-body">

                                <h4><i class="fa fa-chart-line"></i> {{(!empty($client_converted_count) ? $client_converted_count : '0')}}</h4>
                                <p class="d-inline-block">Completed</p>
                                <span class="float-right d-inline-block"><a href="{{route('clients.index',['step'=>1000])}}&f=2019-01-01&t=2019-12-31" class="btn btn-sm btn-outline-light"><i class="fa fa-share"></i> View</a></span>
                            </div>
                        </div>
                    </div>
            </div>

    <div class="row pt-3">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Number of days to complete a client
                    <div class="float-right">
                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary btn-graph active" id="type-1-column">
                                <input type="radio" name="blackboard-dashboard-1-type"><i class="far fa-chart-bar"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-1-bar">
                                <input type="radio" name="blackboard-dashboard-1-type"><i class="fa fa-align-left"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-1-line">
                                <input type="radio" name="blackboard-dashboard-1-type"><i class="fa fa-chart-line"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-body p-1 pt-2 pb-2">
                    <div id="blackboard-dashboard-1" class="m-0" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Completed Clients
                    <div class="float-right">
                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary btn-graph active" id="type-2-column">
                                <input type="radio" name="blackboard-dashboard-2-type"><i class="far fa-chart-bar"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-2-bar">
                                <input type="radio" name="blackboard-dashboard-2-type"><i class="fa fa-align-left"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-2-line">
                                <input type="radio" name="blackboard-dashboard-2-type"><i class="fa fa-chart-line"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-body p-1 pt-2 pb-2">
                    <div id="blackboard-dashboard-2" class="m-0" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Average Step Lead Time
                    <div class="float-right">
                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary btn-graph active" id="type-3-column">
                                <input type="radio" name="blackboard-dashboard-3-type"><i class="far fa-chart-bar"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-3-bar">
                                <input type="radio" name="blackboard-dashboard-3-type"><i class="fa fa-align-left"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-3-line">
                                <input type="radio" name="blackboard-dashboard-3-type"><i class="fa fa-chart-line"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-body p-1 pt-2 pb-2">
                    <div id="blackboard-dashboard-3" class="m-0" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    Outstanding Activities for {{$outstanding_activity_name->name}}
                    <div class="float-right">
                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary active" id="type-4-column">
                                <input type="radio" name="blackboard-dashboard-4-type"><i class="far fa-chart-bar"></i>
                            </label>
                            <label class="btn btn-secondary btn-graph" id="type-4-bar">
                                <input type="radio" name="blackboard-dashboard-4-type"><i class="fa fa-align-left"></i>
                            </label>
                            <label class="btn btn-secondary  btn-graph" id="type-4-line">
                                <input type="radio" name="blackboard-dashboard-4-type"><i class="fa fa-chart-line"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-body p-1 pt-2 pb-2">
                    <div id="blackboard-dashboard-4" class="m-0" style="height: 250px;"></div>
                </div>
            </div>
        </div>
    </div>
    </div>



@endsection

@section('extra-js')
    <script>

        $(document).ready(function() {
            Highcharts.theme = {
                colors: ['#86bffd', '#17a2b8'],
                title: {
                    text: ''
                },
                chart: {
                    type: 'column'
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                xAxis: {
                    crosshair: true
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
            };

            Highcharts.setOptions(Highcharts.theme);


            let blackboardDashboard1 = Highcharts.chart('blackboard-dashboard-1', {
                yAxis: {
                    labels: {
                        formatter: function (x) {
                            return (this.value) + " days";
                        }
                    },
                    plotLines: [{
                        color: '#f6918c',
                        width: 2,
                        value: {{$config->onboard_days}},
                        dashStyle: 'shortdash',
                        zIndex: 5
                    }]
                },

                xAxis: {
                    categories: ['Min', 'Avg', 'Max'],
                },
                tooltip: {
                    formatter: function () {
                        return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' days</b>';
                    }
                },
                series: [{
                    data: [{{$client_onboard_times["minimum"]}}, {{$client_onboard_times["average"]}}, {{$client_onboard_times["maximum"]}}]
                }],
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        borderRadiusTopLeft: '3px',
                        borderRadiusTopRight: '3px',
                        point: {
                            events: {
                                click: function () {
                                    switch (this.category) {
                                        case 'Min':
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'a']) !!}';
                                            break;
                                        case 'Avg':
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'a']) !!}';
                                            break;
                                        case 'Max':
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'d']) !!}';
                                            break;
                                    }
                                }
                            }
                        }
                    }
                },
            });

            let blackboardDashboard2 = Highcharts.chart('blackboard-dashboard-2', {
                yAxis: {
                    labels: {
                        formatter: function (x) {
                            return (this.value) + " clients";
                        }
                    },
                    plotLines: [{
                        color: '#dc3545',
                        width: 2,
                        value: {{$config->onboards_per_day}},
                        dashStyle: 'shortdash',
                        zIndex: 5
                    }]
                },
                xAxis: {
                    categories: [
                        @foreach($client_onboards as $key => $client)
                            '{{$key}}',
                        @endforeach
                    ]
                },
                tooltip: {
                    formatter: function () {
                        return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' clients</b>';
                    }
                },
                series: [{
                    data: [
                        @foreach($client_onboards as $client)
                        {{$client}},
                        @endforeach
                    ]
                }],
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        borderRadiusTopLeft: '3px',
                        borderRadiusTopRight: '3px',
                        point: {
                            events: {
                                click: function () {
                                    let start = '';
                                    switch ('{{request()->input('r')}}') {
                                        case 'day':
                                            start = moment(this.category, 'DD MMMM YYYY');
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.format('YYYY-MM-DD');
                                            break;
                                        case 'week':
                                            start = moment(moment(this.category).format('WW YYYY'), 'WW YYYY');
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(6, 'days').format('YYYY-MM-DD');
                                            break;
                                        case 'month':
                                            start = moment(this.category, 'MMMM YYYY');
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(1, 'months').format('YYYY-MM-DD');
                                            break;
                                        case 'year':
                                            start = moment(this.category, 'YYYY');
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(1, 'years').format('YYYY-MM-DD');
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            let blackboardDashboard3 = Highcharts.chart('blackboard-dashboard-3', {
                yAxis: {
                    labels: {
                        formatter: function (x) {
                            return (this.value) + " days";
                        }
                    },
                },
                xAxis: {
                    categories: [
                        @foreach($process_average_times as $name => $step)
                            '{{$name}}',
                        @endforeach
                    ]
                },
                tooltip: {
                    formatter: function () {
                        return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' days</b>';
                    }
                },
                series: [{
                    data: [
                        @foreach($process_average_times as $name => $step)
                        {{$step}},
                        @endforeach
                    ]
                }],
                plotOptions: {
                    series: {
                        borderRadiusTopLeft: '3px',
                        borderRadiusTopRight: '3px'
                    }
                }
            });

            let blackboardDashboard4 = Highcharts.chart('blackboard-dashboard-4', {
                yAxis: [
                    {
                        title: {},
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    {
                        title: {},
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        },
                        opposite: true
                    }
                ],
                xAxis: {
                    categories: [
                        @foreach($process_outstanding_activities as $name => $amount)
                            '{{$name}}',
                        @endforeach
                    ]
                },
                series: [
                    {
                        name: 'Outstanding Activities',
                        data: [
                            @foreach($process_outstanding_activities as $amount)
                            {{$amount['user']}},
                            @endforeach
                        ]
                    },
                    /*{
                        name: 'Client Activities',
                        data: [
{{--@foreach($process_outstanding_activities as $amount)
                        {{$amount['client']}},
                        @endforeach--}}
                    ]
                }*/
                ],
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        borderRadiusTopLeft: '3px',
                        borderRadiusTopRight: '3px',
                        point: {
                            events: {
                                click: function () {
                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p')]) !!}&oa='+this.category+'&s=all&c=all&step={!! $config->dashboard_outstanding_step !!}';
                                }
                            }
                        }
                    }
                }
            });

            $('#type-1-bar').click(function () {
                blackboardDashboard1.update({
                    chart: {
                        type: 'bar'
                    }
                });
            });

            $('#type-1-line').click(function () {
                blackboardDashboard1.update({
                    chart: {
                        type: 'line'
                    }
                });
            });

            $('#type-1-column').click(function () {
                blackboardDashboard1.update({
                    chart: {
                        type: 'column'
                    }
                });
            });

            $('#type-2-bar').click(function () {
                blackboardDashboard2.update({
                    chart: {
                        type: 'bar'
                    }
                });
            });

            $('#type-2-line').click(function () {
                blackboardDashboard2.update({
                    chart: {
                        type: 'line'
                    }
                });
            });

            $('#type-2-column').click(function () {
                blackboardDashboard2.update({
                    chart: {
                        type: 'column'
                    }
                });
            });

            $('#type-3-bar').click(function () {
                blackboardDashboard3.update({
                    chart: {
                        type: 'bar'
                    }
                });
            });

            $('#type-3-line').click(function () {
                blackboardDashboard3.update({
                    chart: {
                        type: 'line'
                    }
                });
            });

            $('#type-3-column').click(function () {
                blackboardDashboard3.update({
                    chart: {
                        type: 'column'
                    }
                });
            });

            $('#type-4-bar').click(function () {
                blackboardDashboard4.update({
                    chart: {
                        type: 'bar'
                    }
                });
            });

            $('#type-4-line').click(function () {
                blackboardDashboard4.update({
                    chart: {
                        type: 'line'
                    }
                });
            });

            $('#type-4-column').click(function () {
                blackboardDashboard4.update({
                    chart: {
                        type: 'column'
                    }
                });
            });
        })
    </script>
@endsection