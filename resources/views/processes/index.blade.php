@extends('adminlte.default')

@section('title') {{$type_name}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processes.create')}}?t={{$process_type_id}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> {{$type_name}}</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','company'=>'Branch'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; of &nbsp;
        {{Form::select('p',['all'=>'All Time','week'=>'Last Week','month'=>'Last Month','year'=>'Last Year'],old('period'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('processes.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th>Division</th>
                <th>Region</th>
                <th>Area</th>
                <th>Office</th>
                <th>Created</th>
                <th>Modified</th>
                <th class="last">Actions</th>
            </tr>
            </thead>
            <tbody>
            @forelse($processes as $process)
                <tr>
                    <td>{{$process->name}}</td>
                    <td>{{$process->office->area->region->division->name}}</td>
                    <td>{{$process->office->area->region->name}}</td>
                    <td>{{$process->office->area->name}}</td>
                    <td>{{$process->office->name}}</td>
                    <td>{{$process->created_at->diffForHumans()}}</td>
                    <td>{{$process->updated_at->diffForHumans()}}</td>
                    <td class="last">
                        <a href="{{route('processes.show',$process)}}?t={{$process_type_id}}" class="btn btn-info btn-sm">View</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No {{$type_name}} match those criteria.</small></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    </div>
@endsection
