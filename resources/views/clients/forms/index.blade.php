@extends('clients.show')

@section('tab-content')
    <div class="col-lg-12">

        {{--@if($crf == 0)--}}
        <a href="{{route('forms.createcrf',['client'=>$client->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> New CRF Form</a>
        {{--@endif--}}
        <a href="{{route('forms.uploadforms',['client'=>$client->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Upload Form</a>
        {{--<a href="{{route('forms.upload',['client'=>$client->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Upload Form</a>--}}

        <div class="table-responsive mt-3">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>Name</th>
                    <th>Form Type</th>
                    <th>Type</th>
                    <th>Size</th>
                    <th>Uploader</th>
                    <th>Signed</th>
                    <th>Signed Date</th>
                    <th>Added</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($list as $crf)
                    <tr>
                        @if(strpos($crf->name, "CRF Form - ") !== false)
                        <td><a href="{{route('forms.generatecrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" target="_blank">{{$crf->name}}</a></td>
                        @else
                        <td><a href="{{route('crf_client',['q'=>$crf->file])}}" target="_blank">{{$crf->name}}</a></td>
                        @endif
                        <td>{{$crf->form_type}}</td>
                        <td>{{$crf->type()}}</td>
                        <td>{{$crf->size()}}</td>
                        <td><a href="{{route('profile',$crf->user)}}" title="{{$crf->user->name()}}"><img src="{{route('avatar',['q'=>$crf->user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline" alt="{{$crf->user->name()}}"/></a></td>
                        <td>{{($crf->signed && $crf->signed != null ? ($crf->signed == 1 ? 'Yes' : 'No') : '')}}</td>
                        <td>{{($crf->signed_date && $crf->signed_date != null ? \Illuminate\Support\Carbon::parse($crf->signed_date)->format('Y-m-d') : '')}}</td>
                        <td>{{$crf->created_at->diffForHumans()}}</td>
                        <td class="last">
                            @if(strpos($crf->name, "CRF Form - ") !== false)
                                @if(!isset($crf->signed) && $crf->signed != '1')
                                    {{Form::open(['url' => route('forms.signcrf',['clientid'=>$client->id,'formid'=>$crf->id]), 'method' => 'get','id'=>'crf_form'.$crf->id])}}
                                    <a href="javascript:void(0)" id="sign_form_{{$crf->id}}" class="btn btn-sm btn-dark" onclick="SignCRF({{$crf->id}},{{$client->id}})">Sign</a>&nbsp;
                                    {{Form::close()}}
                                @endif
                                    <a href="{{route('forms.editcrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" class="btn btn-success btn-sm">Edit</a>
                            @else
                                <a href="{{route('forms.editforms',['clientid'=>$client->id,'formid'=>$crf->id])}}" class="btn btn-success btn-sm">Edit</a>
                            @endif
                                {{--@if(strpos($crf->name, "CRF Form - Online") !== false)
                                    {{ Form::open(['id' => 'documentDelete','method' => 'DELETE','route' => ['crfforms.destroy','id'=>$crf->id,'crf_id' =>$crf->crf_form_id],'style'=>'display:inline']) }}
                                    <a href="#" class="delete deleteDoc btn btn-danger btn-sm">Delete</a>
                                    {{Form::close() }}
                                @else
                                @endif--}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No forms match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="modal fade" id="modalSignCRF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <div class="row">
                            <div class="md-form col-sm-12">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Kindly confirm by entering your password.</label>
                                <input type="hidden" id="defaultForm-form_id" class="form-control form-control-sm validate crf_form_id">
                                <input type="hidden" id="defaultForm-client_id" class="form-control form-control-sm validate crf_client_id">
                                <input type="password" id="defaultForm-pass" class="form-control form-control-sm validate">
                            </div>
                                <div class="md-form mb-4 col-sm-12">
                                <div class="sign_error_message invalid-feedback"></div>
                                </div>
                            </div>
                                <div class="row text-center col-sm-12">
                                <button class="btn btn-sm btn-default" id="signcrf">Sign</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="cancelsigncrf">Cancel</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        function SignCRF(id,client_id){

            $('#modalSignCRF').modal('show');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#modalSignCRF").find(".crf_form_id").val(id);
            $("#modalSignCRF").find(".crf_client_id").val(client_id);



        }

        $('#signcrf').on('click',function (e) {
            var id = $("#modalSignCRF").find(".crf_form_id").val();
            var pwd = $("#modalSignCRF").find("#defaultForm-pass").val();

            if(pwd === '') {
                $("#modalSignCRF").find(".sign_error_message").html('The password you entered is incorrect.');
                $("#modalSignCRF").find(".invalid-feedback").css('display','block');
            } else {

                $.ajax({
                    url: '/check_password/' + pwd,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if (data.result !== 1) {
                            $("#modalSignCRF").find(".sign_error_message").html('The password you entered is incorrect.');
                            $("#modalSignCRF").find(".invalid-feedback").css('display','block');
                            return false

                        } else {
                            $("#modalSignCRF").find(".invalid-feedback").css('display','none');
                            $("#modalSignCRF").modal('hide');
                            $('#crf_form'+id).submit();

                        }
                    }
                });
            }


        })

        $("#cancelsigncrf").on('click',function (e){
            e.preventDefault();
            $("#modalSignCRF").modal('hide');

        })
    </script>
@endsection