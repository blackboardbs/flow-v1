@extends('adminlte.default')

@section('title') Clients @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('clients.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Client</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','country'=>'Country wide','region'=>'Region wide','office'=>'Office wide','deleted'=>'Deleted'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; for &nbsp;
        {{Form::select('p',$processes ,old('p'),['class'=>'form-control form-control-sm'])}}
        &nbsp; for &nbsp;
        {{Form::select('c',['all'=>'All completions','no'=>'Not converted','yes'=>'Converted','progress'=>'Progressing','noprogress'=>'Not progressing','approval'=>'Needs approval'],old('c'),['class'=>'form-control form-control-sm'])}}
        &nbsp; in &nbsp;
        {{Form::select('step',$steps ,old('step'),['class'=>'form-control form-control-sm'])}}
        &nbsp; from &nbsp;
        {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
        &nbsp; to &nbsp;
        {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('clients.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-hover table-sm">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('company', 'Name')</th>
                <th>@sortablelink('email', 'Email')</th>
                <th>@sortablelink('contact', 'Contact')</th>
                <th>@sortablelink('id_number', 'ID Number')</th>
                <th>@sortablelink('company_registration_number', 'Company Registration Number')</th>
                <th>@sortablelink('cif_code', 'CIF Code')</th>
                {{--<th>@sortablelink('process_id', 'Process')</th>--}}
                <th>@sortablelink('created', 'Created')</th>
                @if($np != 0)
                <th>@sortablelink('non_progressing', 'Non Progressing Date')</th>
                @endif
                <th>@sortablelink('completed', 'Completed')</th>
                <th><abbr title="Days taken to complete a clients process.">@sortablelink('process', 'Process')</abbr></th>
                <th>@sortablelink('step', 'Step')</th>
                {{--<th><abbr title="Is the client progressing or not."><i class="fa fa-tasks"></i></abbr></th>
                <th><abbr title="Does the client need approval."><i class="fa fa-tasks"></i></abbr></th>--}}
                <th>@sortablelink('introducer', 'User')</th>
            </tr>
            </thead>
            <tbody>
            @forelse($clients as $client)
                <tr>
                    <td><a href="{{route('clients.show',$client["id"])}}">{{($client["company"] == '' || $client["company"] == 'N/A' ?  : $client["company"] )}}</a></td>
                    <td>{{$client["email"]}}</td>
                    <td>{{$client["contact"]}}</td>
                    <td>{{$client["id_number"]}}</td>
                    <td>{{$client["company_registration_number"]}}</td>
                    <td>{{$client["cif_code"]}}</td>
                    {{--<td>{{$client["process"]}}</td>--}}
                    <td>{{$client["created_at"]}}</td>
                    @if($np != 0)
                        <td>{{$client["non_progressing"]}}</td>
                    @endif
                    <td>{{!is_null($client["completed_at"]) ? \Carbon\Carbon::parse($client["completed_at"])->format('Y-m-d') : ''}}</td>
                    <td>{{$client["process"]}}</td>
                    <td>{{$client["step"]}}</td>
                    {{--<td>{!!($client["is_progressing"]) ? '' : '<i class="fa fa-exclamation-triangle text-danger"></i>'!!}</td>
                    <td>{!!(!$client["needs_approval"]) ? '' : '<i class="fa fa-asterisk text-danger"></i>'!!}</td>--}}
                    <td><a href="{{route('profile',$client["introducer"])}}"><img src="{{route('avatar',['q'=>$client["avatar"] ])}}" class="blackboard-avatar blackboard-avatar-inline blackboard-avatar-navbar-img" alt="Avatar"/></a></td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
                </tr>
            @endforelse
            </tbody>

        </table>
    </div>

    <small class="text-muted">Found <b>{{count($clients)}}</b> clients matching those criteria.</small>
    </div>
@endsection
