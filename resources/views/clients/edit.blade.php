@extends('adminlte.default')

@section('title') Update Client @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('clients.show',$client)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />

    {{Form::open(['url' => route('clients.update', $client), 'method' => 'put'])}}

    <div class="form-group mt-3">
        {{Form::label('process', 'Process')}}
        {{Form::select('process',$processes,$client->process_id,['class'=>'form-control'. ($errors->has('process') ? ' is-invalid' : ''),'autofocus','id'=>'process'])}}
        @foreach($errors->get('process') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('first_name', 'First Name')}}
        {{Form::text('first_name',$client->first_name,['class'=>'form-control'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
        @foreach($errors->get('first_name') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('last_name', 'Last Name')}}
        {{Form::text('last_name',$client->last_name,['class'=>'form-control'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
        @foreach($errors->get('last_name') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>
    <div class="form-group">
        {{Form::label('id_number', 'ID Number')}}
        {{Form::text('id_number',$client->id_number,['class'=>'form-control'. ($errors->has('id_number') ? ' is-invalid' : ''),'placeholder'=>'ID Number'])}}
        @foreach($errors->get('id_number') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>
    <div class="form-group">
        {{Form::label('company', 'Company Name')}}
        {{Form::text('company',$client->company,['class'=>'form-control'. ($errors->has('company') ? ' is-invalid' : ''),'placeholder'=>'Company name'])}}
        @foreach($errors->get('company') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('company_registration_number', 'Company Registration Number')}}
        {{Form::text('company_registration_number',$client->company_registration_number,['class'=>'form-control form-control-sm'. ($errors->has('company_registration_number') ? ' is-invalid' : ''),'placeholder'=>'Company Registration Number'])}}
        @foreach($errors->get('company_registration_number') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('cif_code', 'CIF Code')}}
        {{Form::text('cif_code',$client->cif_code,['class'=>'form-control form-control-sm'. ($errors->has('cif_code') ? ' is-invalid' : ''),'placeholder'=>'CIF Code'])}}
        @foreach($errors->get('cif_code') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::email('email',$client->email,['class'=>'form-control'. ($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'Email'])}}
        @foreach($errors->get('email') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('contact', 'Contact Number')}}
        {{Form::text('contact',$client->contact,['class'=>'form-control'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
        @foreach($errors->get('contact') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('referrer', 'Referrer')}}
        {{Form::select('referrer',$referrers,$client->referrer_id,['class'=>'form-control'. ($errors->has('referrer') ? ' is-invalid' : ''),'placeholder'=>'N/A'])}}
        @foreach($errors->get('referrer') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-sm update-client">Update</button>
    </div>

    {{Form::close()}}
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function(){
            $('#process').on('change',function(){
                $.ajax({
                    dataType: 'json',
                    url: '/processes/step_count/'+$('#process').val(),
                    type:'GET'
                }).done(function(data) {
                    if(data === 0){
                        let process = $('#process').val();
                        $('#modalProcess').modal('show');
                        $('#modalProcess').find('.message').html('The process you selected currently has no steps.<br /><br />' +
                            'Click <a href="/processes/' + process + '/show">here</a> to add steps to this process now.');
                        $('body').find('.update-client').prop('disabled',true);
                    } else {
                        $('body').find('.update-client').prop('disabled',false);
                    }
                });
            })
        })
    </script>