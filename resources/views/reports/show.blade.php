@extends('adminlte.default')
@section('title') {{$activity->name}} Report @endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('reports.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-9">
            <form id="customreportsform" class="form-inline mt-3">
                @if(isset($activity->actionable_type))
                    {{$activity->name}}&nbsp;{{Form::select('activity',$filter_data ,old('activity'),['class'=>'form-control form-control-sm','id'=>'activity'])}}
                @endif
                &nbsp;&nbsp;&nbsp;Process Step&nbsp;{{Form::select('step',$steps ,old('step'),['class'=>'form-control form-control-sm','id'=>'step'])}}
                    &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    {{Form::text('s',old('s'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
                </div>
                <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
        <div class="col-sm-3" style="text-align: right;">
            <form id="download_pdf" class="form-inline mt-3" style="display: inline-block" action="{{route('reports.pdfexport', $activity)}}">
                <input type="hidden" id="pdf_activity_selected" name="pdf_activity_selected" value="{{isset($_GET['activity'])?$_GET['activity']:''}}" />
                <input type="hidden" id="pdf_process_step_selected" name="pdf_process_step_selected" value="{{isset($_GET['step'])?$_GET['step']:''}}" />
                <input type="hidden" id="pdf_s_selected" name="pdf_s_selected" value="{{isset($_GET['s'])?$_GET['s']:''}}" />
                <button style="margin-right: 10px;" type="submit" class="btn btn-default btn-sm"><i class="fa fa-file-pdf-o"></i> PDF</button>
            </form>
            <form id="download_excel" class="form-inline mt-3" style="display: inline-block" action="{{route('reports.export', $activity)}}">
                <input type="hidden" id="activity_selected" name="activity_selected" value="{{isset($_GET['activity'])?$_GET['activity']:''}}" />
                <input type="hidden" id="activity_selected_name" name="activity_selected_name" />
                <input type="hidden" id="process_step_selected" name="process_step_selected" value="{{isset($_GET['step'])?$_GET['step']:''}}" />
                <input type="hidden" id="process_step_selected_name" name="process_step_selected_name" />
                <input type="hidden" id="s_selected" name="s_selected" value="{{isset($_GET['s'])?$_GET['s']:''}}" />
                <button style="margin-right: 20px;" type="submit" class="btn btn-default btn-sm"><i class="fa fa-file-excel-o"></i> Excel</button>
            </form>
        </div>
    </div>
    <hr>
    <div class="table-responsive js-pscroll">
        <table class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: block;overflow-x:auto !important;max-height: 75vh;border-collapse: collapse">
            <thead class="btn-dark">
            <tr>
                <th>Name</th>
                <th>Referrer</th>
                <th>Director</th>
                <th>Onboarding Lead</th>
                <th>Onboarding Member In Charge</th>
                <th>Activity Name: {{$activity->name}}</th>
                {{--{!! $activity->actionable_type == "App\ActionableText" || $activity->actionable_type == "App\ActionableDate" || $activity->actionable_type == "App\ActionableDropdown"/*|| $activity->actionable_type == "App\ActionableDocument"*/ ? '<th>Activity Value</th>' : '' !!}--}}
                <th>Email</th>
                <th>Contact</th>
                <th>Process</th>
                <th>Created</th>
                <th>Completed</th>
                <th><abbr title="Days taken to complete a clients process.">Duration</abbr></th>
                <th>Step</th>
                {{--<th><abbr title="Is the client progressing or not."><i class="fa fa-tasks"></i></abbr></th>
                <th><abbr title="Does the client need approval."><i class="fa fa-tasks"></i></abbr></th>--}}
                <th>User</th>
                <th>Export</th>
            </tr>
            </thead>
            <tbody>
            @forelse($clients as $client)
                <tr>
                    <td class="table100-firstcol"><a href="{{route('clients.show',$client['id'])}}">{{$client['company']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['referrer']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['director']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['onboardingl']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['onboardingm']}}</a></td>
                    {!! $activity->actionable_type == "App\ActionableDropdown" ? '<td><a href="'.route('clients.show',$client['id']).'">'.$client["activity_data"].'</a></td>' : '' !!}
                    {!! $activity->actionable_type == "App\ActionableDocument" || $activity->actionable_type == "App\ActionableBoolean" ? '<td><a href="'.route('clients.show',$client['id']).'">'.$client['completed_yn'].'</a></td>' : '' !!}
                    {!! $activity->actionable_type == "App\ActionableText" || $activity->actionable_type == "App\ActionableDate" ? '<td><a href="'.route('clients.show',$client['id']).'">'.$client['data_value'].'</a></td>' : '' !!}
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['email']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['contact']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['process']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['created_at']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['completed_at']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['completed_days']}}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{{$client['step']}}</a></td>
                    {{--<td><a href="{{route('clients.show',$client['id'])}}">{!!($client['is_progressing'] ? '' : '<i class="fa fa-exclamation-triangle text-danger"></i>')!!}</a></td>
                    <td><a href="{{route('clients.show',$client['id'])}}">{!!(!$client['needs_approval'] ? '' : '<i class="fa fa-asterisk text-danger"></i>')!!}</a></td>--}}
                    <td><a href="{{route('profile',$client["introducer"])}}"><img src="{{route('avatar',['q'=>$client["introducer"]["avatar"] ])}}" class="blackboard-avatar blackboard-avatar-inline blackboard-avatar-navbar-img" alt="Avatar"/></a></td>
                    <td><a href="{{route('reports.powerpointexport',$client["id"])}}"><img src="{{ url('/powerpoint-icon.png')}}" /></a></td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
        <small class="text-muted">Found <b>{{$clients->count()}}</b> clients matching those criteria.</small>
    </div>

@endsection
@section('extra-css')

    <style>
        thead th {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            top: 0;
            z-index: 2;
            color: #fff;
            background-color: #343a40;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
        }

        tbody td:first-child {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            left: 0;
        }
        thead th:first-child {
            left: -1px;
            z-index: 3;
        }
        tbody td:first-child {
            left: -1px;
            z-index: 1;
            background: #FFFFFF;
            border-left: 1px solid #ffffff
        }

        .column-shadow{
            box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -webkit-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            border-left: 1px solid #dee2e6;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $('select[name="activity"], [name="step"]').change(function () {
            $('#customreportsform').submit();

        });

        $(document).ready(function()
            {
                $('.js-pscroll').each(function () {
                    var ps = new PerfectScrollbar(this);

                    $(window).on('resize', function () {
                        ps.update();
                    })

                    $(this).on('ps-x-reach-start', function () {
                        $('.table100-firstcol').removeClass('column-shadow');
                    });

                    $(this).on('ps-scroll-x', function () {
                        $('.table100-firstcol').addClass('column-shadow');
                    });

                });

                if($('#activity option:selected').val() > 0) {
                    $('#activity_selected_name').val($('#activity option:selected').text());
                }

                if($('#step option:selected').val() > 0) {
                    $('#process_step_selected_name').val($('#step option:selected').text());
                }
            }


        )
    </script>
@endsection