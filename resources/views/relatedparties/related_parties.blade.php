@extends('clients.show')
@section('tab-content')
    @php
        $counter = 0;

        function recursiveRelatedPartyAccordion($related_parties, $client_id, &$counter, $related_party_rarent_id, $process_id, $max_step,$r){

            if(!isset($related_parties[$related_party_rarent_id])){
                return 0;
            }
            foreach($related_parties[$related_party_rarent_id] as $related_party):
                if(!isset($related_party->id)){
                    continue;
                }

                $related_party_level_id = isset($related_party->level_id)?$related_party->level_id:-1;

                ++$counter;
    @endphp
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            @if(isset($related_parties[$related_party->id])){{--<i class="fa fa-caret-down"></i>--}}<i class="fa fa-caret-right"></i>@endif
                            <div style="display: inline; width: 90%;" data-toggle="collapse" data-parent="#accordion_{{$counter}}" href="#collapse_{{$counter}}">
                                {{isset($related_party->description)?$related_party->description:''}}
                            </div>
                            <span style="float: right;">
                                @if($related_party_level_id != $max_step)<a class="panel-btn btn-secondary" href="javascrip:void(0)" onclick="addRelatedParty({{$client_id}}, {{$related_party->id}})">+</a>&nbsp;@endif
                                <a href="{{route('relatedparty.related_party', [$client_id, $process_id, $r, (isset($related_party->id)?$related_party->id:-1)])}}" class="panel-btn">&#183;&#183;&#183;</a>
                            </span>
                        </h4>
                    </div>
                    <div id="collapse_{{$counter}}" class="panel-collapse collapse in">
                        <div class="panel-body">
                            @php
                                if(isset($related_parties[$related_party->id]))
                                    recursiveRelatedPartyAccordion($related_parties, $client_id, $counter, $related_party->id, $process_id, $max_step, $r);
                            @endphp
                        </div>
                    </div>
                </div>
    @php
        endforeach;
    }
    @endphp

    <div class="panel-group" id="related_parties_accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    @if(isset($related_parties[0])){{--<i class="fa fa-caret-down"></i>--}}<i class="fa fa-caret-right"></i>@endif
                    <div style="display: inline; width: 90%;" data-toggle="collapse" data-parent="#accordion1" href="#collapse_0">
                        {{isset($client->first_name)?$client->first_name:''}} {{isset($client->last_name)?$client->last_name:''}}
                    </div>
                    <span style="float: right;">

                        <a class="panel-btn btn-secondary" href="javascript:void(0)" onclick="addRelatedParty({{$client->id}}, 0)">+</a>&nbsp;

                        {{--@if($show_view == true)
                        <a href="{{route('relatedparty.editfirststep', $client)}}" class="panel-btn">&#183;&#183;&#183;</a>
                        @endif--}}
                    </span>
                </h4>
            </div>
            <div id="collapse_0" class="panel-collapse collapse in">
                <div class="panel-body">
                    @php
                        $related_party_parent_id = 0;
                        recursiveRelatedPartyAccordion($related_parties, $client->id, $counter, $related_party_parent_id, $process_id, $max_step,$r);
                    @endphp

                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="modalRelatedParties" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <input type="hidden" name="clientid" id="addrelatedclientid" />
                    <input type="hidden" name="relatedpartyid" id="addrelatedrelatedid" />
                    <div class="row step1">
                        <div class="md-form col-sm-12 pb-3 text-left">
                            <label data-error="wrong" data-success="right" for="defaultForm-pass">Related Party Description.</label>
                            <input type="text" id="addrelateddescription" name="addrelateddescription" class="form-control form-control-sm validate">
                        </div>
                        <div class="md-form mb-4 col-sm-12 text-center">
                            <button class="btn btn-sm btn-default" id="addrelatedsave">Save</button>&nbsp;
                            <button class="btn btn-sm btn-default" id="addrelatedcancel">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('extra-css')
    <style>
        #related_parties_accordion{
            width: 100%;
        }

        .panel{
            width: 100%;
            padding-left: 30px;
            padding-right: 30px;
        }

        .panel-heading{
            background-color: #343a40;
            border-radius: 4px;
            margin-top: 7px;
            margin-right: 0px;
            margin-bottom: 0px;
            margin-left: 7px;
            padding: 7px 0px 7px 7px;
            font-size: 14px;
            color: #ffffff !important;
        }

        .panel-title{
            margin: 7px;
            padding: 4px;
            font-size: 16px;
            color: #ffffff !important;
            cursor: pointer;
        }

        .panel-body{
            margin-left: 7px;
            margin-right: 0px;
            /*border-width: 1px;
            border-style: solid;*/
            border-color: red;
            padding: 10px 0px 10px 10px;
        }

        a.panel-btn{
            color: #ffffff !important;
            border-radius: 4px;
            padding-left: 7px;
            padding-right: 7px;
            /*background-color: #343a40;
            border-color: #343a40;*/
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
        }

        a.panel-btn:hover{
            background-color: #000000;
        }

    </style>
@endsection
@section('extra-js')
    <script>
        $(function(){
            $('#modalRelatedParties').on('hidden.bs.modal', function () {

                $('#modalRelatedParties').find('#addrelateddescription').val('');
            })

            $("#addrelatedsave").on("click",function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let client_id = $('#modalRelatedParties').find('#addrelatedclientid').val();
                let related_party_id = $('#modalRelatedParties').find('#addrelatedrelatedid').val();
                let description = $('#modalRelatedParties').find('#addrelateddescription').val();

                $.ajax({
                    url: '/relatedparty/save/'+client_id+'/'+related_party_id,
                    type: "POST",
                    data: {client_id: client_id, related_party_id: related_party_id, description:description},
                    success: function (data) {
                        $("#modalRelatedParties").modal('hide');

                        window.location.href = '/relatedparty/'+client_id;
                        /*$('.flash_msg').html('<div class="alert alert-success alert-dismissible blackboard-alert">\n' +
                            '                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>\n' +
                            '                    <strong>Success!</strong> Client successfully unconverted.\n' +
                            '                </div>');*/
                    }
                });
            })

            $("#addrelatedcancel").on("click",function(){
                $("#modalRelatedParties").modal("hide");
            })

            $('.test123').click(function(){
                alert("Test");
            });
        });

        function addRelatedParty(client_id,related_party_id,) {
            $("#modalRelatedParties").modal("show");
            $('#modalRelatedParties').find('#addrelatedclientid').val(client_id);
            $('#modalRelatedParties').find('#addrelatedrelatedid').val(related_party_id);
        }
    </script>
@endsection