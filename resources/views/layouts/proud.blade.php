<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <title>{{env('APP_NAME')}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{!! asset('storage/favicon.ico') !!}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/proud.css')}}">

</head>{{--<link rel="stylesheet" href="{{asset('css/absa.css')}}">--}}

<body>
<div class="content p-3">
    <div class="card">
        <div class="card-body px-4 pb-4 pt-2">
            <div class="text-center">
                <a href="{{route('home')}}" class="header-link"><img src="{{asset('assets/logo-md.jpg')}}" class="blackboard-proud-logo p-3" style="width:100%" /></a>
            </div>
            <hr>
                @if(Session::has('flash_success'))
                    <div class="alert alert-success alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Success!</strong> {{Session::get('flash_success')}}
                    </div>
                @endif

                @if(Session::has('flash_info'))
                    <div class="alert alert-info alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Notice.</strong> {{Session::get('flash_info')}}
                    </div>
                @endif

                @if(Session::has('flash_danger'))
                    <div class="alert alert-danger alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Error!</strong> {{Session::get('flash_danger')}}
                    </div>
                @endif

            @yield('proud-form')
        </div>
        @yield('proud-footer')
    </div>
    @yield('proud-extra')
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {

        if(localStorage.getItem('popState') === 'shown'){
            localStorage.setItem('popState','')
        }
    });
</script>
</body>
</html>