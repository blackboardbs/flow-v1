<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <title>@yield('title')</title>
    <!--
    ______ _            _    _                         _
    | ___ \ |          | |  | |                       | |
    | |_/ / | __ _  ___| | _| |__   ___   __ _ _ __ __| |
    | ___ \ |/ _` |/ __| |/ / '_ \ / _ \ / _` | '__/ _` |
    | |_/ / | (_| | (__|   <| |_) | (_) | (_| | | | (_| |
    \____/|_|\__,_|\___|_|\_\_.__/ \___/ \__,_|_|  \__,_|

    -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth" content="{{ auth()->id() }}">

    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    <!-- CSS -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('extra-css')
</head>
<body>

<!-- Frontend wrapper -->
<div id="app">
    @include('layouts.navbar')

    <div class="row">
        <div class="blackboard-hero p-3 pl-4 pr-4">
            @yield('header')
        </div>
    </div>

    <div class="row ml-0 mr-0 blackboard-content">

        @auth
            <div onmouseover="showScroller(this)" onmouseout="hideScroller(this)" id="blackboard-sidebar-div" class="blackboard-scrollbar col-lg-2 p-0 d-none d-lg-block">
                @include('layouts.sidebar')
            </div>
        @endauth

        <div onmouseover="showScroller(this)" onmouseout="hideScroller(this)" id="blackboard-content-div" class="blackboard-scrollbar col-lg-10 mb-5 pr-0 pr-lg-3">

            <div class="row">
                @if(Session::has('flash_success'))
                    <div class="alert alert-success alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Success!</strong> {{Session::get('flash_success')}}
                    </div>
                @endif


                @if(Session::has('flash_info'))
                    <div class="alert alert-info alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Notice.</strong> {{Session::get('flash_info')}}
                    </div>
                @endif

                @if(Session::has('flash_danger'))
                    <div class="alert alert-danger alert-dismissible blackboard-alert">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                        <strong>Error!</strong> {{Session::get('flash_danger')}}
                    </div>
                @endif
            </div>
            @yield('content')
        </div>
    </div>
</div>

<!-- Bootsrap Modal -->

<div class="modal fade" id="edit_email_template">
    <div class="modal-dialog" style="width:800px !important;max-width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="email_id" id="email_id" >
                            <input type="hidden" class="form-control" name="activity_id" id="activity_id" >
                        </div>
                        <div class="form-group mt-3">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name','id'=>'email_title'])}}

                        </div>

                        <div class="form-group">
                            {{Form::label('Email Body')}}
                            {{ Form::textarea('email_content', null, ['class'=>'form-control my-editor','size' => '30x10','id'=>'email_content']) }}

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" onclick="saveEmailTemplate()" class="btn btn-primary">Use Template</button>
                    </div>
            </div>
        </div>
    </div>
</div>


<!-- JS -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=361nrfmxzoobhsuqvaj3hyc2zmknskzl4ysnhn78pjosbik2"></script>
<script>
    $(".delete").on("click", function(){
        return confirm("Are you sure you want to delete this record?");
    });

    $(".deleteDoc").on('click', function(){
        $(this).parents('form:first').submit();
    });
</script>
@yield('extra-js')

</body>
</html>
