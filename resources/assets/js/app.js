/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('bootstrap');
require('popper.js');

window.Vue = require('vue');
window.$ = require('jquery');
window._ = require('lodash');
window.axios = require('axios');


window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import Echo from 'laravel-echo'

import Vue from 'vue'
import VueTextareaAutosize from 'vue-textarea-autosize'

Vue.use(VueTextareaAutosize)

window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '6be490b9f6584a494233',
    cluster: 'ap2',
    encrypted: false
});

Vue.component('blackboard-process-editor', require('./components/ProcessEditor.vue'));
Vue.component('blackboard-insight', require('./components/Insight.vue'));
Vue.component('blackboard-search', require('./components/Search.vue'));
Vue.component('blackboard-notifications', require('./components/Notifications.vue'));
Vue.component('blackboard-messages', require('./components/Messages.vue'));

const app = new Vue({
    el: '#app'
});

$("#sidebar_process_status").click(function (e) {
    e.preventDefault();
});

$("#sidebar_process_status").change(function () {
    $(this).closest('form').submit();
});

$("#sidebar_report_dropdown").click(function () {
    $(".child_2").toggle();
});

$("#sidebar_report_dropdown_3").click(function () {
    $(".child_3").toggle();
});

$(document).ready(function () {
    if (window.location.href.indexOf("#") > -1) {
        $('body').css('overflow', 'auto');
    }
    else
    {
        $('#blackboard-content-div').css('height', $(window).height() - 150);
        $('#blackboard-sidebar-div').css('height', $(window).height() - 150);
    }
});
/*
$( window ).resize(function() {
    $('#blackboard-content-div').css('height', $(window).height() - 150);
    $('#blackboard-sidebar-div').css('height', $(window).height() - 150);
});
*/
window.showScroller = function (elem) {
    if (window.location.href.indexOf("#") > -1) {
        //alert("Contains #");
    }
    else
    {
        $('#'+elem.id).css('overflow', 'auto');
    }
}

window.hideScroller = function (elem) {
    if (window.location.href.indexOf("#") > -1) {
        //alert("Contains #");
    }
    else
    {
        //$('#'+elem.id).css('overflow', 'hidden');
    }
}